﻿// <copyright file="Database.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DocumentStorage.Data
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Linq;
    using DocumentStorage.Entities;

    /// <summary>
    /// The database class.
    /// </summary>
    public class Database : IDatabase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Database"/> class.
        /// </summary>
        public Database()
        {
            this.DbEntities = new LocalDBEntities();
        }

        /// <summary>
        /// Gets or sets the EF entity.
        /// </summary>
        public LocalDBEntities DbEntities { get; set; }

        /// <summary>
        /// On startup events.
        /// </summary>
        public void Startup()
        {
        }

        /// <summary>
        /// Inserts a page to the DB.
        /// </summary>
        /// <param name="image"> The image.</param>
        /// <param name="filename">The location of the image.</param>
        /// <param name="user">The activity user.</param>
        /// <param name="id">the Id.</param>
        /// <returns>The page id.</returns>
        public int Insertpage(Bitmap image, string filename, string user, int id = 0)
        {
            Page newPage = new Page()
            {
                BASE64_IMAGE = this.BitmapToBase64(image),
                OGIGINAL_FILENAME = Path.GetFileName(filename),
                CREATE_DATE = DateTime.Now,
                CREATE_USER = this.DbEntities.User.Where(x => x.NAME.ToLower() == user.ToLower()).First().ID,
                DOCUMENT_TYPE = 1,
                Id = id != 0 ? id : this.DbEntities.Page.Count() == 0 ? 1 : this.DbEntities.Page.OrderByDescending(x => x.Id).First().Id + 1,
            };
            this.DbEntities.Page.Add(newPage);
            this.DbEntities.SaveChanges();
            GC.Collect();
            GC.WaitForPendingFinalizers();
            return this.DbEntities.Page.Where(x => x.BASE64_IMAGE == newPage.BASE64_IMAGE).First().Id;
        }

        /// <summary>
        /// Inserts a line into the DB.
        /// </summary>
        /// <param name="value">the string value. </param>
        /// <param name="user">The activity user.</param>
        /// <param name="page_id">The page id.</param>
        /// <param name="id">The id.</param>
        /// <returns>the object id.</returns>
        public int Insertline(string value, string user, int page_id = 0, int id = 0)
        {
            Line newLine = new Line()
            {
                Page = this.DbEntities.Page.Where(x => x.Id == page_id).First(),
                PAGE_ID = page_id,
                CREATE_DATE = DateTime.Now,
                User = this.DbEntities.User.Where(x => x.NAME == user).First(),
                CREATE_USER = this.DbEntities.User.Where(x => x.NAME.ToLower() == user.ToLower()).First().ID,
                VALUE = value,
                Id = id != 0 ? id : this.DbEntities.Line.Count() == 0 ? 1 : this.DbEntities.Line.OrderByDescending(x => x.Id).First().Id + 1,
            };
            this.DbEntities.Line.Add(newLine);
            this.DbEntities.SaveChanges();
            GC.Collect();
            GC.WaitForPendingFinalizers();
            return this.DbEntities.Line.Where(x => x.Id == newLine.Id).First().Id;
        }

        /// <summary>
        /// Inserts a word into the DB.
        /// </summary>
        /// <param name="rectangle">The position.</param>
        /// <param name="value">The string value.</param>
        /// <param name="user">The activity user.</param>
        /// <param name="line_id">The id of the line containing this word.</param>
        /// <param name="id">The word id.</param>
        /// <returns>The id.</returns>
        public int Insertword(Rectangle rectangle, string value, string user, int line_id = 0, int id = 0)
        {
            int line_Id = line_id == 0 ? 0 : this.DbEntities.Line.Where(x => x.Id == line_id).FirstOrDefault().Id;
            Word newWord = new Word()
            {
                PAGE_ID = line_id == 0 ? 0 : this.DbEntities.Line.Where(x => x.Id == line_Id).Select(x => x.PAGE_ID).First(),
                LINE_ID = line_id == 0 ? 0 : line_Id,
                CREATE_DATE = DateTime.Now,
                CREATE_USER = this.DbEntities.User.Where(x => x.NAME.ToLower() == user.ToLower()).First().ID,
                X = rectangle.X,
                Y = rectangle.Y,
                WIDTH = rectangle.Width,
                HEIGHT = rectangle.Height,
                VALUE = value,
                Id = id != 0 ? id : this.DbEntities.Word.Count() == 0 ? 1 : this.DbEntities.Word.OrderByDescending(x => x.Id).First().Id + 1,
            };
            this.DbEntities.Word.Add(newWord);
            this.DbEntities.SaveChanges();
            GC.Collect();
            GC.WaitForPendingFinalizers();
            return this.DbEntities.Word.Where(x => x.Id == newWord.Id).First().Id;
        }

        /// <summary>
        /// Inserts a Rule into the DB.
        /// </summary>
        /// <param name="wrongChar">wrongchar.</param>
        /// <param name="rightChar">The right char.</param>
        /// <param name="user_id">The activity user.</param>
        /// <param name="id">The id.</param>
        /// <param name="type">The rule type.</param>
        public void Insertrule(string wrongChar, string rightChar, int user_id, int id = 0, int type = 0)
        {
            Rule newRule = new Rule()
            {
                REC_VALUE = wrongChar,
                COR_VALUE = rightChar,
                CREATE_DATE = DateTime.Now,
                CREATE_USER = user_id,
                ID = id != 0 ? id : this.DbEntities.Rule.Count() == 0 ? 1 : this.DbEntities.Rule.OrderByDescending(x => x.ID).First().ID + 1,
            };
            this.DbEntities.Rule.Add(newRule);
            this.DbEntities.SaveChanges();
        }

        /// <summary>
        /// Inserts a user into th DB.
        /// </summary>
        /// <param name="name">The username.</param>
        /// <param name="admin">Is admin.</param>
        /// <param name="user_id">Th id of the user.</param>
        public void Insertuser(string name, bool admin, int user_id)
        {

            User newUser = new User()
            {
                CREATE_DATE = DateTime.Now,
                IS_ADMIN = admin,
                NAME = name,
                ID = user_id != 0 ? user_id : this.DbEntities.User.Count() == 0 ? 1 : this.DbEntities.User.OrderByDescending(x => x.ID).First().ID + 1,
            };
            this.DbEntities.User.Add(newUser);
            this.DbEntities.SaveChanges();
        }

        /// <summary>
        /// Gets the users.
        /// </summary>
        /// <param name="admin">is admin.</param>
        /// <returns>The user.</returns>
        public IEnumerable<ITupleObj<int, string, string, DateTime>> Getusers(bool admin = false)
        {
            var user = from us in this.DbEntities.User
                       where
                       !admin ? !us.IS_ADMIN : us.IS_ADMIN || !us.IS_ADMIN
                       orderby us.ID
                       select new TupleObj<int, string, string, DateTime>()
                       {
                           Item1 = us.ID,
                           Item2 = us.NAME,
                           Item3 = us.IS_ADMIN ? "ADMIN" : "USER",
                           Item4 = us.CREATE_DATE,
                       };
            return user;
        }

        /// <summary>
        /// Gets the rules from the DB.
        /// </summary>
        /// <returns>The ruleset.</returns>
        public IEnumerable<ITupleObj<int, string, string, int, DateTime>> Getrules()
        {
            var rule = from ru in this.DbEntities.Rule
                       orderby ru.ID
                       select new TupleObj<int, string, string, int, DateTime>()
                       {
                           Item1 = ru.ID,
                           Item2 = ru.REC_VALUE,
                           Item3 = ru.COR_VALUE,
                           Item4 = ru.CREATE_USER,
                           Item5 = ru.CREATE_DATE,
                       };
            return rule;
        }

        /// <summary>
        /// Gets the pages.
        /// </summary>
        /// <returns>The pages.</returns>
        public IEnumerable<ITupleObj<int, string, string, DateTime, int, int>> Getpages()
        {
            List<ITupleObj<int, string, string, DateTime, int, int>> retvals = new List<ITupleObj<int, string, string, DateTime, int, int>>();
            foreach (var item in this.DbEntities.Page)
            {
                retvals.Add(new TupleObj<int, string, string, DateTime, int, int>
                {
                    Item1 = item.Id,
                    Item2 = item.BASE64_IMAGE,
                    Item3 = item.OGIGINAL_FILENAME,
                    Item4 = item.CREATE_DATE,
                    Item5 = item.CREATE_USER,
                    Item6 = item.DOCUMENT_TYPE,
                });
            }

            return retvals;
        }

        /// <summary>
        /// Gets the lines.
        /// </summary>
        /// <returns>The lines.</returns>
        public IEnumerable<ITupleObj<int, string, DateTime, int, int, int, int, int, int, string>> Getlines()
        {
            List<ITupleObj<int, string, DateTime, int, int, int, int, int, int, string>> retvals = new List<ITupleObj<int, string, DateTime, int, int, int, int, int, int, string>>();
            foreach (var item in this.DbEntities.Line)
            {
                retvals.Add(new TupleObj<int, string, DateTime, int, int, int, int, int, int, string>
                {
                    Item1 = item.Id,
                    Item3 = item.CREATE_DATE,
                    Item4 = item.CREATE_USER,
                    Item5 = item.PAGE_ID,
                });
            }

            return retvals;
        }

        /// <summary>
        /// Gets the words.
        /// </summary>
        /// <returns>the words.</returns>
        public IEnumerable<ITupleObj<int, string, DateTime, int, int, int, int, int, int, string>> Getwords()
        {
            List<ITupleObj<int, string, DateTime, int, int, int, int, int, int, string>> retvals = new List<ITupleObj<int, string, DateTime, int, int, int, int, int, int, string>>();
            foreach (var item in this.DbEntities.Word)
            {
                retvals.Add(new TupleObj<int, string, DateTime, int, int, int, int, int, int, string>
                {
                    Item1 = item.Id,
                    Item3 = item.CREATE_DATE,
                    Item4 = item.CREATE_USER,
                    Item5 = item.LINE_ID.Value,
                    Item10 = item.VALUE,
                });
            }

            return retvals;
        }

        /// <summary>
        /// Returns all page entities of a user.
        /// </summary>
        /// <param name="user_id">The user.</param>
        /// <returns>The pages.</returns>
        public List<ITupleObj<int, string, string, DateTime, int, int, List<ITupleObj<int, string, DateTime, int, int, int, int, int, int, string, List<ITupleObj<int, string, DateTime, int, int, int, int, int, int, string>>>>>> GetPagesByUser(int user_id)
        {
            List<ITupleObj<int, string, string, DateTime, int, int, List<ITupleObj<int, string, DateTime, int, int, int, int, int, int, string, List<ITupleObj<int, string, DateTime, int, int, int, int, int, int, string>>>>>> pages = new List<ITupleObj<int, string, string, DateTime, int, int, List<ITupleObj<int, string, DateTime, int, int, int, int, int, int, string, List<ITupleObj<int, string, DateTime, int, int, int, int, int, int, string>>>>>>();
            foreach (var page in this.DbEntities.Page.Where(x => x.CREATE_USER == user_id))
            {
                var repage = new TupleObj<int, string, string, DateTime, int, int, List<ITupleObj<int, string, DateTime, int, int, int, int, int, int, string, List<ITupleObj<int, string, DateTime, int, int, int, int, int, int, string>>>>>()
                {
                    Item1 = page.Id,
                    Item2 = page.BASE64_IMAGE,
                    Item3 = page.OGIGINAL_FILENAME,
                    Item4 = page.CREATE_DATE,
                    Item5 = page.CREATE_USER,
                    Item6 = page.DOCUMENT_TYPE,
                    Item7 = new List<ITupleObj<int, string, DateTime, int, int, int, int, int, int, string, List<ITupleObj<int, string, DateTime, int, int, int, int, int, int, string>>>>(),
                };

                foreach (var line in this.DbEntities.Line.Where(x => x.PAGE_ID == page.Id))
                {
                    var reline = new TupleObj<int, string, DateTime, int, int, int, int, int, int, string, List<ITupleObj<int, string, DateTime, int, int, int, int, int, int, string>>>()
                    {
                        Item1 = line.Id,
                        Item3 = line.CREATE_DATE,
                        Item4 = line.CREATE_USER,
                        Item5 = page.Id,
                        Item11 = new List<ITupleObj<int, string, DateTime, int, int, int, int, int, int, string>>(),
                    };

                    foreach (var word in this.DbEntities.Word.Where(x => x.LINE_ID == line.Id))
                    {
                        var reword = new TupleObj<int, string, DateTime, int, int, int, int, int, int, string>()
                        {
                            Item1 = word.Id,
                            Item2 = word.BASE64_IMAGE,
                            Item3 = word.CREATE_DATE,
                            Item4 = word.CREATE_USER,
                            Item5 = line.Id,
                            Item6 = word.X.Value,
                            Item7 = word.Y.Value,
                            Item8 = word.WIDTH.Value,
                            Item9 = word.HEIGHT.Value,
                            Item10 = word.VALUE,
                        };
                        reline.Item11.Add(reword);
                    }

                    repage.Item7.Add(reline);
                }

                pages.Add(repage);
            }

            return pages;
        }

        /// <summary>
        /// Return the number of rules to a user.
        /// </summary>
        /// <returns>The number.</returns>
        public IEnumerable<ITupleObj<string, int>> GetRuleCountForUsers()
        {
            var count = from us in this.DbEntities.User
                        orderby us.ID
                        select new TupleObj<string, int>()
                        {
                            Item1 = us.NAME,
                            Item2 = this.DbEntities.Rule.Where(x => x.CREATE_USER == us.ID).Count(),
                        };
            return count;
        }

        /// <summary>
        /// Updates a rule in the DB.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="rec_value">The rec_value.</param>
        /// <param name="cor_value">The cor_value.</param>
        /// <param name="type">The type.</param>
        public void UpdateRule(int id, string rec_value, string cor_value, int type)
        {
            if (this.DbEntities.Rule.Where(z => z.ID == id).Count() < 1)
            {
                throw new Exception("The id not found in the Rule's Dataset.");
            }

            Rule upRule = new Rule()
            {
                ID = id,
                COR_VALUE = cor_value == string.Empty ? this.DbEntities.Rule.Where(x => x.ID == id).First().COR_VALUE : cor_value,
                REC_VALUE = rec_value == string.Empty ? this.DbEntities.Rule.Where(x => x.ID == id).First().REC_VALUE : rec_value,
                TYPE = type == 0 ? this.DbEntities.Rule.Where(x => x.ID == id).First().TYPE : type,
                CREATE_DATE = this.DbEntities.Rule.Where(x => x.ID == id).First().CREATE_DATE,
                CREATE_USER = this.DbEntities.Rule.Where(x => x.ID == id).First().CREATE_USER,
            };
            this.DbEntities.Rule.Remove(this.DbEntities.Rule.Where(x => x.ID == id).First());
            this.DbEntities.Rule.Add(upRule);
            this.DbEntities.SaveChanges();
        }

        /// <summary>
        /// Updates a user in the DB.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="name">The name.</param>
        /// <param name="admin">Is admin.</param>
        public void UpdateUser(int id, string name, bool admin)
        {
            if (this.DbEntities.User.Where(z => z.ID == id).Count() < 1)
            {
                throw new Exception("The id not found in the User's Dataset.");
            }

            User upUser = new User()
            {
                ID = id,
                NAME = name == string.Empty ? this.DbEntities.User.Where(x => x.ID == id).First().NAME : name,
                IS_ADMIN = admin,
            };
            this.DbEntities.User.Remove(this.DbEntities.User.Where(x => x.ID == id).First());
            this.DbEntities.User.Add(upUser);
            this.DbEntities.SaveChanges();
        }

        /// <summary>
        /// Updates a word in the DB.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="x">The x cord.</param>
        /// <param name="y">The y cord.</param>
        /// <param name="width">The witdth.</param>
        /// <param name="height">The height.</param>
        /// <param name="value">The value.</param>
        public void UpdateWord(int id, int x, int y, int width, int height, string value)
        {
            if (this.DbEntities.Word.Where(z => z.Id == id).Count() < 1)
            {
                throw new Exception("The id not found in the Word's Dataset.");
            }

            Word upWord = new Word()
            {
                Id = id,
                X = x == 0 ? this.DbEntities.Word.Where(z => z.Id == id).First().X : x,
                Y = y == 0 ? this.DbEntities.Word.Where(z => z.Id == id).First().Y : y,
                WIDTH = width == 0 ? this.DbEntities.Word.Where(z => z.Id == id).First().X : width,
                HEIGHT = height == 0 ? this.DbEntities.Word.Where(z => z.Id == id).First().X : height,
                VALUE = value == string.Empty ? this.DbEntities.Word.Where(z => z.Id == id).First().VALUE : value,
            };
            this.DbEntities.Word.Remove(this.DbEntities.Word.Where(z => z.Id == id).First());
            this.DbEntities.Word.Add(upWord);
            this.DbEntities.SaveChanges();
        }

        /// <summary>
        /// Deletes a user from the DB.
        /// </summary>
        /// <param name="id">The id.</param>
        public void DeleteUser(int id)
        {
            if (this.DbEntities.User.Where(z => z.ID == id).Count() < 1)
            {
                throw new Exception("The id not found in the User's Dataset.");
            }

            this.DbEntities.User.Remove(this.DbEntities.User.Where(z => z.ID == id).First());
            this.DbEntities.SaveChanges();
        }

        /// <summary>
        /// Deletes a page from the DB.
        /// </summary>
        /// <param name="id">The id.</param>
        public void DeletePage(int id)
        {
            if (this.DbEntities.Page.Where(z => z.Id == id).Count() < 1)
            {
                throw new Exception("The id not found in the Page's Dataset.");
            }

            foreach (Line line in this.DbEntities.Line.Where(x => x.PAGE_ID == id))
            {
                foreach (Word word in this.DbEntities.Word.Where(x => x.LINE_ID == line.Id))
                {
                    this.DbEntities.Word.Remove(word);
                }

                this.DbEntities.Line.Remove(line);
            }

            this.DbEntities.User.Remove(this.DbEntities.User.Where(z => z.ID == id).First());
            this.DbEntities.SaveChanges();
        }

        /// <summary>
        /// Deletes a rule from the DB.
        /// </summary>
        /// <param name="id">The id.</param>
        public void DeleteRule(int id)
        {
            if (this.DbEntities.Rule.Where(z => z.ID == id).Count() < 1)
            {
                throw new Exception("The id not found in the Rule's Dataset.");
            }

            this.DbEntities.Rule.Remove(this.DbEntities.Rule.Where(z => z.ID == id).First());
            this.DbEntities.SaveChanges();
        }

        private string BitmapToBase64(Bitmap image)
        {
            System.Drawing.Imaging.Encoder myEncoder = System.Drawing.Imaging.Encoder.Quality;
            ImageCodecInfo info = null;
            foreach (var item in ImageCodecInfo.GetImageEncoders())
            {
                if (item.FormatDescription == "PNG")
                {
                    info = item;
                }
            }

            MemoryStream memoryStream = new MemoryStream();
            image.Save(memoryStream, info, new EncoderParameters(1)
            { Param = new System.Drawing.Imaging.EncoderParameter[] { new EncoderParameter(myEncoder, 50L) } });
            memoryStream.Position = 0;
            byte[] byteBuffer = memoryStream.ToArray();
            memoryStream.Close();
            return Convert.ToBase64String(byteBuffer);
        }
    }
}
