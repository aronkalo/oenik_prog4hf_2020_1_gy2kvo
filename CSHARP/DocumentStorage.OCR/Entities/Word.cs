﻿// <copyright file="Word.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace DocumentStorage.OCR.Entities
{
    using System.Drawing;

    /// <summary>
    /// The Word Object.
    /// </summary>
    public class Word
    {
        /// <summary>
        /// Gets or sets position of the Word on the image.
        /// </summary>
        public Rectangle Position { get; set; }

        /// <summary>
        /// Gets or sets the Word string value.
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Gets or sets the Id of the line contains this word.
        /// </summary>
        public int LineId { get; set; }
    }
}
