﻿// <copyright file="Page.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DocumentStorage.OCR.Entities
{
    using System.Collections.Generic;
    using System.Drawing;

    /// <summary>
    /// The structured representation of a document image.
    /// </summary>
    public class Page
    {
        /// <summary>
        /// Gets or sets the image of the page.
        /// </summary>
        public Bitmap Image { get; set; }

        /// <summary>
        /// Gets or sets the lines of the page.
        /// </summary>
        public List<Line> Lines { get; set; }
    }
}
