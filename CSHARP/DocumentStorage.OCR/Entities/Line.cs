﻿// <copyright file="Line.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DocumentStorage.OCR.Entities
{
    using System.Collections.Generic;

    /// <summary>
    /// The Line of a page.
    /// </summary>
    public class Line
    {
        /// <summary>
        /// Gets or sets the id of the Line.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets list of the words contained in this list.
        /// </summary>
        public List<Word> Words { get; set; }
    }
}
