﻿// <copyright file="ITesseractCop.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DocumentStorage.OCR.Interfaces
{
    using System.Collections.Generic;
    using System.Drawing;
    using DocumentStorage.Entities;

    /// <summary>
    /// The interface of the Structured processing of Tesseract.
    /// </summary>
    public interface ITesseractCop
    {
        /// <summary>
        /// The OCR Process Structured Method.
        /// </summary>
        /// /// <param name="workDir">the tesseract.exe directory.</param>
        /// <param name="confFile">the config file type.</param>
        /// <param name="images">the input images.</param>
        /// <returns>The Structured PageSet.</returns>
        List<ITupleObj<Bitmap, List<ITupleObj<int, List<ITupleObj<Rectangle, string, int>>>>>> StructuredProcess(string workDir, string confFile, IEnumerable<Bitmap> images);

        /// <summary>
        /// The method of Highlighting the input words in a pages image.
        /// </summary>
        /// <param name="pages">The iput pages.</param>
        /// <param name="sewords"> The matching words.</param>
        /// <param name="color">The color of the bracket.</param>
        /// <returns>The Higglighted pages.</returns>
        List<ITupleObj<Bitmap, List<ITupleObj<int, List<ITupleObj<Rectangle, string, int>>>>>> HighLightWords(List<ITupleObj<Bitmap, List<ITupleObj<int, List<ITupleObj<Rectangle, string, int>>>>>> pages, List<ITupleObj<Rectangle, string, int>> sewords, Color color);
    }
}
