﻿// <copyright file="ImageProcessor.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DocumentStorage.OCR.PreProcessing
{
    using System.Collections.Generic;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.Linq;

    /// <summary>
    /// The image manipulator class.
    /// </summary>
    public class ImageProcessor
    {
        /// <summary>
        /// Gets or sets the image to process.
        /// </summary>
        public Bitmap Image { get; set; }

        /// <summary>
        ///  Puts a Grayscale filter on th input image.
        /// </summary>
        /// <param name="image">The input image.</param>
        /// <returns>The Graysacle version of the input image.</returns>
        public Bitmap GrayScale(Bitmap image)
        {
            Bitmap d = new Bitmap(image.Width, image.Height, PixelFormat.Format16bppGrayScale);
            int x, y;

            // Loop through the images pixels to reset color.
            for (x = 0; x < image.Width; x++)
            {
                for (y = 0; y < image.Height; y++)
                {
                    Color oc = image.GetPixel(x, y);
                    int grayScale = (int)((oc.R * 0.3) + (oc.G * 0.59) + (oc.B * 0.11));
                    image.SetPixel(x, y, Color.FromArgb(oc.A, grayScale, grayScale, grayScale)); // Now grayscale
                }
            }

            return d = image;
        }

        /// <summary>
        /// Invert all pixels of the input image.
        /// </summary>
        /// <param name="image">The input image.</param>
        /// <returns>The inverted image.</returns>
        public Bitmap Invert(Bitmap image)
        {
            for (int y = 0; y <= (image.Height - 1); y++)
            {
                for (int x = 0; x <= (image.Width - 1); x++)
                {
                    Color inv = image.GetPixel(x, y);
                    inv = Color.FromArgb(255, 255 - inv.R, 255 - inv.G, 255 - inv.B);
                    image.SetPixel(x, y, inv);
                }
            }

            return image;
        }

        /// <summary>
        /// Converts the input image to BlackandWhite.
        /// </summary>
        /// <param name="image">The input image.</param>
        /// <param name="pixelCount">The pixelcount to the sliding window.</param>
        /// <returns>The BlackandWhite image.</returns>
        public Bitmap ConvertToBlackAndWhite(Bitmap image, int pixelCount)
        {
            List<Color> neighbours = new List<Color>();
            Bitmap outImage = new Bitmap(image, image.Width, image.Height);
            for (int i = 1; i < image.Width - 1; i++)
            {
                for (int y = 1; y < image.Height - 1; y++)
                {
                    neighbours = this.GetSurroundingPixels(image, i, y);
                    bool white = false;
                    if (neighbours[0].R < 128 && neighbours[0].G < 128 && neighbours[0].B < 128)
                    {
                        white = true;
                    }

                    outImage.SetPixel(i, y, white ? neighbours.Where(x => x.R < 128 && x.G < 128 && x.B < 128).Count() < pixelCount ? Color.FromArgb(255, 255, 255, 255) : Color.FromArgb(255, 0, 0, 0) : Color.FromArgb(255, 255, 255, 255));
                }
            }

            return outImage;
        }

        private List<Color> GetSurroundingPixels(Bitmap image, int x, int y)
        {
            List<Color> sorroundingPixels = new List<Color>();
            sorroundingPixels.Add(image.GetPixel(x, y));
            sorroundingPixels.Add(image.GetPixel(x - 1, y));
            sorroundingPixels.Add(image.GetPixel(x + 1, y));
            sorroundingPixels.Add(image.GetPixel(x, y - 1));
            sorroundingPixels.Add(image.GetPixel(x, y + 1));
            sorroundingPixels.Add(image.GetPixel(x - 1, y + 1));
            sorroundingPixels.Add(image.GetPixel(x - 1, y - 1));
            sorroundingPixels.Add(image.GetPixel(x + 1, y + 1));
            sorroundingPixels.Add(image.GetPixel(x + 1, y - 1));
            return sorroundingPixels;
        }
    }
}
