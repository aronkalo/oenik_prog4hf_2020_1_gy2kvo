﻿// <copyright file="TesseractEngine.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DocumentStorage.OCR.Tesseract
{
    using System;
    using System.Diagnostics;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Text;

    /// <summary>
    /// OCR Engine.
    /// </summary>
    internal class TesseractEngine
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TesseractEngine"/> class.
        /// </summary>
        /// <param name="workDir">The tesseract.exe directory.</param>
        public TesseractEngine(string workDir)
        {
            this.WorkDir = workDir;
            this.Lang = "eng";
            this.ExeName = "tesseract.exe";
        }

        /// <summary>
        /// Gets or sets the image to process.
        /// </summary>
        private Bitmap ProcImage { get; set; }

        /// <summary>
        /// Gets or sets the OCR engine working directory.
        /// </summary>
        private string WorkDir { get; set; }

        /// <summary>
        /// Gets or sets the name of the OCR exe.
        /// </summary>
        private string ExeName { get; set; }

        /// <summary>
        /// Gets or sets the language of the recognition.
        /// </summary>
        private string Lang { get; set; }

        /// <summary>
        /// Gets or sets the configfile of the recognition.
        /// </summary>
        private string ConfigFile { get; set; }

        /// <summary>
        /// Returns the String from an image.
        /// </summary>
        /// <param name="image">the image to process.</param>
        /// <param name="configFile">the conffile to use after the process.</param>
        /// <returns>the value of the image.</returns>
        public string StartProcess(Bitmap image, string configFile)
        {
            this.ProcImage = image;
            if (this.ProcImage == null)
            {
                throw new Exception("Unexpected Error Occured during adding your Bitmap instance");
            }

            this.ConfigFile = configFile;
            string stdOut = string.Empty;
            ProcessStartInfo info = new ProcessStartInfo()
            {
                RedirectStandardInput = true,
                RedirectStandardError = true,
                Arguments = "stdin " + "stdout" + " " + " -l " + this.Lang + " " + this.ConfigFile,
                CreateNoWindow = true,
                FileName = Path.Combine(this.WorkDir, this.ExeName),
                RedirectStandardOutput = true,
                StandardOutputEncoding = Encoding.UTF8,
                UseShellExecute = false,
            };
            Process process = new Process()
            {
                StartInfo = info,
            };
            process.Start();
            using (var stream = new BinaryWriter(process.StandardInput.BaseStream))
            {
                stream.Write(this.BitmapToByteArray(this.ProcImage, new ImageFormat(new Guid(Encoding.ASCII.GetBytes(new string(' ', 16))))));
            }

            stdOut = process.StandardOutput.ReadToEnd();
            process.WaitForExit();
            return stdOut;
        }

        private byte[] BitmapToByteArray(Bitmap image, ImageFormat imageFormat)
        {
            System.Drawing.Imaging.Encoder myEncoder = System.Drawing.Imaging.Encoder.Quality;
            ImageCodecInfo info = null;
            foreach (var item in ImageCodecInfo.GetImageEncoders())
            {
                if (item.FormatDescription == "PNG")
                {
                    info = item;
                }
            }

            MemoryStream memoryStream = new MemoryStream();
            image.Save(memoryStream, info, new EncoderParameters(1)
            { Param = new System.Drawing.Imaging.EncoderParameter[] { new EncoderParameter(myEncoder, 50L) } });
            memoryStream.Position = 0;
            byte[] byteBuffer = memoryStream.ToArray();
            memoryStream.Close();
            return byteBuffer;
        }
    }
}
