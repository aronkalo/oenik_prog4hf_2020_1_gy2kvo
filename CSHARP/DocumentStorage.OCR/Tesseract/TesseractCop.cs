﻿// <copyright file="TesseractCop.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DocumentStorage.OCR.Tesseract
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using DocumentStorage.Entities;
    using DocumentStorage.OCR.Entities;
    using DocumentStorage.OCR.Interfaces;

    /// <summary>
    /// The Structured output validator in Tesseract.
    /// </summary>
    public class TesseractCop : ITesseractCop
    {
        /// <summary>
        /// Gets the pages to process.
        /// </summary>
        public List<Page> Pages { get; private set; }

        /// <summary>
        /// Gets or sets the processor of the images.
        /// </summary>
        private TesseractEngine Engine { get; set; }

        /// <summary>
        /// The Structured processing of the Cop's Pages.
        /// </summary>
        /// <param name="workDir">the tesseract.exe directory.</param>
        /// <param name="confFile">the config file to use.</param>
        /// <param name="images">the images to process.</param>
        /// <returns>the structured pages.</returns>
        public List<ITupleObj<Bitmap, List<ITupleObj<int, List<ITupleObj<Rectangle, string, int>>>>>> StructuredProcess(string workDir, string confFile, IEnumerable<Bitmap> images)
        {
            List<ITupleObj<Bitmap, List<ITupleObj<int, List<ITupleObj<Rectangle, string, int>>>>>> retval = new List<ITupleObj<Bitmap, List<ITupleObj<int, List<ITupleObj<Rectangle, string, int>>>>>>();
            foreach (var image in images)
            {
                this.Pages = new List<Page>();
                this.Engine = new TesseractEngine(workDir);
                string imageStr = this.Engine.StartProcess(image, confFile);
                this.Pages.Add(new Page()
                {
                    Image = image,
                    Lines = new List<Line>(),
                });
                retval.Add(new TupleObj<Bitmap, List<ITupleObj<int, List<ITupleObj<Rectangle, string, int>>>>>()
                {
                    Item1 = image,
                    Item2 = new List<ITupleObj<int, List<ITupleObj<Rectangle, string, int>>>>(),
                });
                string[] splittedByLines = imageStr.Split(new string[] { "\r\n" }, StringSplitOptions.None);
                for (int i = 0; i < splittedByLines.Length; i++)
                {
                    if (splittedByLines[i] != string.Empty)
                    {
                        string[] splittedByAttributes = splittedByLines[i].Split('\t');

                        if (i > 0 && splittedByAttributes.Count() > 10 && (int.Parse(splittedByAttributes[10]) != -1))
                        {
                            if (this.Pages.Last().Lines.Count == 0)
                            {
                                this.Pages.Last().Lines.Add(new Line()
                                {
                                    Id = this.Pages.Last().Lines.Count + 1,
                                    Words = new List<Word>(),
                                });
                                retval.Last().Item2.Add(new TupleObj<int, List<ITupleObj<Rectangle, string, int>>>()
                                {
                                    Item1 = this.Pages.Last().Lines.Count,
                                    Item2 = new List<ITupleObj<Rectangle, string, int>>(),
                                });
                            }

                            this.Pages.Last().Lines.Last().Words.Add(new Word()
                            {
                                Value = splittedByAttributes[11],
                                LineId = this.Pages.Last().Lines.Last().Id,
                                Position = new Rectangle(int.Parse(splittedByAttributes[6]), int.Parse(splittedByAttributes[7]), int.Parse(splittedByAttributes[8]), int.Parse(splittedByAttributes[9])),
                            });

                            retval.Last().Item2.Last().Item2.Add(new TupleObj<Rectangle, string, int>()
                            {
                                Item1 = new Rectangle(int.Parse(splittedByAttributes[6]), int.Parse(splittedByAttributes[7]), int.Parse(splittedByAttributes[8]), int.Parse(splittedByAttributes[9])),
                                Item2 = splittedByAttributes[11],
                                Item3 = this.Pages.Last().Lines.Last().Id,
                            });
                        }
                        else
                        {
                            this.Pages.Last().Lines.Add(new Line()
                            {
                                Id = this.Pages.Last().Lines.Count + 1,
                                Words = new List<Word>(),
                            });

                            retval.Last().Item2.Add(new TupleObj<int, List<ITupleObj<Rectangle, string, int>>>()
                            {
                                Item1 = this.Pages.Last().Lines.Count,
                                Item2 = new List<ITupleObj<Rectangle, string, int>>(),
                            });
                        }
                    }
                }
            }

            return retval;
        }

        /// <summary>
        /// Highlight rectangles on pages.
        /// </summary>
        /// <param name="pages">The pages to prrocess.</param>
        /// <param name="sewords">The search patterns.</param>
        /// <param name="color">The color of the Rectangle.</param>
        /// <returns>The Highlighted pages list.</returns>
        public List<ITupleObj<Bitmap, List<ITupleObj<int, List<ITupleObj<Rectangle, string, int>>>>>> HighLightWords(List<ITupleObj<Bitmap, List<ITupleObj<int, List<ITupleObj<Rectangle, string, int>>>>>> pages, List<ITupleObj<Rectangle, string, int>> sewords, Color color)
        {
            foreach (var page in pages)
            {
                foreach (var line in page.Item2)
                {
                    foreach (var seword in sewords)
                    {
                        foreach (var word in line.Item2.Where(y => y.Item1 == seword.Item1 && y.Item2 == seword.Item2))
                        {
                            using (Graphics gfx = Graphics.FromImage(this.Pages.Last().Image))
                            {
                                using (SolidBrush sb = new SolidBrush(color))
                                {
                                    gfx.DrawRectangle(
                                        new Pen(sb)
                                        {
                                        Color = color,
                                        Width = 2,
                                        },
                                        word.Item1);
                                }
                            }
                        }
                    }
                }
            }

            return pages;
        }
    }
}
