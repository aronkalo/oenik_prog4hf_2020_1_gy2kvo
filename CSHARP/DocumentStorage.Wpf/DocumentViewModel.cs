﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentStorage.Wpf
{
    /*
                    Id = rule.Item1,
                    Name = rule.Item2,
                    AddDate = rule.Item4,
                    Admin = rule.Item3.ToUpper() == "ADMIN" ? true : false,
                    */
    class DocumentViewModel : ObservableObject
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private bool admin;

        public bool Admin
        {
            get { return admin; }
            set { admin = value; }
        }


        private DateTime addDate;

        public DateTime AddDate
        {
            get { return addDate; }
            set { addDate = value; }
        }


        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public void CopyFrom(DocumentViewModel viewModel)
        {
            if (viewModel == null)
            {
                return;
            }
            this.AddDate = viewModel.AddDate;
            this.Admin = viewModel.Admin;
            this.Id = viewModel.Id;
            this.Name = viewModel.Name;
        }

    }
}
