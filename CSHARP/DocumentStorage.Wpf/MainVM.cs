﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DocumentStorage.Wpf
{
	class MainVM : ViewModelBase
	{
		private MainLogic logic;

		private DocumentViewModel selectedDoc;

		public DocumentViewModel SelectedDoc
		{
			get { return selectedDoc; }
			set { Set(ref selectedDoc, value); }
		}

		private ObservableCollection<DocumentViewModel> allDoc;

		public MainVM()
		{
			logic = new MainLogic();

			DelCmd = new RelayCommand(() => logic.ApiDelCar(selectedDoc));
			AddCmd = new RelayCommand(() => logic.EditCar(null, EditorFunc));
			ModCmd = new RelayCommand(() => logic.EditCar(selectedDoc, EditorFunc));
			LoadCmd = new RelayCommand(() =>
			AllDoc = new ObservableCollection<DocumentViewModel>(logic.ApiGetCars()));
		}

		public ObservableCollection<DocumentViewModel> AllDoc
		{
			get { return allDoc; }
			set { Set(ref allDoc, value); }
		}

		public ICommand AddCmd { get; private set; }
		public ICommand DelCmd { get; private set; }
		public ICommand ModCmd { get; private set; }
		public ICommand LoadCmd { get; private set; }
		public Func<DocumentViewModel, bool> EditorFunc {get; set;}
	}
}
