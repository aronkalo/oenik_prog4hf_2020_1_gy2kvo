﻿using GalaSoft.MvvmLight.Messaging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DocumentStorage.Wpf
{
    class MainLogic
    {
        string url = "http://localhost:61395/api/UserApi/";
        HttpClient client = new HttpClient();

        void SendMessage(bool success)
        {
            string msg = success ? "Operation Completed Succesfully" : "Operation Failed";
            Messenger.Default.Send(msg, "DocumentResult");
        }

        public List<DocumentViewModel> ApiGetCars()
        {
            string json = client.GetStringAsync(url + "all").Result;
            var list = JsonConvert.DeserializeObject<List<DocumentViewModel>>(json);
            return list;
        }

        public void ApiDelCar(DocumentViewModel vm)
        {
            bool success = false;
            if (vm != null)
            {
                string json = client.GetStringAsync(url + "del/" + vm.Id).Result;
                JObject obj = JObject.Parse(json);
                success = (bool)obj["OperationResult"];
            }
            SendMessage(success);
        }

        bool ApiEditDocument(DocumentViewModel vm, bool edit)
        {
            if (vm == null)
            {
                return false;
            }
            string myUrl = edit ? url + "mod" : url + "add";

            Dictionary<string, string> postData = new Dictionary<string, string>();
            if (edit)
            {
                postData.Add(nameof(vm.Id), vm.Id.ToString());
            }
            postData.Add(nameof(vm.Name), vm.Name.ToString());
            postData.Add(nameof(vm.AddDate), vm.AddDate.ToString());
            postData.Add(nameof(vm.Admin), vm.Admin.ToString());

            string json = client.PostAsync(myUrl, new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
            JObject obj = JObject.Parse(json);
            return (bool)obj["OperationResult"];
        }

        public void EditCar(DocumentViewModel vm, Func<DocumentViewModel, bool> editor)
        {
            DocumentViewModel clone = new DocumentViewModel();
            if (vm != null)
            {
                clone.CopyFrom(vm);
            }
            bool? success = editor?.Invoke(clone);
            if (success == true)
            {
                if (vm != null)
                {
                    success = ApiEditDocument(clone, true);
                }
                else
                {
                    success = ApiEditDocument(clone, false);
                }
            }
            SendMessage(success == true);
        }

    }
}
