﻿
namespace DocumentStorage.JavaWeb
{
    using System;
    using System.Linq;
    using System.Xml.Linq;

    /// <summary>
    /// THe WebClient class.
    /// </summary>
    public class Client : IClient
    {
        /// <summary>
        /// The random user getter method.
        /// </summary>
        /// <returns>the id.</returns>
        public int GetRandomSysUser()
        {
            XDocument xDoc = XDocument.Load("http://localhost:8080/FFegyszeru/Lotto");
            return int.Parse(xDoc.Descendants("szam").First().Value);
        }
    }
}
