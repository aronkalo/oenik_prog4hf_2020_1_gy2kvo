﻿// <copyright file="IClient.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DocumentStorage.JavaWeb
{
    /// <summary>
    /// The Client Interface.
    /// </summary>
    public interface IClient
    {
        /// <summary>
        /// The random user getter method.
        /// </summary>
        /// <returns>the id.</returns>
        int GetRandomSysUser();
    }
}