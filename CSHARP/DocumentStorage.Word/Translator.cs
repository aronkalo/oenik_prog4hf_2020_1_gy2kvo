﻿// <copyright file="Translator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DocumentStorage.Word
{
    using System;
    using System.Net;
    using System.Web;
    using DocumentStorage.Word.Interfaces;

    /// <summary>
    /// The class to translate strings.
    /// </summary>
    public class Translator : ITranslator
    {
        /// <summary>
        /// The method to translate, the input string to the target Language.
        /// </summary>
        /// <param name="inputText">the text to translate.</param>
        /// <param name="sourceLang">The text language.</param>
        /// <param name="targetLang">The target language.</param>
        /// <returns>The translated Text.</returns>
        public string Translate(string inputText, string sourceLang, string targetLang)
        {
            var url = $"https://translate.googleapis.com/translate_a/single?client=gtx&sl={sourceLang}&tl={targetLang}&dt=t&q={HttpUtility.UrlEncode(inputText)}";
            var webClient = new WebClient
            {
                Encoding = System.Text.Encoding.UTF8,
            };
            var result = webClient.DownloadString(url);
            result = result.Substring(4, result.IndexOf("\"", 4, StringComparison.Ordinal) - 4);
            return result;
        }

        /// <summary>
        /// Gets the source Language of the input string.
        /// </summary>
        /// <param name="inputText">The input text.</param>
        /// <returns>The language of the input text.</returns>
        public string GetSourceLang(string inputText)
        {
            var url = $"https://translate.googleapis.com/translate_a/single?client=gtx&sl=auto&tl=yo&dt=t&q={HttpUtility.UrlEncode(inputText)}";
            var webClient = new WebClient
            {
                Encoding = System.Text.Encoding.UTF8,
            };
            var result = webClient.DownloadString(url);
            result = result.Substring(result.Length - 9, 2);
            return result;
        }
    }
}
