﻿// <copyright file="SpellChecker.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DocumentStorage.Word
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DocumentStorage.Word.Interfaces;

    /// <summary>
    /// This is the class to correct the misspelled words.
    /// </summary>
    public class SpellChecker : ISpellChecker
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SpellChecker"/> class.
        /// sets the words, and the language.
        /// </summary>
        /// <param name="words">the words to fill.</param>
        /// <param name="language">the language of the words.</param>
        public SpellChecker(IDictionary<string, int> words, string language)
        {
            this.WordSet = new List<Word>();
            foreach (var word in words)
            {
                this.WordSet.Add(new Word() { WordValue = word.Key, Rarity = word.Value });
            }

            this.Language = language;
        }

        /// <summary>
        /// Gets the language of the SpellChecker.
        /// </summary>
        public string Language { get; private set; }

        private List<Word> WordSet { get; set; }

        /// <summary>
        /// Configures the Rarity of the words.
        /// </summary>
        /// <param name="text">The input text.</param>
        public void ConfigureRarity(string text)
        {
            string[] words = text.ToLower().Replace('.', ' ').Replace(',', ' ').Replace('\n', ' ').Replace('\t', ' ').Replace('?', ' ').Replace('!', ' ').Split(' ');
            foreach (var word in words)
            {
                if (this.WordSet.Where(x => x.WordValue == word).Count() == 1)
                {
                    int idx = this.WordSet.IndexOf(this.WordSet.Where(x => x.WordValue == word).First());
                    int rarity = this.WordSet[idx].Rarity;
                    this.WordSet.Remove(this.WordSet[idx]);
                    this.WordSet.Add(new Word() { WordValue = word, Rarity = rarity + 1 });
                }
            }
        }

        /// <summary>
        /// Correct the input words.
        /// </summary>
        /// <param name="wordsToCheck">The input words.</param>
        /// <returns> the corrected words.</returns>
        public IEnumerable<string> CorrectWordSet(IEnumerable<string> wordsToCheck)
        {
            List<string> incorrectWords = new List<string>();
            foreach (var word in wordsToCheck)
            {
                if (this.WordSet.Where(x => x.WordValue == word).Count() < 1)
                {
                    incorrectWords.Add(word);
                }
            }

            IEnumerable<string> correctedWords = this.TryToCorrectWords(incorrectWords);
            return correctedWords;
        }

        private IEnumerable<string> TryToCorrectWords(IEnumerable<string> incorrectWords)
        {
            IEnumerable<string> retlist = new List<string>();
            List<string> helperlist = new List<string>();
            foreach (var word in incorrectWords)
            {
                helperlist.Add(this.TryCorrect(word));
            }

            retlist = helperlist;
            return retlist;
        }

        private string TryCorrect(string inWord)
        {
            char[] inWordArray = inWord.ToCharArray();
            List<Word> wordsCanBe = new List<Word>();
            foreach (Word word in this.WordSet.Where(x => x.WordValue.Length == inWord.Length)
                .OrderByDescending(y => y.Rarity))
            {
                int notEqual = 0;
                char[] wordSetArray = word.WordValue.ToCharArray();
                if (inWordArray.Length == wordSetArray.Length)
                {
                    for (int i = 0; i < wordSetArray.Length; i++)
                    {
                        if (inWordArray[i] != wordSetArray[i])
                        {
                            notEqual++;
                        }
                    }

                    if (inWord.Length < 4 && notEqual <= 1)
                    {
                        wordsCanBe.Add(word);
                    }
                    else if (notEqual <= 1)
                    {
                        wordsCanBe.Add(word);
                    }
                }
            }

            if (wordsCanBe.Count > 0)
            {
                inWord = wordsCanBe.OrderByDescending(x => x.Rarity).First().WordValue;
            }

            return inWord;
        }

        private struct Word
        {
            public string WordValue;
            public int Rarity;
        }
    }
}
