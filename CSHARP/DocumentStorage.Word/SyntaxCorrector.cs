﻿// <copyright file="SyntaxCorrector.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DocumentStorage.Word
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DocumentStorage.Word.Interfaces;

    /// <summary>
    /// The class to correct the syntax of the words.
    /// </summary>
    public class SyntaxCorrector : ISyntaxCorrector
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SyntaxCorrector"/> class.
        /// Fills the RuleSet of the Corrector.
        /// </summary>
        /// <param name="rules">The rules to fill.</param>
        public SyntaxCorrector(IDictionary<string, string> rules)
        {
            this.RuleSet = rules;
        }

        private IDictionary<string, string> RuleSet { get; set; }

        /// <summary>
        /// Corrects the syntax of the input word list.
        /// </summary>
        /// <param name="words">Input word list.</param>
        /// <returns>the corrected words.</returns>
        public IEnumerable<string> CorrectWordSet(IEnumerable<string> words)
        {
            if (this.RuleSet == null)
            {
                throw new Exception("The RuleSet of this object did not load corrrectly.");
            }

            List<string> retWords = new List<string>();
            foreach (var word in words)
            {
                retWords.Add(this.CorrectSyntax(word));
            }

            return retWords;
        }

        /// <summary>
        /// The method to modify the ruleset of this object.
        /// </summary>
        /// <param name="newRules">The new RuleSet.</param>
        public void ModifyRuleSet(IDictionary<string, string> newRules)
        {
            this.RuleSet = newRules;
        }

        /// <summary>
        /// Adds a rule to the ruleset.
        /// </summary>
        /// <param name="sourceChar">The source character.</param>
        /// <param name="targetChar">The target character.</param>
        public void AddRule(char sourceChar, char targetChar)
        {
            if (!this.RuleSet.Contains(new KeyValuePair<string, string>(sourceChar.ToString(), targetChar.ToString())))
            {
                this.RuleSet.Add(new KeyValuePair<string, string>(sourceChar.ToString(), targetChar.ToString()));
            }
        }

        private string CorrectSyntax(string word)
        {
            string helpString = string.Empty;
            char[] charArray = word.ToCharArray();
            for (int i = 0; i < charArray.Length; i++)
            {
                char current = charArray[i];
                char before = ' ';
                char next = ' ';
                if (i < charArray.Length - 1)
                {
                    next = charArray[i + 1];
                }

                if (i > 0)
                {
                    before = charArray[i - 1];
                }

                if (this.IsOther(current, next, before))
                {
                    if (this.RuleSet.Keys.Contains(current.ToString()))
                    {
                        current = Convert.ToChar(this.RuleSet.Where(x => x.Key.Equals(current.ToString())).Select(y => y.Value).First());
                    }
                }

                helpString += current;
            }

            return helpString;
        }

        private bool IsOther(char current, char next, char before)
        {
            if (char.IsLetter(current))
            {
                if ((char.IsNumber(next) && !char.IsLetter(before)) ||
                    (char.IsNumber(before) && !char.IsLetter(next)))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else if (char.IsNumber(current))
            {
                if ((!char.IsNumber(next) && char.IsLetter(before)) ||
                   (!char.IsNumber(before) && char.IsLetter(next)))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            return false;
        }
    }
}
