﻿// <copyright file="ITranslator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DocumentStorage.Word.Interfaces
{
    /// <summary>
    /// The interface to the Translator class.
    /// </summary>
    public interface ITranslator
    {
        /// <summary>
        /// translates the input text to the targetlanguage.
        /// </summary>
        /// <param name="inputText">The input text.</param>
        /// <param name="sourceLang">The source language.</param>
        /// <param name="targetLang">The target language.</param>
        /// <returns>The translated text.</returns>
        string Translate(string inputText, string sourceLang, string targetLang);

        /// <summary>
        /// Gets the language of the input text.
        /// </summary>
        /// <param name="inputText">The text to get the language.</param>
        /// <returns>The text language.</returns>
        string GetSourceLang(string inputText);
    }
}
