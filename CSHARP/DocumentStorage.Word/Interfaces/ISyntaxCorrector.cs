﻿// <copyright file="ISyntaxCorrector.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DocumentStorage.Word.Interfaces
{
    using System.Collections.Generic;

    /// <summary>
    /// The interface of the SyntaxCorrector class.
    /// </summary>
    public interface ISyntaxCorrector
    {
        /// <summary>
        /// Corrects the input words.
        /// </summary>
        /// <param name="words">the input words.</param>
        /// <returns>the corrected words.</returns>
        IEnumerable<string> CorrectWordSet(IEnumerable<string> words);

        /// <summary>
        /// Modifies the ruleset of this object.
        /// </summary>
        /// <param name="newRules">the new rules.</param>
        void ModifyRuleSet(IDictionary<string, string> newRules);

        /// <summary>
        /// Adds a new rule to the RuleSet of this object.
        /// </summary>
        /// <param name="sourceChar">Char from.</param>
        /// <param name="targetChar">Char to.</param>
        void AddRule(char sourceChar, char targetChar);
    }
}
