﻿// <copyright file="ISpellChecker.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DocumentStorage.Word.Interfaces
{
    using System.Collections.Generic;

    /// <summary>
    /// The SpellChecker class Interface.
    /// </summary>
    public interface ISpellChecker
    {
        /// <summary>
        /// Helps to configure the word files for better performance.
        /// </summary>
        /// <param name="text">the config text.</param>
        void ConfigureRarity(string text);

        /// <summary>
        /// Tries to correct the input texts.
        /// </summary>
        /// <param name="wordsToCorrect">The words to correct.</param>
        /// <returns>The corrected words.</returns>
        IEnumerable<string> CorrectWordSet(IEnumerable<string> wordsToCorrect);
    }
}
