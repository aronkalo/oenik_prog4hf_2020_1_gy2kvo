﻿// <copyright file="IRepoDatabase.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DocumentStorage.Repository.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using DocumentStorage.Entities;

    /// <summary>
    /// The Database Interface.
    /// </summary>
    public interface IRepoDatabase
    {
        /// <summary>
        /// On startup events.
        /// </summary>
        void Startup();

        /// <summary>
        /// Inserts a page to the DB.
        /// </summary>
        /// <param name="image">The image to insert.</param>
        /// <param name="filename">The filename where the image is loacted.</param>
        /// <param name="user">The action DB user.</param>
        /// <param name="id">The id of the object.</param>
        /// <returns>the id of the object. </returns>
        int Insertpage(Bitmap image, string filename, string user, int id);

        /// <summary>
        /// Inserts a line into the DB.
        /// </summary>
        /// <param name="value">The value of the line.</param>
        /// <param name="user">The user of activty.</param>
        /// <param name="page_id">The page which contains the line.</param>
        /// <param name="id">The id of the word.</param>
        /// <returns>the id.</returns>
        int Insertline(string value, string user, int page_id, int id);

        /// <summary>
        /// Inserts a word into the DB.
        /// </summary>
        /// <param name="rectangle">The word position.</param>
        /// <param name="value">The value of the word.</param>
        /// <param name="user">The user.</param>
        /// <param name="line_id">The line id.</param>
        /// <param name="id">the word id.</param>
        /// <returns>the id.</returns>
        int Insertword(Rectangle rectangle, string value, string user, int line_id, int id);

        /// <summary>
        /// Insert a rule into the DB.
        /// </summary>
        /// <param name="wrongChar">the wrong char.</param>
        /// <param name="rightChar">the correct char.</param>
        /// <param name="user_id">the user.</param>
        /// <param name="id">the id.</param>
        /// <param name="type">the type.</param>
        void Insertrule(string wrongChar, string rightChar, int user_id, int id, int type);

        /// <summary>
        /// Inserts a user into the DB.
        /// </summary>
        /// <param name="name">The name of the user.</param>
        /// <param name="admin">Is admin.</param>
        /// <param name="user_id">The id of the user.</param>
        void Insertuser(string name, bool admin, int user_id);

        /// <summary>
        /// Gets all rules.
        /// </summary>
        /// <returns>The rules.</returns>
        IEnumerable<ITupleObj<int, string, string, int, DateTime>> Getrules();

        /// <summary>
        /// Gets all users.
        /// </summary>
        /// <param name="admin">Is admin.</param>
        /// <returns>The users.</returns>
        IEnumerable<ITupleObj<int, string, string, DateTime>> Getusers(bool admin);

        /// <summary>
        /// Gets the pages.
        /// </summary>
        /// <returns>The pages.</returns>
        IEnumerable<ITupleObj<int, string, string, DateTime, int, int>> Getpages();

        /// <summary>
        /// Gets the lines.
        /// </summary>
        /// <returns>The lines.</returns>
        IEnumerable<ITupleObj<int, string, DateTime, int, int, int, int, int, int, string>> Getlines();

        /// <summary>
        /// Gets the words.
        /// </summary>
        /// <returns>The words.</returns>
        IEnumerable<ITupleObj<int, string, DateTime, int, int, int, int, int, int, string>> Getwords();

        /// <summary>
        /// Returns a pages liked to a user.
        /// </summary>
        /// <param name="user_id">The user.</param>
        /// <returns>The pages.</returns>
        List<ITupleObj<int, string, string, DateTime, int, int, List<ITupleObj<int, string, DateTime, int, int, int, int, int, int, string, List<ITupleObj<int, string, DateTime, int, int, int, int, int, int, string>>>>>> GetPagesByUser(int user_id);

        /// <summary>
        /// Returns the count ot the rules to a user.
        /// </summary>
        /// <returns>The rules.</returns>
        IEnumerable<ITupleObj<string, int>> GetRuleCountForUsers();

        /// <summary>
        /// Updates a user in the DB.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="name">The name.</param>
        /// <param name="admin">Is admin.</param>
        void UpdateUser(int id, string name, bool admin);

        /// <summary>
        /// Updates a rule in the DB.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="rec_value">The rec_value.</param>
        /// <param name="cor_value">The cor_value.</param>
        /// <param name="type">The type.</param>
        void UpdateRule(int id, string rec_value, string cor_value, int type);

        /// <summary>
        /// Updates a word in the DB.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="x">The x cord.</param>
        /// <param name="y">The y cord.</param>
        /// <param name="width">The witdth.</param>
        /// <param name="height">The height.</param>
        /// <param name="value">The value.</param>
        void UpdateWord(int id, int x, int y, int width, int height, string value);

        /// <summary>
        /// Deletes a user from the DB.
        /// </summary>
        /// <param name="id">The id.</param>
        void DeleteUser(int id);

        /// <summary>
        /// Deletes a page from the DB.
        /// </summary>
        /// <param name="id">The id.</param>
        void DeletePage(int id);

        /// <summary>
        /// Deletes a rule from the DB.
        /// </summary>
        /// <param name="id">The id.</param>
        void DeleteRule(int id);

        /// <summary>
        /// Random User.
        /// </summary>
        /// <returns>The id.</returns>
        int GetUserName();
    }
}
