﻿// <copyright file="IRepoTesseractCop.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DocumentStorage.Repository.Interfaces
{
    using System.Collections.Generic;
    using System.Drawing;
    using DocumentStorage.Entities;

    /// <summary>
    /// The TesseractCop interface of the Repository layer.
    /// </summary>
    public interface IRepoTesseractCop
    {
        /// <summary>
        /// The interface of the TeseractCop.
        /// </summary>
        /// <param name="workDir">The tesseract.exe location.</param>
        /// <param name="confFile">The config file.</param>
        /// <param name="images">The input images.</param>
        /// <returns>The output pages.</returns>
        List<ITupleObj<Bitmap, List<ITupleObj<int, List<ITupleObj<Rectangle, string, int>>>>>> StructuredProcess(string workDir, string confFile, IEnumerable<Bitmap> images);

        /// <summary>
        /// The Word highlighter method.
        /// </summary>
        /// <param name="pages">The input pages.</param>
        /// <param name="sewords">The input matching words.</param>
        /// <param name="color">The bracket color.</param>
        /// <returns>The output pages.</returns>
        List<ITupleObj<Bitmap, List<ITupleObj<int, List<ITupleObj<Rectangle, string, int>>>>>> HighLightWords(List<ITupleObj<Bitmap, List<ITupleObj<int, List<ITupleObj<Rectangle, string, int>>>>>> pages, List<ITupleObj<Rectangle, string, int>> sewords, Color color);
    }
}
