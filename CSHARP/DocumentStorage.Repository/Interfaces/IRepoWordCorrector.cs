﻿// <copyright file="IRepoWordCorrector.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DocumentStorage.Repository.Interfaces
{
    using System.Collections.Generic;

    /// <summary>
    /// The word corrector interface to the Logic layer.
    /// </summary>
    public interface IRepoWordCorrector
    {
        /// <summary>
        /// Modifies the ruleset of this object.
        /// </summary>
        /// <param name="newRules">These will be the new rules.</param>
        void ModifyRuleSet(IDictionary<string, string> newRules);

        /// <summary>
        /// Adds a rule to the ruleset of the object.
        /// </summary>
        /// <param name="sourceChar">From char.</param>
        /// <param name="targetChar">To char.</param>
        void AddRule(char sourceChar, char targetChar);

        /// <summary>
        /// Corrects the parameterized WordSet anr returns it.
        /// </summary>
        /// <param name="words">The input wordset.</param>
        /// <returns>The corrected wordset.</returns>
        IEnumerable<string> CorrectWordSet(IEnumerable<string> words);
    }
}
