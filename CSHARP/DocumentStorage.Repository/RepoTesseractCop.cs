﻿// <copyright file="RepoTesseractCop.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DocumentStorage.Repository
{
    using System.Collections.Generic;
    using System.Drawing;
    using DocumentStorage.Entities;
    using DocumentStorage.OCR.Interfaces;
    using DocumentStorage.OCR.Tesseract;
    using DocumentStorage.Repository.Interfaces;

    /// <summary>
    /// The repository layer OCR Engine.
    /// </summary>
    public class RepoTesseractCop : IRepoTesseractCop
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RepoTesseractCop"/> class.
        /// </summary>
        public RepoTesseractCop()
        {
            this.TesseractCop = new TesseractCop();
        }

        private ITesseractCop TesseractCop { get; set; }

        /// <summary>
        /// Structured processing of OCR.
        /// </summary>
        /// <param name="workDir">The tesseract.exe location.</param>
        /// <param name="confFile">The config file.</param>
        /// <param name="images">The input images.</param>
        /// <returns>The Processed pages.</returns>
        public List<ITupleObj<Bitmap, List<ITupleObj<int, List<ITupleObj<Rectangle, string, int>>>>>> StructuredProcess(string workDir, string confFile, IEnumerable<Bitmap> images)
        {
            return this.TesseractCop.StructuredProcess(workDir, confFile, images);
        }

        /// <summary>
        /// The method of Highlighting the input pages matching words.
        /// </summary>
        /// <param name="pages">the Input pages.</param>
        /// <param name="sewords">The input words.</param>
        /// <param name="color">The color of the bracket.</param>
        /// <returns>The output pages.</returns>
        public List<ITupleObj<Bitmap, List<ITupleObj<int, List<ITupleObj<Rectangle, string, int>>>>>> HighLightWords(List<ITupleObj<Bitmap, List<ITupleObj<int, List<ITupleObj<Rectangle, string, int>>>>>> pages, List<ITupleObj<Rectangle, string, int>> sewords, Color color)
        {
            return this.TesseractCop.HighLightWords(pages, sewords, color);
        }
    }
}
