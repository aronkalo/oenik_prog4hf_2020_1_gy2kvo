﻿// <copyright file="RepoWordCorrector.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DocumentStorage.Repository
{
    using System.Collections.Generic;
    using DocumentStorage.Word;
    using DocumentStorage.Word.Interfaces;

    /// <summary>
    /// The container repo class to the wordcorrector.
    /// </summary>
    public class RepoWordCorrector
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RepoWordCorrector"/> class.
        /// Initializes the interface objects.
        /// </summary>
        /// <param name="ruleSet"> the syntaxCorrector ruleSet.</param>
        /// <param name="words">The spellChecker words.</param>
        /// <param name="lang">The spellChecker language.</param>
        public RepoWordCorrector(IDictionary<string, string> ruleSet, IDictionary<string, int> words, string lang)
        {
            this.SyntaxCorrector = new SyntaxCorrector(ruleSet);

            this.SpellChecker = new SpellChecker(words, lang);

            this.Translator = new Translator();
        }

        /// <summary>
        /// Gets or sets the SyntaxCorrector interface.
        /// </summary>
        private ISyntaxCorrector SyntaxCorrector { get; set; }

        /// <summary>
        /// Gets or sets the SpellChecker interface.
        /// </summary>
        private ISpellChecker SpellChecker { get; set; }

        /// <summary>
        /// Gets or sets the Translator interface.
        /// </summary>
        private ITranslator Translator { get; set; }

        /// <summary>
        /// Corrects the input wordSet.
        /// </summary>
        /// <param name="words">The input words.</param>
        /// <returns>The corrected words.</returns>
        public IEnumerable<string> CorrectWordSet(IEnumerable<string> words)
        {
            return this.SyntaxCorrector.CorrectWordSet(words);
        }

        /// <summary>
        /// Modiffies the rules with another ruleSet.
        /// </summary>
        /// <param name="newRules">The new rules.</param>
        public void ModifyRuleSet(IDictionary<string, string> newRules)
        {
            this.SyntaxCorrector.ModifyRuleSet(newRules);
        }

        /// <summary>
        /// Adds a rule to the ruleSet.
        /// </summary>
        /// <param name="sourceChar">From char.</param>
        /// <param name="targetChar">To char.</param>
        public void AddRule(char sourceChar, char targetChar)
        {
            this.SyntaxCorrector.AddRule(sourceChar, targetChar);
        }
    }
}
