﻿// <copyright file="RepoDatabase.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DocumentStorage.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using DocumentStorage.Data;
    using DocumentStorage.Entities;
    using DocumentStorage.JavaWeb;
    using DocumentStorage.Repository.Interfaces;

    /// <summary>
    /// The repo Layer Database.
    /// </summary>
    public class RepoDatabase : IRepoDatabase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RepoDatabase"/> class.
        /// </summary>
        public RepoDatabase()
        {
            this.Database = new Database();
            this.Client = new Client();
        }

        private IDatabase Database { get; set; }

        private IClient Client { get; set; }

        /// <summary>
        /// On startup events.
        /// </summary>
        public void Startup()
        {
            this.Database.Startup();
        }

        /// <summary>
        /// Inserts a page to the DB.
        /// </summary>
        /// <param name="image"> The image.</param>
        /// <param name="filename">The location of the image.</param>
        /// <param name="user">The activity user.</param>
        /// <param name="id">the Id.</param>
        /// <returns>The page id.</returns>
        public int Insertpage(Bitmap image, string filename, string user, int id = 0)
        {
            return this.Database.Insertpage(image, filename, user, id);
        }

        /// <summary>
        /// Inserts a line into the DB.
        /// </summary>
        /// <param name="value">the string value. </param>
        /// <param name="user">The activity user.</param>
        /// <param name="page_id">The page id.</param>
        /// <param name="id">The id.</param>
        /// <returns>the object id.</returns>
        public int Insertline(string value, string user, int page_id = 0, int id = 0)
        {
            return this.Database.Insertline(value, user, page_id, id);
        }

        /// <summary>
        /// Inserts a word into the DB.
        /// </summary>
        /// <param name="rectangle">The position.</param>
        /// <param name="value">The string value.</param>
        /// <param name="user">The activity user.</param>
        /// <param name="line_id">The id of the line containing this word.</param>
        /// <param name="id">The word id.</param>
        /// <returns>The id.</returns>
        public int Insertword(Rectangle rectangle, string value, string user, int line_id = 0, int id = 0)
        {
            return this.Database.Insertword(rectangle, value, user, line_id, id);
        }

        /// <summary>
        /// Inserts a Rule into the DB.
        /// </summary>
        /// <param name="wrongChar">wrongchar.</param>
        /// <param name="rightChar">The right char.</param>
        /// <param name="user_id">The activity user.</param>
        /// <param name="id">The id.</param>
        /// <param name="type">The rule type.</param>
        public void Insertrule(string wrongChar, string rightChar, int user_id, int id = 0, int type = 0)
        {
            this.Database.Insertrule(wrongChar, rightChar, user_id, id, type);
        }

        /// <summary>
        /// Inserts a user into th DB.
        /// </summary>
        /// <param name="name">The username.</param>
        /// <param name="admin">Is admin.</param>
        /// <param name="user_id">Th id of the user.</param>
        public void Insertuser(string name, bool admin, int user_id)
        {
            this.Database.Insertuser(name, admin, user_id);
        }

        /// <summary>
        /// Gets the users.
        /// </summary>
        /// <param name="admin">is admin.</param>
        /// <returns>The user.</returns>
        public IEnumerable<ITupleObj<int, string, string, DateTime>> Getusers(bool admin = false)
        {
            return this.Database.Getusers(admin);
        }

        /// <summary>
        /// Gets the rules from the DB.
        /// </summary>
        /// <returns>The ruleset.</returns>
        public IEnumerable<ITupleObj<int, string, string, int, DateTime>> Getrules()
        {
            return this.Database.Getrules();
        }

        /// <summary>
        /// Gets the pages.
        /// </summary>
        /// <returns>The pages.</returns>
        public IEnumerable<ITupleObj<int, string, string, DateTime, int, int>> Getpages()
        {
            return this.Database.Getpages();
        }

        /// <summary>
        /// Gets the lines.
        /// </summary>
        /// <returns>The lines.</returns>
        public IEnumerable<ITupleObj<int, string, DateTime, int, int, int, int, int, int, string>> Getlines()
        {
            return this.Database.Getlines();
        }

        /// <summary>
        /// Gets the words.
        /// </summary>
        /// <returns>the words.</returns>
        public IEnumerable<ITupleObj<int, string, DateTime, int, int, int, int, int, int, string>> Getwords()
        {
            return this.Database.Getwords();
        }

        /// <summary>
        /// Return the pages linked to a user.
        /// </summary>
        /// <param name="user_id">the user id.</param>
        /// <returns>The pages.</returns>
        public List<ITupleObj<int, string, string, DateTime, int, int, List<ITupleObj<int, string, DateTime, int, int, int, int, int, int, string, List<ITupleObj<int, string, DateTime, int, int, int, int, int, int, string>>>>>> GetPagesByUser(int user_id)
        {
            return this.Database.GetPagesByUser(user_id);
        }

        /// <summary>
        /// Return the rules linked to a user.
        /// </summary>
        /// <returns>The rules.</returns>
        public IEnumerable<ITupleObj<string, int>> GetRuleCountForUsers()
        {
            return this.Database.GetRuleCountForUsers();
        }

        /// <summary>
        /// Updates a user in the DB.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="name">The name.</param>
        /// <param name="admin">Is admin.</param>
        public void UpdateUser(int id, string name, bool admin)
        {
            this.Database.UpdateUser(id, name, admin);
        }

        /// <summary>
        /// Updates a rule in the DB.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="rec_value">The rec_value.</param>
        /// <param name="cor_value">The cor_value.</param>
        /// <param name="type">The type.</param>
        public void UpdateRule(int id, string rec_value, string cor_value, int type)
        {
            this.Database.UpdateRule(id, rec_value, cor_value, type);
        }

        /// <summary>
        /// Updates a word in the DB.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="x">The x cord.</param>
        /// <param name="y">The y cord.</param>
        /// <param name="width">The witdth.</param>
        /// <param name="height">The height.</param>
        /// <param name="value">The value.</param>
        public void UpdateWord(int id, int x, int y, int width, int height, string value)
        {
            this.Database.UpdateWord(id, x, y, width, height, value);
        }

        /// <summary>
        /// Deletes a user from the DB.
        /// </summary>
        /// <param name="id">The id.</param>
        public void DeleteUser(int id)
        {
            this.Database.DeleteUser(id);
        }

        /// <summary>
        /// Deletes a page from the DB.
        /// </summary>
        /// <param name="id">The id.</param>
        public void DeletePage(int id)
        {
            this.Database.DeletePage(id);
        }

        /// <summary>
        /// Deletes a rule from the DB.
        /// </summary>
        /// <param name="id">The id.</param>
        public void DeleteRule(int id)
        {
            this.Database.DeleteRule(id);
        }

        /// <summary>
        /// Random User.
        /// </summary>
        /// <returns>The id.</returns>
        public int GetUserName()
        {
            return this.Client.GetRandomSysUser();
        }
    }
}
