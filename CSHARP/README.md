**DOCUMENT STORAGE SYSTEM CSHARP**
---

## Functons

1. List Users
2. List the pages of a specified user
3. Set the ImageFolder
4. Set the tesseract.exe folder
5. Process Files from ImageFolder
6. Add new rule
7. Add new user
8. update user
9. update rule
10. update word
11. delete user
12. delete page
13. delete rule
14. Get data from JAVA endpoint
15. Search for word in specified page
16. List rulecount for users
17. Get a user by ID
18. Get a page by iD
19. Get a line by ID
20. Get a word by ID
21. Exit from the application
