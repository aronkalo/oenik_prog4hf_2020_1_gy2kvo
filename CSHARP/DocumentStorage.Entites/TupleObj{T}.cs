﻿// <copyright file="TupleObj{T}.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DocumentStorage.Entities
{
    /// <summary>
    /// The class of the TupleObj.
    /// </summary>
    /// <typeparam name="T"> first param.</typeparam>
    public class TupleObj<T> : ITupleObj<T>
    {
        /// <summary>
        /// Gets or sets the first item.
        /// </summary>
        public T Item1 { get; set; }
    }
}