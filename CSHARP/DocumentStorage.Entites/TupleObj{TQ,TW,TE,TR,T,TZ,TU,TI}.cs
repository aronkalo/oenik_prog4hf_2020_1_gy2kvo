﻿// <copyright file="TupleObj{TQ,TW,TE,TR,T,TZ,TU,TI}.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DocumentStorage.Entities
{
    /// <summary>
    /// The class of the TupleObj.
    /// </summary>
    /// <typeparam name="TQ"> first param.</typeparam>
    /// <typeparam name="TW">second param.</typeparam>
    /// <typeparam name="TE">third param.</typeparam>
    /// <typeparam name="TR">fourth param.</typeparam>
    /// <typeparam name="T">fifth param.</typeparam>
    /// <typeparam name="TZ">6. param.</typeparam>
    /// <typeparam name="TU">seventh param.</typeparam>
    /// <typeparam name="TI">8. param.</typeparam>
    public class TupleObj<TQ, TW, TE, TR, T, TZ, TU, TI> : ITupleObj<TQ, TW, TE, TR, T, TZ, TU, TI>
    {
        /// <summary>
        /// Gets or sets the first item.
        /// </summary>
        public TQ Item1 { get; set; }

        /// <summary>
        /// Gets or sets the 2. item.
        /// </summary>
        public TW Item2 { get; set; }

        /// <summary>
        /// Gets or sets the 3. item.
        /// </summary>
        public TE Item3 { get; set; }

        /// <summary>
        /// Gets or sets the 4.item.
        /// </summary>
        public TR Item4 { get; set; }

        /// <summary>
        /// Gets or sets the 5. item.
        /// </summary>
        public T Item5 { get; set; }

        /// <summary>
        /// Gets or sets the 6. item.
        /// </summary>
        public TZ Item6 { get; set; }

        /// <summary>
        /// Gets or sets the 7. item.
        /// </summary>
        public TU Item7 { get; set; }

        /// <summary>
        /// Gets or sets the 8.item.
        /// </summary>
        public TI Item8 { get; set; }
    }
}