﻿// <copyright file="TupleObj{T,TW,TE}.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DocumentStorage.Entities
{
    /// <summary>
    /// The class of the TupleObj.
    /// </summary>
    /// <typeparam name="T"> first param.</typeparam>
    /// <typeparam name="TW">second param.</typeparam>
    /// <typeparam name="TE">third param.</typeparam>
    public class TupleObj<T, TW, TE> : ITupleObj<T, TW, TE>
    {
        /// <summary>
        /// Gets or sets the first item.
        /// </summary>
        public T Item1 { get; set; }

        /// <summary>
        /// Gets or sets the 2. item.
        /// </summary>
        public TW Item2 { get; set; }

        /// <summary>
        /// Gets or sets the 3. item.
        /// </summary>
        public TE Item3 { get; set; }
    }
}