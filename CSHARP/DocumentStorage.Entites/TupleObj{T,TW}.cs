﻿// <copyright file="TupleObj{T,TW}.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DocumentStorage.Entities
{
    /// <summary>
    /// The class of the TupleObj.
    /// </summary>
    /// <typeparam name="T"> first param.</typeparam>
    /// <typeparam name="TW">second param.</typeparam>
    public class TupleObj<T, TW> : ITupleObj<T, TW>
    {
        /// <summary>
        /// Gets or sets the first item.
        /// </summary>
        public T Item1 { get; set; }

        /// <summary>
        /// Gets or sets the 2. item.
        /// </summary>
        public TW Item2 { get; set; }
    }
}