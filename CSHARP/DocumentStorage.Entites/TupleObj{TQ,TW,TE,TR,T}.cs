﻿// <copyright file="TupleObj{TQ,TW,TE,TR,T}.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DocumentStorage.Entities
{
    /// <summary>
    /// The class of the TupleObj.
    /// </summary>
    /// <typeparam name="TQ"> first param.</typeparam>
    /// <typeparam name="TW">second param.</typeparam>
    /// <typeparam name="TE">third param.</typeparam>
    /// <typeparam name="TR">fourth param.</typeparam>
    /// <typeparam name="T">fifth param.</typeparam>
    public class TupleObj<TQ, TW, TE, TR, T> : ITupleObj<TQ, TW, TE, TR, T>
    {
        /// <summary>
        /// Gets or sets the first item.
        /// </summary>
        public TQ Item1 { get; set; }

        /// <summary>
        /// Gets or sets the 2. item.
        /// </summary>
        public TW Item2 { get; set; }

        /// <summary>
        /// Gets or sets the 3. item.
        /// </summary>
        public TE Item3 { get; set; }

        /// <summary>
        /// Gets or sets the 4.item.
        /// </summary>
        public TR Item4 { get; set; }

        /// <summary>
        /// Gets or sets the 5. item.
        /// </summary>
        public T Item5 { get; set; }
    }
}