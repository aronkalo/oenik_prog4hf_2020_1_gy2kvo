﻿// <copyright file="ITupleObj.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DocumentStorage.Entities
{
    /// <summary>
    /// The interface of the TupleObj.
    /// </summary>
    /// <typeparam name="TQ"> first param.</typeparam>
    /// <typeparam name="TW">second param.</typeparam>
    /// <typeparam name="TE">third param.</typeparam>
    /// <typeparam name="TR">fourth param.</typeparam>
    /// <typeparam name="T">fifth param.</typeparam>
    /// <typeparam name="TZ">6. param.</typeparam>
    /// <typeparam name="TU">seventh param.</typeparam>
    /// <typeparam name="TI">8. param.</typeparam>
    /// <typeparam name="TO">9. param.</typeparam>
    /// <typeparam name="TP">10. param.</typeparam>
    /// <typeparam name="TA">11. param.</typeparam>
    /// <typeparam name="TS">12. param.</typeparam>
    public interface ITupleObj<TQ, TW, TE, TR, T, TZ, TU, TI, TO, TP, TA, TS>
    {
        /// <summary>
        /// Gets or sets the first item.
        /// </summary>
        TQ Item1 { get; set; }

        /// <summary>
        /// Gets or sets the 2. item.
        /// </summary>
        TW Item2 { get; set; }

        /// <summary>
        /// Gets or sets the 3. item.
        /// </summary>
        TE Item3 { get; set; }

        /// <summary>
        /// Gets or sets the 4.item.
        /// </summary>
        TR Item4 { get; set; }

        /// <summary>
        /// Gets or sets the 5. item.
        /// </summary>
        T Item5 { get; set; }

        /// <summary>
        /// Gets or sets the 6. item.
        /// </summary>
        TZ Item6 { get; set; }

        /// <summary>
        /// Gets or sets the 7. item.
        /// </summary>
        TU Item7 { get; set; }

        /// <summary>
        /// Gets or sets the 8.item.
        /// </summary>
        TI Item8 { get; set; }

        /// <summary>
        /// Gets or sets the 9. item.
        /// </summary>
        TO Item9 { get; set; }

        /// <summary>
        /// Gets or sets the 10. item.
        /// </summary>
        TP Item10 { get; set; }

        /// <summary>
        /// Gets or sets the 11. item.
        /// </summary>
        TA Item11 { get; set; }

        /// <summary>
        /// Gets or sets the 12. item.
        /// </summary>
        TS Item12 { get; set; }
    }

    /// <summary>
    /// The interface of the TupleObj.
    /// </summary>
    /// <typeparam name="TQ"> first param.</typeparam>
    /// <typeparam name="TW">second param.</typeparam>
    /// <typeparam name="TE">third param.</typeparam>
    /// <typeparam name="TR">fourth param.</typeparam>
    /// <typeparam name="T">fifth param.</typeparam>
    /// <typeparam name="TZ">6. param.</typeparam>
    /// <typeparam name="TU">seventh param.</typeparam>
    /// <typeparam name="TI">8. param.</typeparam>
    /// <typeparam name="TO">9. param.</typeparam>
    /// <typeparam name="TP">10. param.</typeparam>
    /// <typeparam name="TA">11. param.</typeparam>
    public interface ITupleObj<TQ, TW, TE, TR, T, TZ, TU, TI, TO, TP, TA>
    {
        /// <summary>
        /// Gets or sets the first item.
        /// </summary>
        TQ Item1 { get; set; }

        /// <summary>
        /// Gets or sets the 2. item.
        /// </summary>
        TW Item2 { get; set; }

        /// <summary>
        /// Gets or sets the 3. item.
        /// </summary>
        TE Item3 { get; set; }

        /// <summary>
        /// Gets or sets the 4.item.
        /// </summary>
        TR Item4 { get; set; }

        /// <summary>
        /// Gets or sets the 5. item.
        /// </summary>
        T Item5 { get; set; }

        /// <summary>
        /// Gets or sets the 6. item.
        /// </summary>
        TZ Item6 { get; set; }

        /// <summary>
        /// Gets or sets the 7. item.
        /// </summary>
        TU Item7 { get; set; }

        /// <summary>
        /// Gets or sets the 8.item.
        /// </summary>
        TI Item8 { get; set; }

        /// <summary>
        /// Gets or sets the 9. item.
        /// </summary>
        TO Item9 { get; set; }

        /// <summary>
        /// Gets or sets the 10. item.
        /// </summary>
        TP Item10 { get; set; }

        /// <summary>
        /// Gets or sets the 11. item.
        /// </summary>
        TA Item11 { get; set; }
    }

    /// <summary>
    /// The interface of the TupleObj.
    /// </summary>
    /// <typeparam name="T"> first param.</typeparam>
    /// <typeparam name="TW">second param.</typeparam>
    /// <typeparam name="TE">third param.</typeparam>
    /// <typeparam name="TR">fourth param.</typeparam>
    /// <typeparam name="TQ">fifth param.</typeparam>
    /// <typeparam name="TZ">6. param.</typeparam>
    /// <typeparam name="TU">seventh param.</typeparam>
    /// <typeparam name="TI">8. param.</typeparam>
    /// <typeparam name="TO">9. param.</typeparam>
    /// <typeparam name="TP">10. param.</typeparam>
    public interface ITupleObj<T, TW, TE, TR, TQ, TZ, TU, TI, TO, TP>
    {
        /// <summary>
        /// Gets or sets the first item.
        /// </summary>
        TQ Item1 { get; set; }

        /// <summary>
        /// Gets or sets the 2. item.
        /// </summary>
        TW Item2 { get; set; }

        /// <summary>
        /// Gets or sets the 3. item.
        /// </summary>
        TE Item3 { get; set; }

        /// <summary>
        /// Gets or sets the 4.item.
        /// </summary>
        TR Item4 { get; set; }

        /// <summary>
        /// Gets or sets the 5. item.
        /// </summary>
        T Item5 { get; set; }

        /// <summary>
        /// Gets or sets the 6. item.
        /// </summary>
        TZ Item6 { get; set; }

        /// <summary>
        /// Gets or sets the 7. item.
        /// </summary>
        TU Item7 { get; set; }

        /// <summary>
        /// Gets or sets the 8.item.
        /// </summary>
        TI Item8 { get; set; }

        /// <summary>
        /// Gets or sets the 9. item.
        /// </summary>
        TO Item9 { get; set; }

        /// <summary>
        /// Gets or sets the 10. item.
        /// </summary>
        TP Item10 { get; set; }
    }

    /// <summary>
    /// The interface of the TupleObj.
    /// </summary>
    /// <typeparam name="TQ"> first param.</typeparam>
    /// <typeparam name="TW">second param.</typeparam>
    /// <typeparam name="TE">third param.</typeparam>
    /// <typeparam name="TR">fourth param.</typeparam>
    /// <typeparam name="T">fifth param.</typeparam>
    /// <typeparam name="TZ">6. param.</typeparam>
    /// <typeparam name="TU">seventh param.</typeparam>
    /// <typeparam name="TI">8. param.</typeparam>
    /// <typeparam name="TO">9. param.</typeparam>
    public interface ITupleObj<TQ, TW, TE, TR, T, TZ, TU, TI, TO>
    {
        /// <summary>
        /// Gets or sets the first item.
        /// </summary>
        TQ Item1 { get; set; }

        /// <summary>
        /// Gets or sets the 2. item.
        /// </summary>
        TW Item2 { get; set; }

        /// <summary>
        /// Gets or sets the 3. item.
        /// </summary>
        TE Item3 { get; set; }

        /// <summary>
        /// Gets or sets the 4.item.
        /// </summary>
        TR Item4 { get; set; }

        /// <summary>
        /// Gets or sets the 5. item.
        /// </summary>
        T Item5 { get; set; }

        /// <summary>
        /// Gets or sets the 6. item.
        /// </summary>
        TZ Item6 { get; set; }

        /// <summary>
        /// Gets or sets the 7. item.
        /// </summary>
        TU Item7 { get; set; }

        /// <summary>
        /// Gets or sets the 8.item.
        /// </summary>
        TI Item8 { get; set; }

        /// <summary>
        /// Gets or sets the 9. item.
        /// </summary>
        TO Item9 { get; set; }
    }

    /// <summary>
    /// The interface of the TupleObj.
    /// </summary>
    /// <typeparam name="TQ"> first param.</typeparam>
    /// <typeparam name="TW">second param.</typeparam>
    /// <typeparam name="TE">third param.</typeparam>
    /// <typeparam name="TR">fourth param.</typeparam>
    /// <typeparam name="T">fifth param.</typeparam>
    /// <typeparam name="TZ">6. param.</typeparam>
    /// <typeparam name="TU">seventh param.</typeparam>
    /// <typeparam name="TI">8. param.</typeparam>
    public interface ITupleObj<TQ, TW, TE, TR, T, TZ, TU, TI>
    {
        /// <summary>
        /// Gets or sets the first item.
        /// </summary>
        TQ Item1 { get; set; }

        /// <summary>
        /// Gets or sets the 2. item.
        /// </summary>
        TW Item2 { get; set; }

        /// <summary>
        /// Gets or sets the 3. item.
        /// </summary>
        TE Item3 { get; set; }

        /// <summary>
        /// Gets or sets the 4.item.
        /// </summary>
        TR Item4 { get; set; }

        /// <summary>
        /// Gets or sets the 5. item.
        /// </summary>
        T Item5 { get; set; }

        /// <summary>
        /// Gets or sets the 6. item.
        /// </summary>
        TZ Item6 { get; set; }

        /// <summary>
        /// Gets or sets the 7. item.
        /// </summary>
        TU Item7 { get; set; }

        /// <summary>
        /// Gets or sets the 8.item.
        /// </summary>
        TI Item8 { get; set; }
    }

    /// <summary>
    /// The interface of the TupleObj.
    /// </summary>
    /// <typeparam name="TQ"> first param.</typeparam>
    /// <typeparam name="TW">second param.</typeparam>
    /// <typeparam name="TE">third param.</typeparam>
    /// <typeparam name="TR">fourth param.</typeparam>
    /// <typeparam name="T">fifth param.</typeparam>
    /// <typeparam name="TZ">6. param.</typeparam>
    /// <typeparam name="TU">seventh param.</typeparam>
    public interface ITupleObj<TQ, TW, TE, TR, T, TZ, TU>
    {
        /// <summary>
        /// Gets or sets the first item.
        /// </summary>
        TQ Item1 { get; set; }

        /// <summary>
        /// Gets or sets the 2. item.
        /// </summary>
        TW Item2 { get; set; }

        /// <summary>
        /// Gets or sets the 3. item.
        /// </summary>
        TE Item3 { get; set; }

        /// <summary>
        /// Gets or sets the 4.item.
        /// </summary>
        TR Item4 { get; set; }

        /// <summary>
        /// Gets or sets the 5. item.
        /// </summary>
        T Item5 { get; set; }

        /// <summary>
        /// Gets or sets the 6. item.
        /// </summary>
        TZ Item6 { get; set; }

        /// <summary>
        /// Gets or sets the 7. item.
        /// </summary>
        TU Item7 { get; set; }
    }

    /// <summary>
    /// The interface of the TupleObj.
    /// </summary>
    /// <typeparam name="TQ"> first param.</typeparam>
    /// <typeparam name="TW">second param.</typeparam>
    /// <typeparam name="TE">third param.</typeparam>
    /// <typeparam name="TR">fourth param.</typeparam>
    /// <typeparam name="T">fifth param.</typeparam>
    /// <typeparam name="TZ">6. param.</typeparam>
    public interface ITupleObj<TQ, TW, TE, TR, T, TZ>
    {
        /// <summary>
        /// Gets or sets the first item.
        /// </summary>
        TQ Item1 { get; set; }

        /// <summary>
        /// Gets or sets the 2. item.
        /// </summary>
        TW Item2 { get; set; }

        /// <summary>
        /// Gets or sets the 3. item.
        /// </summary>
        TE Item3 { get; set; }

        /// <summary>
        /// Gets or sets the 4.item.
        /// </summary>
        TR Item4 { get; set; }

        /// <summary>
        /// Gets or sets the 5. item.
        /// </summary>
        T Item5 { get; set; }

        /// <summary>
        /// Gets or sets the 6. item.
        /// </summary>
        TZ Item6 { get; set; }
    }

    /// <summary>
    /// The interface of the TupleObj.
    /// </summary>
    /// <typeparam name="TQ"> first param.</typeparam>
    /// <typeparam name="TW">second param.</typeparam>
    /// <typeparam name="TE">third param.</typeparam>
    /// <typeparam name="TR">fourth param.</typeparam>
    /// <typeparam name="T">fifth param.</typeparam>
    public interface ITupleObj<TQ, TW, TE, TR, T>
    {
        /// <summary>
        /// Gets or sets the first item.
        /// </summary>
        TQ Item1 { get; set; }

        /// <summary>
        /// Gets or sets the 2. item.
        /// </summary>
        TW Item2 { get; set; }

        /// <summary>
        /// Gets or sets the 3. item.
        /// </summary>
        TE Item3 { get; set; }

        /// <summary>
        /// Gets or sets the 4.item.
        /// </summary>
        TR Item4 { get; set; }

        /// <summary>
        /// Gets or sets the 5. item.
        /// </summary>
        T Item5 { get; set; }
    }

    /// <summary>
    /// The interface of the TupleObj.
    /// </summary>
    /// <typeparam name="TQ"> first param.</typeparam>
    /// <typeparam name="TW">second param.</typeparam>
    /// <typeparam name="TE">third param.</typeparam>
    /// <typeparam name="TR">fourth param.</typeparam>
    public interface ITupleObj<TQ, TW, TE, TR>
    {
        /// <summary>
        /// Gets or sets the first item.
        /// </summary>
        TQ Item1 { get; set; }

        /// <summary>
        /// Gets or sets the 2. item.
        /// </summary>
        TW Item2 { get; set; }

        /// <summary>
        /// Gets or sets the 3. item.
        /// </summary>
        TE Item3 { get; set; }

        /// <summary>
        /// Gets or sets the 4.item.
        /// </summary>
        TR Item4 { get; set; }
    }

    /// <summary>
    /// The interface of the TupleObj.
    /// </summary>
    /// <typeparam name="T"> first param.</typeparam>
    /// <typeparam name="TW">second param.</typeparam>
    /// <typeparam name="TE">third param.</typeparam>
    public interface ITupleObj<T, TW, TE>
    {
        /// <summary>
        /// Gets or sets the first item.
        /// </summary>
        T Item1 { get; set; }

        /// <summary>
        /// Gets or sets the 2. item.
        /// </summary>
        TW Item2 { get; set; }

        /// <summary>
        /// Gets or sets the 3. item.
        /// </summary>
        TE Item3 { get; set; }
    }

    /// <summary>
    /// The interface of the TupleObj.
    /// </summary>
    /// <typeparam name="T"> first param.</typeparam>
    /// <typeparam name="TW">second param.</typeparam>
    public interface ITupleObj<T, TW>
    {
        /// <summary>
        /// Gets or sets the first item.
        /// </summary>
        T Item1 { get; set; }

        /// <summary>
        /// Gets or sets the 2. item.
        /// </summary>
        TW Item2 { get; set; }
    }

    /// <summary>
    /// The interface of the TupleObj.
    /// </summary>
    /// <typeparam name="T"> first param.</typeparam>
    public interface ITupleObj<T>
    {
        /// <summary>
        /// Gets or sets the first item.
        /// </summary>
        T Item1 { get; set; }
    }
}