var class_document_storage_1_1_word_1_1_spell_checker =
[
    [ "SpellChecker", "class_document_storage_1_1_word_1_1_spell_checker.html#a9d9b29781f3f5c07ea0aafd5b22ac766", null ],
    [ "ConfigureRarity", "class_document_storage_1_1_word_1_1_spell_checker.html#ad9949099b4a7bb660b088bb95039f1f5", null ],
    [ "CorrectWordSet", "class_document_storage_1_1_word_1_1_spell_checker.html#ade2603549d04b2f7b6f2646ae092126e", null ],
    [ "Language", "class_document_storage_1_1_word_1_1_spell_checker.html#a9ec6579edae1245a3c9fc3bdfa54fa8f", null ],
    [ "WordSet", "class_document_storage_1_1_word_1_1_spell_checker.html#ab97a0f6d3dd0d44f9405e9474fa40de1", null ]
];