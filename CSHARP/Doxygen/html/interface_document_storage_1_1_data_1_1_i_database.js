var interface_document_storage_1_1_data_1_1_i_database =
[
    [ "DeletePage", "interface_document_storage_1_1_data_1_1_i_database.html#a30c9e0252b601f6ae6cee7a3ffb3002f", null ],
    [ "DeleteRule", "interface_document_storage_1_1_data_1_1_i_database.html#a7dd2fd1d7a136ba410188c78418bc276", null ],
    [ "DeleteUser", "interface_document_storage_1_1_data_1_1_i_database.html#a4c71368c262d1412be68577234f1114f", null ],
    [ "Getlines", "interface_document_storage_1_1_data_1_1_i_database.html#a222da41e493c92ed6371cffe34390d56", null ],
    [ "Getpages", "interface_document_storage_1_1_data_1_1_i_database.html#a849b95365a9400a26f1eb24e2539ea4e", null ],
    [ "GetPagesByUser", "interface_document_storage_1_1_data_1_1_i_database.html#a3ca1c89de6e2c3edcd641423eae9ba05", null ],
    [ "GetRuleCountForUsers", "interface_document_storage_1_1_data_1_1_i_database.html#a6278e3b1d23fe79bab7f8a922e106c7f", null ],
    [ "Getrules", "interface_document_storage_1_1_data_1_1_i_database.html#a3530955f5d69ed6fbee850fecb5b712b", null ],
    [ "Getusers", "interface_document_storage_1_1_data_1_1_i_database.html#ab7678e13d9e635c792585ab0373b2ec8", null ],
    [ "Getwords", "interface_document_storage_1_1_data_1_1_i_database.html#aab5f720821d2455cd917e4c3493b403b", null ],
    [ "Insertline", "interface_document_storage_1_1_data_1_1_i_database.html#a4b39a0972c5ce4befe97629131aaca1e", null ],
    [ "Insertpage", "interface_document_storage_1_1_data_1_1_i_database.html#a589a2afffd4f96467c4a8e2964164afe", null ],
    [ "Insertrule", "interface_document_storage_1_1_data_1_1_i_database.html#a0d9e1949bdd93e87f071c09203960c1f", null ],
    [ "Insertuser", "interface_document_storage_1_1_data_1_1_i_database.html#aeb6fc4282e4f0ce6b304c0dbe0a10cbd", null ],
    [ "Insertword", "interface_document_storage_1_1_data_1_1_i_database.html#aeb9c876df9b367e00791fe7991560e87", null ],
    [ "Startup", "interface_document_storage_1_1_data_1_1_i_database.html#a83cbbfec2be688f55c17da45456e7035", null ],
    [ "UpdateRule", "interface_document_storage_1_1_data_1_1_i_database.html#a5037ff6cdc6e96c65df5effcaff7361d", null ],
    [ "UpdateUser", "interface_document_storage_1_1_data_1_1_i_database.html#a3335e368a2e76781a76bfa4d5a60b6ef", null ],
    [ "UpdateWord", "interface_document_storage_1_1_data_1_1_i_database.html#ac20f9a7d0edfe88b02cca648c0cfd22f", null ]
];