var namespace_document_storage_1_1_logic_1_1_processes =
[
    [ "DataSaver", "class_document_storage_1_1_logic_1_1_processes_1_1_data_saver.html", "class_document_storage_1_1_logic_1_1_processes_1_1_data_saver" ],
    [ "Digitalizor", "class_document_storage_1_1_logic_1_1_processes_1_1_digitalizor.html", "class_document_storage_1_1_logic_1_1_processes_1_1_digitalizor" ],
    [ "ISearcher", "interface_document_storage_1_1_logic_1_1_processes_1_1_i_searcher.html", "interface_document_storage_1_1_logic_1_1_processes_1_1_i_searcher" ],
    [ "Searcher", "class_document_storage_1_1_logic_1_1_processes_1_1_searcher.html", "class_document_storage_1_1_logic_1_1_processes_1_1_searcher" ]
];