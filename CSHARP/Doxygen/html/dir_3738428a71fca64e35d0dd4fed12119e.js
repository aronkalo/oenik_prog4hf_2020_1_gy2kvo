var dir_3738428a71fca64e35d0dd4fed12119e =
[
    [ "DocumentStorage.Data", "dir_efa1bb5eff6e682b5654101d0078841c.html", "dir_efa1bb5eff6e682b5654101d0078841c" ],
    [ "DocumentStorage.Entites", "dir_e47a380654c7fc13f6b6bd779c92d4eb.html", "dir_e47a380654c7fc13f6b6bd779c92d4eb" ],
    [ "DocumentStorage.JavaWeb", "dir_5fe27932b8be3582d213cea1c6fe7bbd.html", "dir_5fe27932b8be3582d213cea1c6fe7bbd" ],
    [ "DocumentStorage.Logic", "dir_2ad2592eb851353c1d774ec003d03414.html", "dir_2ad2592eb851353c1d774ec003d03414" ],
    [ "DocumentStorage.Logic.Tests", "dir_6fa5582ed2fe854faba365ba24605fbf.html", "dir_6fa5582ed2fe854faba365ba24605fbf" ],
    [ "DocumentStorage.OCR", "dir_eceb2986870305efe7788272ff61b987.html", "dir_eceb2986870305efe7788272ff61b987" ],
    [ "DocumentStorage.Repository", "dir_e2856dcca4b1eea8f9c0c132e780e17a.html", "dir_e2856dcca4b1eea8f9c0c132e780e17a" ],
    [ "DocumentStorage.Word", "dir_f7c2a2840177c41b7e5cc6f01a21779f.html", "dir_f7c2a2840177c41b7e5cc6f01a21779f" ],
    [ "OENIK_PROG3_2019_2_GY2KVO", "dir_26dbae50b202fc6a17ea82339e52099b.html", "dir_26dbae50b202fc6a17ea82339e52099b" ]
];