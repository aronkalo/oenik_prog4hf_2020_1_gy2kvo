var namespace_document_storage_1_1_repository =
[
    [ "Interfaces", "namespace_document_storage_1_1_repository_1_1_interfaces.html", "namespace_document_storage_1_1_repository_1_1_interfaces" ],
    [ "RepoDatabase", "class_document_storage_1_1_repository_1_1_repo_database.html", "class_document_storage_1_1_repository_1_1_repo_database" ],
    [ "RepoTesseractCop", "class_document_storage_1_1_repository_1_1_repo_tesseract_cop.html", "class_document_storage_1_1_repository_1_1_repo_tesseract_cop" ],
    [ "RepoWordCorrector", "class_document_storage_1_1_repository_1_1_repo_word_corrector.html", "class_document_storage_1_1_repository_1_1_repo_word_corrector" ]
];