var class_document_storage_1_1_repository_1_1_repo_word_corrector =
[
    [ "RepoWordCorrector", "class_document_storage_1_1_repository_1_1_repo_word_corrector.html#acb93c07b8a003a92efab8089f6d36937", null ],
    [ "AddRule", "class_document_storage_1_1_repository_1_1_repo_word_corrector.html#aa5096f6f767e3b6752b0ba5c999a1f06", null ],
    [ "CorrectWordSet", "class_document_storage_1_1_repository_1_1_repo_word_corrector.html#a80f56d1b4a9be052c9197f43276ba4ba", null ],
    [ "ModifyRuleSet", "class_document_storage_1_1_repository_1_1_repo_word_corrector.html#a62233897e1b8761ae2b72a2829ca3dd9", null ],
    [ "SpellChecker", "class_document_storage_1_1_repository_1_1_repo_word_corrector.html#a52bf5bba5f8673b29990acda19511bce", null ],
    [ "SyntaxCorrector", "class_document_storage_1_1_repository_1_1_repo_word_corrector.html#afeaa6fc16737b121dc3b5d7ce61ca995", null ],
    [ "Translator", "class_document_storage_1_1_repository_1_1_repo_word_corrector.html#ae9bc24f4b096ff19c9e1a40aef816226", null ]
];