var class_document_storage_1_1_data_1_1_word =
[
    [ "BASE64_IMAGE", "class_document_storage_1_1_data_1_1_word.html#ac7d30adcd024b8ee66da56d8bd35e437", null ],
    [ "CREATE_DATE", "class_document_storage_1_1_data_1_1_word.html#a7ded2cecf3432bbe11e61569ed9f3e4b", null ],
    [ "CREATE_USER", "class_document_storage_1_1_data_1_1_word.html#a0ba7b6279149afcca916d1a51a378dd4", null ],
    [ "HEIGHT", "class_document_storage_1_1_data_1_1_word.html#a3d82286d490590e0ec619ea6047445e8", null ],
    [ "Id", "class_document_storage_1_1_data_1_1_word.html#a4b2fadc035ac92dc1298757e099c929a", null ],
    [ "Line", "class_document_storage_1_1_data_1_1_word.html#aad67311509efe42be54db5a38bb0e32a", null ],
    [ "LINE_ID", "class_document_storage_1_1_data_1_1_word.html#a8782c3b3b211dadf3f34c4b153f45f39", null ],
    [ "Page", "class_document_storage_1_1_data_1_1_word.html#afbdb54b5ca544e48671a773eafee97a9", null ],
    [ "PAGE_ID", "class_document_storage_1_1_data_1_1_word.html#aa78806563d01f2084e8997794af95828", null ],
    [ "User", "class_document_storage_1_1_data_1_1_word.html#a86b2bd46c3de8675aebfda4017cc4850", null ],
    [ "VALUE", "class_document_storage_1_1_data_1_1_word.html#a683c3b173c961f09fe8ddfb8686b4029", null ],
    [ "WIDTH", "class_document_storage_1_1_data_1_1_word.html#a17cc91297e9b919915a87ec0adf440a1", null ],
    [ "X", "class_document_storage_1_1_data_1_1_word.html#ae3d8575a03bc62e1d139ebe8788db87e", null ],
    [ "Y", "class_document_storage_1_1_data_1_1_word.html#af96f6f4b822ed8369a8aed8529f74491", null ]
];