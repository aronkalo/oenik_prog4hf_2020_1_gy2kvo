var namespace_document_storage_1_1_repository_1_1_interfaces =
[
    [ "IRepoDatabase", "interface_document_storage_1_1_repository_1_1_interfaces_1_1_i_repo_database.html", "interface_document_storage_1_1_repository_1_1_interfaces_1_1_i_repo_database" ],
    [ "IRepoTesseractCop", "interface_document_storage_1_1_repository_1_1_interfaces_1_1_i_repo_tesseract_cop.html", "interface_document_storage_1_1_repository_1_1_interfaces_1_1_i_repo_tesseract_cop" ],
    [ "IRepoWordCorrector", "interface_document_storage_1_1_repository_1_1_interfaces_1_1_i_repo_word_corrector.html", "interface_document_storage_1_1_repository_1_1_interfaces_1_1_i_repo_word_corrector" ]
];