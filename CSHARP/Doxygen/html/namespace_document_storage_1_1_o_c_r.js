var namespace_document_storage_1_1_o_c_r =
[
    [ "Entities", "namespace_document_storage_1_1_o_c_r_1_1_entities.html", "namespace_document_storage_1_1_o_c_r_1_1_entities" ],
    [ "Interfaces", "namespace_document_storage_1_1_o_c_r_1_1_interfaces.html", "namespace_document_storage_1_1_o_c_r_1_1_interfaces" ],
    [ "PreProcessing", "namespace_document_storage_1_1_o_c_r_1_1_pre_processing.html", "namespace_document_storage_1_1_o_c_r_1_1_pre_processing" ],
    [ "Tesseract", "namespace_document_storage_1_1_o_c_r_1_1_tesseract.html", "namespace_document_storage_1_1_o_c_r_1_1_tesseract" ]
];