var class_document_storage_1_1_o_c_r_1_1_tesseract_1_1_tesseract_engine =
[
    [ "TesseractEngine", "class_document_storage_1_1_o_c_r_1_1_tesseract_1_1_tesseract_engine.html#a15dc8d7917a4f1d991b0cb7d10a1e4f3", null ],
    [ "StartProcess", "class_document_storage_1_1_o_c_r_1_1_tesseract_1_1_tesseract_engine.html#a4f905f7a04b072c22e3be394a660dacc", null ],
    [ "ConfigFile", "class_document_storage_1_1_o_c_r_1_1_tesseract_1_1_tesseract_engine.html#a2dfd55f820a229a3b20b8aac8cb91286", null ],
    [ "ExeName", "class_document_storage_1_1_o_c_r_1_1_tesseract_1_1_tesseract_engine.html#a81def3c1327b3a94142c49319729a044", null ],
    [ "Lang", "class_document_storage_1_1_o_c_r_1_1_tesseract_1_1_tesseract_engine.html#a81fae167c20e5d3f80c3d84da0150beb", null ],
    [ "ProcImage", "class_document_storage_1_1_o_c_r_1_1_tesseract_1_1_tesseract_engine.html#a9414f8753b4687d260028d149409e837", null ],
    [ "WorkDir", "class_document_storage_1_1_o_c_r_1_1_tesseract_1_1_tesseract_engine.html#a707e0520e8196b8ca38f228aacef32fb", null ]
];