var class_document_storage_1_1_data_1_1_user =
[
    [ "User", "class_document_storage_1_1_data_1_1_user.html#a60c9619bc4b0438ad25fa05d192a943a", null ],
    [ "CREATE_DATE", "class_document_storage_1_1_data_1_1_user.html#a0388affc13e4d3078178bddd2d82cc66", null ],
    [ "ID", "class_document_storage_1_1_data_1_1_user.html#a69a6fbcf28884f99ca204489689f5158", null ],
    [ "IS_ADMIN", "class_document_storage_1_1_data_1_1_user.html#aaacae2f0c7ace7ac1a074e3959e2f199", null ],
    [ "Line", "class_document_storage_1_1_data_1_1_user.html#ab5d728742716b78bc62da7e984111d24", null ],
    [ "NAME", "class_document_storage_1_1_data_1_1_user.html#a53592c94625dc23857046f73fdd305cd", null ],
    [ "Page", "class_document_storage_1_1_data_1_1_user.html#adca4ccdc127175e52d50e04097bca132", null ],
    [ "Rule", "class_document_storage_1_1_data_1_1_user.html#aeed7182ecbfaa8439eb8b45e0d21f5da", null ],
    [ "Word", "class_document_storage_1_1_data_1_1_user.html#a733f0be50fb050da871400603730fc6b", null ]
];