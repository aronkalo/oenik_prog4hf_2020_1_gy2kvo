var searchData=
[
  ['data_203',['Data',['../namespace_document_storage_1_1_data.html',1,'DocumentStorage']]],
  ['documentstorage_204',['DocumentStorage',['../namespace_document_storage.html',1,'']]],
  ['entities_205',['Entities',['../namespace_document_storage_1_1_entities.html',1,'DocumentStorage.Entities'],['../namespace_document_storage_1_1_o_c_r_1_1_entities.html',1,'DocumentStorage.OCR.Entities']]],
  ['interfaces_206',['Interfaces',['../namespace_document_storage_1_1_o_c_r_1_1_interfaces.html',1,'DocumentStorage.OCR.Interfaces'],['../namespace_document_storage_1_1_repository_1_1_interfaces.html',1,'DocumentStorage.Repository.Interfaces'],['../namespace_document_storage_1_1_word_1_1_interfaces.html',1,'DocumentStorage.Word.Interfaces']]],
  ['javaweb_207',['JavaWeb',['../namespace_document_storage_1_1_java_web.html',1,'DocumentStorage']]],
  ['logic_208',['Logic',['../namespace_document_storage_1_1_logic.html',1,'DocumentStorage']]],
  ['ocr_209',['OCR',['../namespace_document_storage_1_1_o_c_r.html',1,'DocumentStorage']]],
  ['preprocessing_210',['PreProcessing',['../namespace_document_storage_1_1_o_c_r_1_1_pre_processing.html',1,'DocumentStorage::OCR']]],
  ['processes_211',['Processes',['../namespace_document_storage_1_1_logic_1_1_processes.html',1,'DocumentStorage::Logic']]],
  ['program_212',['Program',['../namespace_document_storage_1_1_program.html',1,'DocumentStorage']]],
  ['repository_213',['Repository',['../namespace_document_storage_1_1_repository.html',1,'DocumentStorage']]],
  ['tesseract_214',['Tesseract',['../namespace_document_storage_1_1_o_c_r_1_1_tesseract.html',1,'DocumentStorage::OCR']]],
  ['tests_215',['Tests',['../namespace_document_storage_1_1_logic_1_1_tests.html',1,'DocumentStorage::Logic']]],
  ['word_216',['Word',['../namespace_document_storage_1_1_word.html',1,'DocumentStorage']]]
];
