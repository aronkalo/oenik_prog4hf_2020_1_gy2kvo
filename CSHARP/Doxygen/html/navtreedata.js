/*
@licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2019 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "DocumentStorage", "index.html", [
    [ "Castle Core Changelog", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_607c0580d00c9bf4f5b5ebf929da1eb1.html", [
      [ "4.4.0 (2019-04-05)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_607c0580d00c9bf4f5b5ebf929da1eb1.html#autotoc_md1", null ],
      [ "4.3.1 (2018-06-21)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_607c0580d00c9bf4f5b5ebf929da1eb1.html#autotoc_md2", null ],
      [ "4.3.0 (2018-06-07)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_607c0580d00c9bf4f5b5ebf929da1eb1.html#autotoc_md3", null ],
      [ "4.2.1 (2017-10-11)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_607c0580d00c9bf4f5b5ebf929da1eb1.html#autotoc_md4", null ],
      [ "4.2.0 (2017-09-28)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_607c0580d00c9bf4f5b5ebf929da1eb1.html#autotoc_md5", null ],
      [ "4.1.1 (2017-07-12)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_607c0580d00c9bf4f5b5ebf929da1eb1.html#autotoc_md6", null ],
      [ "4.1.0 (2017-06-11)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_607c0580d00c9bf4f5b5ebf929da1eb1.html#autotoc_md7", null ],
      [ "4.0.0 (2017-01-25)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_607c0580d00c9bf4f5b5ebf929da1eb1.html#autotoc_md8", null ],
      [ "4.0.0-beta002 (2016-10-28)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_607c0580d00c9bf4f5b5ebf929da1eb1.html#autotoc_md9", null ],
      [ "4.0.0-beta001 (2016-07-17)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_607c0580d00c9bf4f5b5ebf929da1eb1.html#autotoc_md10", null ],
      [ "4.0.0-alpha001 (2016-04-07)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_607c0580d00c9bf4f5b5ebf929da1eb1.html#autotoc_md11", null ],
      [ "3.3.3 (2014-11-06)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_607c0580d00c9bf4f5b5ebf929da1eb1.html#autotoc_md12", null ],
      [ "3.3.2 (2014-11-03)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_607c0580d00c9bf4f5b5ebf929da1eb1.html#autotoc_md13", null ],
      [ "3.3.1 (2014-09-10)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_607c0580d00c9bf4f5b5ebf929da1eb1.html#autotoc_md14", null ],
      [ "3.3.0 (2014-04-27)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_607c0580d00c9bf4f5b5ebf929da1eb1.html#autotoc_md15", null ],
      [ "3.2.2 (2013-11-30)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_607c0580d00c9bf4f5b5ebf929da1eb1.html#autotoc_md16", null ],
      [ "3.2.1 (2013-10-05)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_607c0580d00c9bf4f5b5ebf929da1eb1.html#autotoc_md17", null ],
      [ "3.2.0 (2013-02-16)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_607c0580d00c9bf4f5b5ebf929da1eb1.html#autotoc_md18", null ],
      [ "3.1.0 (2012-08-05)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_607c0580d00c9bf4f5b5ebf929da1eb1.html#autotoc_md19", null ],
      [ "3.1.0 RC (2012-07-08)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_607c0580d00c9bf4f5b5ebf929da1eb1.html#autotoc_md20", null ],
      [ "3.0.0 (2011-12-13)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_607c0580d00c9bf4f5b5ebf929da1eb1.html#autotoc_md21", null ],
      [ "3.0.0 RC 1 (2011-11-20)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_607c0580d00c9bf4f5b5ebf929da1eb1.html#autotoc_md22", null ],
      [ "3.0.0 beta 1 (2011-08-14)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_607c0580d00c9bf4f5b5ebf929da1eb1.html#autotoc_md23", null ],
      [ "2.5.2 (2010-11-15)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_607c0580d00c9bf4f5b5ebf929da1eb1.html#autotoc_md24", null ],
      [ "2.5.1 (2010-09-21)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_607c0580d00c9bf4f5b5ebf929da1eb1.html#autotoc_md25", null ],
      [ "2.5.0 (2010-08-21)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_607c0580d00c9bf4f5b5ebf929da1eb1.html#autotoc_md26", null ],
      [ "1.2.0 (2010-01-11)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_607c0580d00c9bf4f5b5ebf929da1eb1.html#autotoc_md27", null ],
      [ "1.2.0 beta (2009-12-04)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_607c0580d00c9bf4f5b5ebf929da1eb1.html#autotoc_md28", null ],
      [ "1.1.0 (2009-05-04)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_607c0580d00c9bf4f5b5ebf929da1eb1.html#autotoc_md29", null ],
      [ "Release Candidate 3", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_607c0580d00c9bf4f5b5ebf929da1eb1.html#autotoc_md30", null ],
      [ "0.0.1.0", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_607c0580d00c9bf4f5b5ebf929da1eb1.html#autotoc_md31", null ]
    ] ],
    [ "NUnit 3.12 - May 14, 2019", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html", null ],
    [ "Castle Core Changelog", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__castle_8_core_84_84_80__c_h_a_n_g_e_l_o_g.html", [
      [ "4.4.0 (2019-04-05)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__castle_8_core_84_84_80__c_h_a_n_g_e_l_o_g.html#autotoc_md165", null ],
      [ "4.3.1 (2018-06-21)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__castle_8_core_84_84_80__c_h_a_n_g_e_l_o_g.html#autotoc_md166", null ],
      [ "4.3.0 (2018-06-07)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__castle_8_core_84_84_80__c_h_a_n_g_e_l_o_g.html#autotoc_md167", null ],
      [ "4.2.1 (2017-10-11)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__castle_8_core_84_84_80__c_h_a_n_g_e_l_o_g.html#autotoc_md168", null ],
      [ "4.2.0 (2017-09-28)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__castle_8_core_84_84_80__c_h_a_n_g_e_l_o_g.html#autotoc_md169", null ],
      [ "4.1.1 (2017-07-12)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__castle_8_core_84_84_80__c_h_a_n_g_e_l_o_g.html#autotoc_md170", null ],
      [ "4.1.0 (2017-06-11)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__castle_8_core_84_84_80__c_h_a_n_g_e_l_o_g.html#autotoc_md171", null ],
      [ "4.0.0 (2017-01-25)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__castle_8_core_84_84_80__c_h_a_n_g_e_l_o_g.html#autotoc_md172", null ],
      [ "4.0.0-beta002 (2016-10-28)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__castle_8_core_84_84_80__c_h_a_n_g_e_l_o_g.html#autotoc_md173", null ],
      [ "4.0.0-beta001 (2016-07-17)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__castle_8_core_84_84_80__c_h_a_n_g_e_l_o_g.html#autotoc_md174", null ],
      [ "4.0.0-alpha001 (2016-04-07)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__castle_8_core_84_84_80__c_h_a_n_g_e_l_o_g.html#autotoc_md175", null ],
      [ "3.3.3 (2014-11-06)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__castle_8_core_84_84_80__c_h_a_n_g_e_l_o_g.html#autotoc_md176", null ],
      [ "3.3.2 (2014-11-03)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__castle_8_core_84_84_80__c_h_a_n_g_e_l_o_g.html#autotoc_md177", null ],
      [ "3.3.1 (2014-09-10)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__castle_8_core_84_84_80__c_h_a_n_g_e_l_o_g.html#autotoc_md178", null ],
      [ "3.3.0 (2014-04-27)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__castle_8_core_84_84_80__c_h_a_n_g_e_l_o_g.html#autotoc_md179", null ],
      [ "3.2.2 (2013-11-30)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__castle_8_core_84_84_80__c_h_a_n_g_e_l_o_g.html#autotoc_md180", null ],
      [ "3.2.1 (2013-10-05)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__castle_8_core_84_84_80__c_h_a_n_g_e_l_o_g.html#autotoc_md181", null ],
      [ "3.2.0 (2013-02-16)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__castle_8_core_84_84_80__c_h_a_n_g_e_l_o_g.html#autotoc_md182", null ],
      [ "3.1.0 (2012-08-05)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__castle_8_core_84_84_80__c_h_a_n_g_e_l_o_g.html#autotoc_md183", null ],
      [ "3.1.0 RC (2012-07-08)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__castle_8_core_84_84_80__c_h_a_n_g_e_l_o_g.html#autotoc_md184", null ],
      [ "3.0.0 (2011-12-13)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__castle_8_core_84_84_80__c_h_a_n_g_e_l_o_g.html#autotoc_md185", null ],
      [ "3.0.0 RC 1 (2011-11-20)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__castle_8_core_84_84_80__c_h_a_n_g_e_l_o_g.html#autotoc_md186", null ],
      [ "3.0.0 beta 1 (2011-08-14)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__castle_8_core_84_84_80__c_h_a_n_g_e_l_o_g.html#autotoc_md187", null ],
      [ "2.5.2 (2010-11-15)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__castle_8_core_84_84_80__c_h_a_n_g_e_l_o_g.html#autotoc_md188", null ],
      [ "2.5.1 (2010-09-21)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__castle_8_core_84_84_80__c_h_a_n_g_e_l_o_g.html#autotoc_md189", null ],
      [ "2.5.0 (2010-08-21)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__castle_8_core_84_84_80__c_h_a_n_g_e_l_o_g.html#autotoc_md190", null ],
      [ "1.2.0 (2010-01-11)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__castle_8_core_84_84_80__c_h_a_n_g_e_l_o_g.html#autotoc_md191", null ],
      [ "1.2.0 beta (2009-12-04)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__castle_8_core_84_84_80__c_h_a_n_g_e_l_o_g.html#autotoc_md192", null ],
      [ "1.1.0 (2009-05-04)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__castle_8_core_84_84_80__c_h_a_n_g_e_l_o_g.html#autotoc_md193", null ],
      [ "Release Candidate 3", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__castle_8_core_84_84_80__c_h_a_n_g_e_l_o_g.html#autotoc_md194", null ],
      [ "0.0.1.0", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__castle_8_core_84_84_80__c_h_a_n_g_e_l_o_g.html#autotoc_md195", null ]
    ] ],
    [ "NUnit 3.12 - May 14, 2019", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html", null ],
    [ "Packages", "namespaces.html", [
      [ "Packages", "namespaces.html", "namespaces_dup" ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", null ],
        [ "Properties", "functions_prop.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_client_8cs_source.html",
"class_document_storage_1_1_repository_1_1_repo_database.html#a3a448788ac20cd85c958f63309807572",
"md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md35",
"md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md317"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';