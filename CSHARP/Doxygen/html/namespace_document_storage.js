var namespace_document_storage =
[
    [ "Data", "namespace_document_storage_1_1_data.html", "namespace_document_storage_1_1_data" ],
    [ "Entities", "namespace_document_storage_1_1_entities.html", "namespace_document_storage_1_1_entities" ],
    [ "JavaWeb", "namespace_document_storage_1_1_java_web.html", "namespace_document_storage_1_1_java_web" ],
    [ "Logic", "namespace_document_storage_1_1_logic.html", "namespace_document_storage_1_1_logic" ],
    [ "OCR", "namespace_document_storage_1_1_o_c_r.html", "namespace_document_storage_1_1_o_c_r" ],
    [ "Program", "namespace_document_storage_1_1_program.html", "namespace_document_storage_1_1_program" ],
    [ "Repository", "namespace_document_storage_1_1_repository.html", "namespace_document_storage_1_1_repository" ],
    [ "Word", "namespace_document_storage_1_1_word.html", "namespace_document_storage_1_1_word" ]
];