﻿// <copyright file="Mocker.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DocumentStorage.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DocumentStorage.Entities;
    using DocumentStorage.Logic.Processes;
    using DocumentStorage.Repository.Interfaces;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// The moq test class.
    /// </summary>
    [TestFixture]
    public class Mocker
    {
        /// <summary>
        /// The DB.
        /// </summary>
        private Mock<IRepoDatabase> mockLogic;

        /// <summary>
        /// The Logic class.
        /// </summary>
        private Searcher searcher;

        /// <summary>
        /// The initialization method.
        /// </summary>
        [SetUp]
        public void Init()
        {
            this.mockLogic = new Mock<IRepoDatabase>();
            this.mockLogic.Setup(x => x.Getpages()).Returns(new List<ITupleObj<int, string, string, DateTime, int, int>>() {
                new TupleObj<int, string, string, DateTime, int, int>()
                {
                    Item1 = 1,
                    Item2 = "img1",
                    Item3 = "loc1",
                    Item4 = DateTime.Now,
                    Item5 = 1,
                    Item6 = 1,
                },
                new TupleObj<int, string, string, DateTime, int, int>()
                {
                    Item1 = 2,
                    Item2 = "img2",
                    Item3 = "loc2",
                    Item4 = DateTime.Now,
                    Item5 = 2,
                    Item6 = 2,
                },
                new TupleObj<int, string, string, DateTime, int, int>()
                {
                    Item1 = 3,
                    Item2 = "img3",
                    Item3 = "loc3",
                    Item4 = DateTime.Now,
                    Item5 = 1,
                    Item6 = 1,
                },
                new TupleObj<int, string, string, DateTime, int, int>()
                {
                    Item1 = 4,
                    Item2 = "img4",
                    Item3 = "loc4",
                    Item4 = DateTime.Now,
                    Item5 = 2,
                    Item6 = 2,
                },
            });

            this.mockLogic.Setup(x => x.Getusers(true)).Returns(new List<ITupleObj<int, string, string, DateTime>>()
            {
                new TupleObj<int, string, string, DateTime>()
                       {
                           Item1 = 1,
                           Item2 = "1",
                           Item3 = "ADMIN",
                           Item4 = DateTime.Now,
                       },
                new TupleObj<int, string, string, DateTime>()
                       {
                           Item1 = 2,
                           Item2 = "2",
                           Item3 = "ADMIN",
                           Item4 = DateTime.Now,
                       },
                new TupleObj<int, string, string, DateTime>()
                       {
                           Item1 = 3,
                           Item2 = "3",
                           Item3 = "USER",
                           Item4 = DateTime.Now,
                       },
            });

            this.mockLogic.Setup(x => x.Getusers(false)).Returns(new List<ITupleObj<int, string, string, DateTime>>()
            {
                new TupleObj<int, string, string, DateTime>()
                       {
                           Item1 = 3,
                           Item2 = "3",
                           Item3 = "USER",
                           Item4 = DateTime.Now,
                       },
            });

            this.mockLogic.Setup(x => x.Getrules()).Returns(new List<ITupleObj<int, string, string, int, DateTime>>()
            {
                new TupleObj<int, string, string, int, DateTime>()
                           {
                               Item1 = 1,
                               Item2 = "1",
                               Item3 = "l",
                               Item4 = 1,
                               Item5 = DateTime.Now,
                           },
                new TupleObj<int, string, string, int, DateTime>()
                           {
                               Item1 = 2,
                               Item2 = "2",
                               Item3 = "1",
                               Item4 = 3,
                               Item5 = DateTime.Now,
                           },
                new TupleObj<int, string, string, int, DateTime>()
                           {
                               Item1 = 3,
                               Item2 = "3",
                               Item3 = "O",
                               Item4 = 3,
                               Item5 = DateTime.Now,
                           },
            });

            this.mockLogic.Setup(x => x.Getlines()).Returns(new List<ITupleObj<int, string, DateTime, int, int, int, int, int, int, string>>()
            {
                new TupleObj<int, string, DateTime, int, int, int, int, int, int, string>
                {
                    Item1 = 1,
                    Item3 = DateTime.Now,
                    Item4 = 1,
                    Item5 = 1,
                },
                new TupleObj<int, string, DateTime, int, int, int, int, int, int, string>
                {
                    Item1 = 2,
                    Item3 = DateTime.Now,
                    Item4 = 2,
                    Item5 = 2,
                },
                new TupleObj<int, string, DateTime, int, int, int, int, int, int, string>
                {
                    Item1 = 3,
                    Item3 = DateTime.Now,
                    Item4 = 2,
                    Item5 = 2,
                },
                new TupleObj<int, string, DateTime, int, int, int, int, int, int, string>
                {
                    Item1 = 4,
                    Item3 = DateTime.Now,
                    Item4 = 2,
                    Item5 = 2,
                },
            });

            this.mockLogic.Setup(x => x.Getwords()).Returns(new List<ITupleObj<int, string, DateTime, int, int, int, int, int, int, string>>()
            {
                new TupleObj<int, string, DateTime, int, int, int, int, int, int, string>
                {
                    Item1 = 1,
                    Item3 = DateTime.Now,
                    Item4 = 1,
                    Item5 = 1,
                    Item10 = "1",
                },
                new TupleObj<int, string, DateTime, int, int, int, int, int, int, string>
                {
                    Item1 = 2,
                    Item3 = DateTime.Now,
                    Item4 = 1,
                    Item5 = 1,
                    Item10 = "2",
                },
                new TupleObj<int, string, DateTime, int, int, int, int, int, int, string>
                {
                    Item1 = 3,
                    Item3 = DateTime.Now,
                    Item4 = 2,
                    Item5 = 2,
                    Item10 = "3",
                },
                new TupleObj<int, string, DateTime, int, int, int, int, int, int, string>
                {
                    Item1 = 4,
                    Item3 = DateTime.Now,
                    Item4 = 2,
                    Item5 = 2,
                    Item10 = "4",
                },
            });

            this.mockLogic.Setup(x => x.GetRuleCountForUsers()).Returns(new List<ITupleObj<string, int>>()
            {
                new TupleObj<string, int>()
                        {
                            Item1 = "1",
                            Item2 = 1,
                        },
                new TupleObj<string, int>()
                        {
                            Item1 = "2",
                            Item2 = 2,
                        },
                new TupleObj<string, int>()
                        {
                            Item1 = "3",
                            Item2 = 3,
                        },
            });

            this.mockLogic.Setup(x => x.GetPagesByUser(1)).Returns(new List<ITupleObj<int, string, string, DateTime, int, int, List<ITupleObj<int, string, DateTime, int, int, int, int, int, int, string, List<ITupleObj<int, string, DateTime, int, int, int, int, int, int, string>>>>>>()
            {
                new TupleObj<int, string, string, DateTime, int, int, List<ITupleObj<int, string, DateTime, int, int, int, int, int, int, string, List<ITupleObj<int, string, DateTime, int, int, int, int, int, int, string>>>>>()
                {
                    Item1 = 1,
                    Item2 = "img1",
                    Item3 = "orig1",
                    Item4 = DateTime.Now,
                    Item5 = 1,
                    Item6 = 1,
                },
                new TupleObj<int, string, string, DateTime, int, int, List<ITupleObj<int, string, DateTime, int, int, int, int, int, int, string, List<ITupleObj<int, string, DateTime, int, int, int, int, int, int, string>>>>>()
                {
                    Item1 = 1,
                    Item2 = "img2",
                    Item3 = "orig2",
                    Item4 = DateTime.Now,
                    Item5 = 1,
                    Item6 = 2,
                },
            });

            this.mockLogic.Setup(x => x.Insertline("1", "1", 1, 0)).Returns(5);
            this.searcher = new Searcher(this.mockLogic.Object);
        }

        /// <summary>
        /// Overindex and throws exception.
        /// </summary>
        [Test]
        public void Test_GetUsernamesByID_OverindexandthrowsException()
        {
            Assert.Throws<Exception>(() => this.searcher.GetUserNameByID(4));
        }

        /// <summary>
        /// valid index returns name
        /// </summary>
        [Test]
        public void Test_GetUsernamesByID_validIndexReturnsName()
        {
            string name = this.searcher.GetUserNameByID(1);
            Assert.That(name == "1");
        }

        /// <summary>
        /// Gets the users and check first item.
        /// </summary>
        [Test]
        public void Test_GetUsers_AndFirstItemOK()
        {
            var users = this.searcher.GetUsers();
            Assert.That(users.ToList()[0].Item2 == "1");
        }

        /// <summary>
        /// Gets the users and check count.
        /// </summary>
        [Test]
        public void Test_GetUsers_AndCheckCount()
        {
            var users = this.searcher.GetUsers();
            Assert.That(users.Count() == 3);
        }

        /// <summary>
        /// Gets the pages to a user and checkfilename and count.
        /// </summary>
        [Test]
        public void Test_GetPagesToAUserAndCheckFilenameAndCount()
        {
            var userPages = this.searcher.GetPagesByUser(1);

            Assert.That(userPages.Count() == 2);
            Assert.That(userPages.ToList()[0].Item3 == "orig1");
            Assert.That(userPages.ToList()[1].Item3 == "orig2");

        }

        /// <summary>
        /// Gets the rule num for the users.
        /// </summary>
        [Test]
        public void Test_Get_RuleCountForUsers_CheckCount_AndNames()
        {
            var rulesperUser = this.searcher.GetRuleCountForUsers();

            Assert.That(rulesperUser.Count() == 3);
            Assert.That(rulesperUser.ToList()[0].Item1 == "1");
            Assert.That(rulesperUser.ToList()[1].Item1 == "2");
            Assert.That(rulesperUser.ToList()[2].Item1 == "3");
        }

        /// <summary>
        /// Searches for a word and no match.
        /// </summary>
        [Test]
        public void Test_SearchWordAndNoMatch()
        {
            string word = this.searcher.SearchForWord(1, "valaminagyonfontosadat");
            Assert.That(word == "No matches found");
        }

        /// <summary>
        /// Searches for a word and find match.
        /// </summary>
        [Test]
        public void Test_SearchWordAndGotMatch()
        {
            string word = this.searcher.SearchForWord(1, "1");
            Assert.That(word != "No matches found");
        }

        /// <summary>
        /// Searches for a word and throws exception.
        /// </summary>
        [Test]
        public void Test_SearchWordAndOverIndexThrowsException()
        {
            Assert.Throws<Exception>(() => this.searcher.SearchForWord(5, "ugyisExceptonLesz"));
        }

        /// <summary>
        /// Gets the lines and checks the count.
        /// </summary>
        [Test]
        public void Test_GetRulesAndCheckCount()
        {
            Assert.That(this.searcher.Getrules().Count() == 3);
        }

        /// <summary>
        /// Gets a page.
        /// </summary>
        [Test]
        public void Test_GetPageByIDandCheckImage()
        {
            var page = this.searcher.GetPageByID(1);
            Assert.That(page.Item2 == "img1");
        }

        /// <summary>
        /// Gets a page.
        /// </summary>
        [Test]
        public void Test_GetPageByIDandCheckLoc()
        {
            var page = this.searcher.GetPageByID(2);
            Assert.That(page.Item3 == "loc2");
        }

        /// <summary>
        /// Gets a page.
        /// </summary>
        [Test]
        public void Test_GetPageByIDOverIndexAndThrowsException()
        {
            Assert.Throws<Exception>(() => this.searcher.GetPageByID(10));
        }

        /// <summary>
        /// Gets a line.
        /// </summary>
        [Test]
        public void Test_GetLineByIDOverIndexAndThrowsException()
        {
            Assert.Throws<Exception>(() => this.searcher.GetLineByID(10));
        }

        /// <summary>
        /// Gets a line.
        /// </summary>
        [Test]
        public void Test_GetLineByIDandCheckCreateUser()
        {
            var line = this.searcher.GetLineByID(1);
            Assert.That(line.Item4 == 1);
        }

        /// <summary>
        /// Gets a line.
        /// </summary>
        [Test]
        public void Test_GetLineByIDOverIndexAndCheckPageID()
        {
            var line = this.searcher.GetLineByID(2);
            Assert.That(line.Item5 == 2);
        }

        /// <summary>
        /// Gets a line.
        /// </summary>
        [Test]
        public void Test_GetWordByIDOverIndexAndCheckLineID()
        {
            var word = this.searcher.GetWordByID(1);
            Assert.That(word.Item5 == 1);
        }

        /// <summary>
        /// Gets a line.
        /// </summary>
        [Test]
        public void Test_GetWordByIDOverIndexAndCheckValue()
        {
            var word = this.searcher.GetWordByID(2);

            Assert.That(word.Item10 == "2");
        }

        /// <summary>
        /// Gets a line.
        /// </summary>
        [Test]
        public void Test_GetWordByIDOverIndexOverIndexAndThrowsException()
        {
            Assert.Throws<Exception>(() => this.searcher.GetWordByID(10));
        }
    }
}
