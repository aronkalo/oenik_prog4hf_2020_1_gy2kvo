﻿using DocumentStorage.Entities;
using DocumentStorage.Logic.Processes;
using DocumentStorage.Repository;
using DocumentStorage.Repository.Interfaces;
using DocumentStorage.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DocumentStorage.Web.Controllers
{
    public class UserApiController : ApiController
    {
        public class ApiResult
        {
            public bool OperationResult { get; set; }
        }

        ISearcher logic;

        IRepoDatabase db;
        public UserApiController()
        {
            db = new RepoDatabase();
            logic = new Searcher(db);
        }

        // GET api/UserApi
        [ActionName("all")]
        [HttpGet]
        public IEnumerable<User> GetAll()
        {
            var users = logic.GetUsers();
            return GetUserModels(users);
        }

        [ActionName("del")]
        [HttpGet]
        public ApiResult DelOneUser(int id)
        {
            bool result = true;
            try
            {
                logic.DeleteUser(id);
            }
            catch (Exception)
            {
                result = false;
            }
            return new ApiResult() { OperationResult = result };
        }

        [ActionName("add")]
        [HttpPost]
        public ApiResult AddOneUser(User user)
        {
            bool result = true;
            try
            {
                logic.AddUser(user.Name, user.Admin);
            }
            catch (Exception)
            {
                result = false;
            }
            return new ApiResult() { OperationResult = result };
        }

        [ActionName("mod")]
        [HttpPost]
        public ApiResult ModOneUser(User user)
        {
            bool result = true;
            try
            {
                logic.UpdateUser(user.Id, user.Name, user.Admin);
            }
            catch (Exception)
            {
                result = false;
            }
            return new ApiResult() { OperationResult = result };
        }

        private List<User> GetUserModels(IEnumerable<ITupleObj<int, string, string, DateTime>> rules)
        {
            List<User> ruleModels = new List<User>();
            foreach (var rule in rules)
            {
                ruleModels.Add(new User()
                {
                    Id = rule.Item1,
                    Name = rule.Item2,
                    AddDate = rule.Item4,
                    Admin = rule.Item3.ToUpper() == "ADMIN" ? true : false,
                });
            }
            return ruleModels;
        }
    }
}
