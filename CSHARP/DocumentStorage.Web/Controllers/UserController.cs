﻿using DocumentStorage.Entities;
using DocumentStorage.Logic.Processes;
using DocumentStorage.Repository;
using DocumentStorage.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DocumentStorage.Web.Controllers
{
    public class UserController : Controller
    {
        ISearcher logic;
        UserViewModel vm;

        public UserController()
        {
            logic = new Searcher(new RepoDatabase());
            vm = new UserViewModel();
            vm.EditedUser = new User();
            vm.ListOfUsers = GetUserModels(logic.GetUsers());
        }

        private List<User> GetUserModels(IEnumerable<ITupleObj<int, string, string, DateTime>> rules)
        {
            List<User> ruleModels = new List<User>();
            foreach (var rule in rules)
            {
                ruleModels.Add(new User()
                {
                    Id = rule.Item1,
                    Name = rule.Item2,
                    AddDate = rule.Item4,
                    Admin = rule.Item3.ToUpper() == "ADMIN" ? true : false,
                });
            }
            return ruleModels;
        }

        private User GetUser(int id)
        {
            return GetUserModels(logic.GetUsers()).Where(x => x.Id == id).First();
        }

        // GET: User
        public ActionResult Index()
        {
            TempData["editAction"] = "AddNew";
            return View("UserIndex", vm);
        }

        // GET: User/Details/5
        public ActionResult Details(int id)
        {
            return View("UserDetails", GetUser(id));
        }

        public ActionResult Delete(int id)
        {
            ViewData["res"] = "Delete fail";
            try
            {
                logic.DeleteUser(id);
                ViewData["res"] = "Delete OK";
            }
            catch (Exception){ }
            return RedirectToAction("Index");
        }
        public ActionResult Edit(int id)
        {
            TempData["editAction"] = "Edit";
            try
            {
                vm.EditedUser = GetUser(id);
            }
            catch (Exception){ }
            return View("UserIndex", vm);
        }
        [HttpPost]
        public ActionResult Edit(User user, string editAction)
        {
            if (ModelState.IsValid && user != null)
            {
                ViewData["res"] = "Edit OK";
                if (editAction == "AddNew")
                {
                    try
                    {
                        logic.AddUser(user.Name, user.Admin);
                    }
                    catch (Exception)
                    {
                        ViewData["res"] = "Edit Fail";
                    }
                }
                else
                {
                    try
                    {
                        logic.UpdateUser(user.Id, user.Name, user.Admin);
                    }
                    catch (Exception)
                    {
                        ViewData["res"] = "Edit Fail";
                    }
                }
                vm.ListOfUsers = GetUserModels(logic.GetUsers());
                return RedirectToAction(nameof(Index));

            }
            else
            {
                ViewData["res"] = "Edit";
                vm.EditedUser = user;
                return View("UserIndex", vm);
            }
        }
    }
}
