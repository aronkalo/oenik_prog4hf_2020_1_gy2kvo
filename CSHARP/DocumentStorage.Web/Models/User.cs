﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DocumentStorage.Web.Models
{
    //  Form Model
    public class User
    {
        [Display(Name = "Identifier")]
        [Required]
        public int Id { get; set; }

        [Display(Name = "User Name")]
        public string Name { get; set; }

        [Display(Name = "Admin")]
        public bool Admin { get; set; }

        [Display(Name = "Create Date")]
        public DateTime AddDate { get; set; }

    }
}