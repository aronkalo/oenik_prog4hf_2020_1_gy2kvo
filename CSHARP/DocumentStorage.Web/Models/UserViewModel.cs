﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DocumentStorage.Web.Models
{
    public class UserViewModel
    {
        public User EditedUser { get; set; }
        public List<User> ListOfUsers { get; set; }
    }
}