﻿// <copyright file="ISearcher.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DocumentStorage.Logic.Processes
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using DocumentStorage.Entities;
    using DocumentStorage.Repository;
    using DocumentStorage.Repository.Interfaces;

    /// <summary>
    /// The searcher methods.
    /// </summary>
    public interface ISearcher
    {
        /// <summary>
        /// The main method of processing.
        /// </summary>
        /// <param name="workfolder">The tesseract.exe workfolder.</param>
        /// <param name="filefolder">The images folder.</param>
        /// <param name="user">the user.</param>
        /// <param name="color">The color.</param>
        /// <returns>the words.</returns>
        string ProcessImageAndSave(string workfolder, string filefolder, string user, Color color);

        /// <summary>
        /// Adds a page to the DB.
        /// </summary>
        /// <param name="image">The image.</param>
        /// <param name="filename">The filename.</param>
        /// <param name="user">The user.</param>
        /// <param name="id">The id.</param>
        /// <returns>The id of the page.</returns>
        int AddPage(Bitmap image, string filename, string user, int id);

        /// <summary>
        /// Adds a line into the DB.
        /// </summary>
        /// <param name="value">The value of the line.</param>
        /// <param name="user">The user.</param>
        /// <param name="page_id">The page's id.</param>
        /// <param name="id">The id.</param>
        /// <returns>The line id.</returns>
        int AddLine(string value, string user, int page_id, int id);

        /// <summary>
        /// Inserts a word into the DB.
        /// </summary>
        /// <param name="rectangle">The position of the word.</param>
        /// <param name="value">The value of the word.</param>
        /// <param name="user">The activity user.</param>
        /// <param name="line_id">The line's id.</param>
        /// <param name="id">The id.</param>
        /// <returns>The word's id.</returns>
        int AddWord(Rectangle rectangle, string value, string user, int line_id, int id);

        /// <summary>
        /// Adds a user to the DB.
        /// </summary>
        /// <param name="name">The nam of the user.</param>
        /// <param name="admin">Is admin.</param>
        /// <returns>Message.</returns>
        string AddUser(string name, bool admin);

        /// <summary>
        /// Adds a rule to the DB.
        /// </summary>
        /// <param name="wrongChar">The wrong char.</param>
        /// <param name="rightChar">The correct char.</param>
        /// <param name="user">The acivity user.</param>
        /// <param name="type">The type of.</param>
        /// <returns>Message.</returns>
        string AddRule(string wrongChar, string rightChar, string user, int type);

        /// <summary>
        /// Gets the users from the DB.
        /// </summary>
        /// <returns>The users.</returns>
        IEnumerable<ITupleObj<int, string, string, DateTime>> GetUsers();

        /// <summary>
        /// Gets the pages specified to a user.
        /// </summary>
        /// <param name="user_id">The user's id.</param>
        /// <returns>The pages.</returns>
        List<ITupleObj<int, string, string, DateTime, int, int, List<ITupleObj<int, string, DateTime, int, int, int, int, int, int, string, List<ITupleObj<int, string, DateTime, int, int, int, int, int, int, string>>>>>> GetPagesByUser(int user_id);

        /// <summary>
        /// Gets the rules form the DB.
        /// </summary>
        /// <returns>The rules.</returns>
        IEnumerable<ITupleObj<int, string, string, int, DateTime>> Getrules();

        /// <summary>
        /// Returns the rules linked to a user.
        /// </summary>
        /// <returns>The rules.</returns>
        IEnumerable<ITupleObj<string, int>> GetRuleCountForUsers();

        /// <summary>
        /// Updates a user in the DB.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="name">The name.</param>
        /// <param name="admin">Is admin.</param>
        /// <returns>The message.</returns>
        string UpdateUser(int id, string name, bool admin);

        /// <summary>
        /// Updates a rule in the DB.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="rec_value">The rec_value.</param>
        /// <param name="cor_value">The cor_value.</param>
        /// <param name="type">The type.</param>
        /// <returns>The message.</returns>
        string UpdateRule(int id, string rec_value, string cor_value, int type);

        /// <summary>
        /// Updates a word in the DB.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="x">The x cord.</param>
        /// <param name="y">The y cord.</param>
        /// <param name="width">The witdth.</param>
        /// <param name="height">The height.</param>
        /// <param name="value">The value.</param>
        /// <returns>The message.</returns>
        string UpdateWord(int id, int x, int y, int width, int height, string value);

        /// <summary>
        /// Deletes a user from the DB.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns>The message.</returns>
        string DeleteUser(int id);

        /// <summary>
        /// Deletes a page from the DB.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns>The message.</returns>
        string DeletePage(int id);

        /// <summary>
        /// Deletes a rule from the DB.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns>The message.</returns>
        string DeleteRule(int id);

        /// <summary>
        /// Searches for the inpu word.
        /// </summary>
        /// <param name="page_id">The page to search.</param>
        /// <param name="searchWord">The word.</param>
        /// <returns>The list .</returns>
        string SearchForWord(int page_id, string searchWord);

        /// <summary>
        /// Gets the user's name by id.
        /// </summary>
        /// <param name="word_id">The user ID.</param>
        /// <returns>The name.</returns>
        ITupleObj<int, string, DateTime, int, int, int, int, int, int, string> GetWordByID(int word_id);

        /// <summary>
        /// Gets the line by id.
        /// </summary>
        /// <param name="line_id">The line ID.</param>
        /// <returns>The line.</returns>
        ITupleObj<int, string, DateTime, int, int, int, int, int, int, string> GetLineByID(int line_id);

        /// <summary>
        /// Gets the page by id.
        /// </summary>
        /// <param name="page_id">The page ID.</param>
        /// <returns>The page.</returns>
        ITupleObj<int, string, string, DateTime, int, int> GetPageByID(int page_id);

        /// <summary>
        /// Gets the user's name by id.
        /// </summary>
        /// <param name="user_id">The user ID.</param>
        /// <returns>The name.</returns>
        string GetUserNameByID(int user_id);

        /// <summary>
        /// Random User.
        /// </summary>
        /// <returns>The id.</returns>
        int GetUserID();
    }
}
