﻿// <copyright file="Searcher.cs" company="PlaceholderCompany">
// <copyright file="Searcher.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DocumentStorage.Logic.Processes
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using DocumentStorage.Entities;
    using DocumentStorage.Repository;
    using DocumentStorage.Repository.Interfaces;

    /// <summary>
    /// The searcher methods.
    /// </summary>
    public class Searcher : ISearcher
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Searcher"/> class.
        /// </summary>
        /// <param name="repoDb">the repository db.</param>
        public Searcher(IRepoDatabase repoDb)
        {
            this.Saver = new DataSaver();

            this.Digit = new Digitalizor();

            this.RepoDB = repoDb;
        }

        /// <summary>
        /// Gets or sets the repository layers databae.
        /// </summary>
        public IRepoDatabase RepoDB { get; set; }

        private DataSaver Saver { get; set; }

        private Digitalizor Digit { get; set; }

        /// <summary>
        /// The main method of processing.
        /// </summary>
        /// <param name="workfolder">The tesseract.exe workfolder.</param>
        /// <param name="filefolder">The images folder.</param>
        /// <param name="user">the user.</param>
        /// <param name="color">The color.</param>
        /// <returns>the words.</returns>
        public string ProcessImageAndSave(string workfolder, string filefolder, string user, Color color)
        {
            var pages = this.Digit.Digitalize(workfolder, filefolder);

            var refobj = this.Digit.CurrentPages;

            this.Saver.SavePageset(ref refobj, user);

            this.Digit.CurrentPages = refobj;
            return ">Successful Processing";
        }

        /// <summary>
        /// Adds a page to the DB.
        /// </summary>
        /// <param name="image">The image.</param>
        /// <param name="filename">The filename.</param>
        /// <param name="user">The user.</param>
        /// <param name="id">The id.</param>
        /// <returns>The id of the page.</returns>
        public int AddPage(Bitmap image, string filename, string user, int id)
        {
            return this.RepoDB.Insertpage(image, filename, user, id);
        }

        /// <summary>
        /// Adds a line into the DB.
        /// </summary>
        /// <param name="value">The value of the line.</param>
        /// <param name="user">The user.</param>
        /// <param name="page_id">The page's id.</param>
        /// <param name="id">The id.</param>
        /// <returns>The line id.</returns>
        public int AddLine(string value, string user, int page_id, int id)
        {
            return this.RepoDB.Insertline(value, user, page_id, id);
        }

        /// <summary>
        /// Inserts a word into the DB.
        /// </summary>
        /// <param name="rectangle">The position of the word.</param>
        /// <param name="value">The value of the word.</param>
        /// <param name="user">The activity user.</param>
        /// <param name="line_id">The line's id.</param>
        /// <param name="id">The id.</param>
        /// <returns>The word's id.</returns>
        public int AddWord(Rectangle rectangle, string value, string user, int line_id, int id)
        {
            return this.RepoDB.Insertword(rectangle, value, user, line_id, id);
        }

        /// <summary>
        /// Adds a user to the DB.
        /// </summary>
        /// <param name="name">The nam of the user.</param>
        /// <param name="admin">Is admin.</param>
        /// <returns>Message.</returns>
        public string AddUser(string name, bool admin)
        {
            this.RepoDB.Insertuser(name, admin, 0);

            return name + ">A user Succesfully added to the DB.";
        }

        /// <summary>
        /// Adds a rule to the DB.
        /// </summary>
        /// <param name="wrongChar">The wrong char.</param>
        /// <param name="rightChar">The correct char.</param>
        /// <param name="user">The acivity user.</param>
        /// <param name="type">The type of.</param>
        /// <returns>Message.</returns>
        public string AddRule(string wrongChar, string rightChar, string user, int type)
        {
            this.RepoDB.Insertrule(wrongChar, rightChar, this.RepoDB.Getusers(true).Where(x => x.Item2.ToLower() == user.ToLower()).First().Item1, this.RepoDB.Getrules().Count() + 1, type);

            return ">A rule Succesfully added to the DB.";
        }

        /// <summary>
        /// Gets the users from the DB.
        /// </summary>
        /// <returns>The users.</returns>
        public IEnumerable<ITupleObj<int, string, string, DateTime>> GetUsers()
        {
            return this.RepoDB.Getusers(true);
        }

        /// <summary>
        /// Gets the pages specified to a user.
        /// </summary>
        /// <param name="user_id">The user's id.</param>
        /// <returns>The pages.</returns>
        public List<ITupleObj<int, string, string, DateTime, int, int, List<ITupleObj<int, string, DateTime, int, int, int, int, int, int, string, List<ITupleObj<int, string, DateTime, int, int, int, int, int, int, string>>>>>> GetPagesByUser(int user_id)
        {
            return this.RepoDB.GetPagesByUser(user_id);
        }

        /// <summary>
        /// Gets the rules form the DB.
        /// </summary>
        /// <returns>The rules.</returns>
        public IEnumerable<ITupleObj<int, string, string, int, DateTime>> Getrules()
        {
            return this.RepoDB.Getrules();
        }

        /// <summary>
        /// Returns the rules linked to a user.
        /// </summary>
        /// <returns>The rules.</returns>
        public IEnumerable<ITupleObj<string, int>> GetRuleCountForUsers()
        {
            return this.RepoDB.GetRuleCountForUsers();
        }

        /// <summary>
        /// Updates a user in the DB.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="name">The name.</param>
        /// <param name="admin">Is admin.</param>
        /// <returns>The message.</returns>
        public string UpdateUser(int id, string name, bool admin)
        {
            this.RepoDB.UpdateUser(id, name, admin);
            return "Succesfully updated the user: " + name;
        }

        /// <summary>
        /// Updates a rule in the DB.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="rec_value">The rec_value.</param>
        /// <param name="cor_value">The cor_value.</param>
        /// <param name="type">The type.</param>
        /// <returns>The message.</returns>
        public string UpdateRule(int id, string rec_value, string cor_value, int type)
        {
            this.RepoDB.UpdateRule(id, rec_value, cor_value, type);
            return "Succesfully updated rule: " + id;
        }

        /// <summary>
        /// Updates a word in the DB.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="x">The x cord.</param>
        /// <param name="y">The y cord.</param>
        /// <param name="width">The witdth.</param>
        /// <param name="height">The height.</param>
        /// <param name="value">The value.</param>
        /// <returns>The message.</returns>
        public string UpdateWord(int id, int x, int y, int width, int height, string value)
        {
            this.RepoDB.UpdateWord(id, x, y, width, height, value);
            return "Succesfully updated word: " + id;
        }

        /// <summary>
        /// Deletes a user from the DB.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns>The message.</returns>
        public string DeleteUser(int id)
        {
            this.RepoDB.DeleteUser(id);
            return "Succesfully deleted user: " + id;
        }

        /// <summary>
        /// Deletes a page from the DB.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns>The message.</returns>
        public string DeletePage(int id)
        {
            this.RepoDB.DeletePage(id);
            return "Succesfully deleted page: " + id;
        }

        /// <summary>
        /// Deletes a rule from the DB.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns>The message.</returns>
        public string DeleteRule(int id)
        {
            this.RepoDB.DeleteRule(id);
            return "Succesfully deleted a rule: " + id;
        }

        /// <summary>
        /// Searches for the inpu word.
        /// </summary>
        /// <param name="page_id">The page to search.</param>
        /// <param name="searchWord">The word.</param>
        /// <returns>The list .</returns>
        public string SearchForWord(int page_id, string searchWord)
        {
            if (this.RepoDB.Getusers(true).Where(x => x.Item1 == page_id).Count() < 1)
            {
                throw new Exception("Wrong user id specified");
            }

            bool got = false;
            string retval = "|\tThe matching Words:\n";
            foreach (var line in this.RepoDB.Getlines().Where(x => x.Item5 == page_id))
            {
                foreach (var word in this.RepoDB.Getwords().Where(x => x.Item5 == line.Item1))
                {
                    if (word.Item10.ToLower() == searchWord.ToLower())
                    {
                        retval += "|\t ID: " + word.Item1 + "|\t Creation Date: " + word.Item3.ToLongDateString() + "|\t VALUE:" + word.Item10 + "\n";
                        got = true;
                    }
                }
            }

            if (!got)
            {
                return "No matches found";
            }

            return retval;
        }

        /// <summary>
        /// Gets the user's name by id.
        /// </summary>
        /// <param name="user_id">The user ID.</param>
        /// <returns>The name.</returns>
        public string GetUserNameByID(int user_id)
        {
            if (this.RepoDB.Getusers(true).Where(x => x.Item1 == user_id).Count() < 1)
            {
                throw new Exception("Wrong User ID");
            }

            return this.RepoDB.Getusers(true).Where(x => x.Item1 == user_id).First().Item2;
        }

        /// <summary>
        /// Gets the page by id.
        /// </summary>
        /// <param name="page_id">The page ID.</param>
        /// <returns>The page.</returns>
        public ITupleObj<int, string, string, DateTime, int, int> GetPageByID(int page_id)
        {
            if (this.RepoDB.Getpages().Where(x => x.Item1 == page_id).Count() < 1)
            {
                throw new Exception("Wrong page ID");
            }

            return this.RepoDB.Getpages().Where(x => x.Item1 == page_id).First();
        }

        /// <summary>
        /// Gets the line by id.
        /// </summary>
        /// <param name="line_id">The line ID.</param>
        /// <returns>The line.</returns>
        public ITupleObj<int, string, DateTime, int, int, int, int, int, int, string> GetLineByID(int line_id)
        {
            if (this.RepoDB.Getlines().Where(x => x.Item1 == line_id).Count() < 1)
            {
                throw new Exception("Wrong line ID");
            }

            return this.RepoDB.Getlines().Where(x => x.Item1 == line_id).First();
        }

        /// <summary>
        /// Gets the user's name by id.
        /// </summary>
        /// <param name="word_id">The user ID.</param>
        /// <returns>The name.</returns>
        public ITupleObj<int, string, DateTime, int, int, int, int, int, int, string> GetWordByID(int word_id)
        {
            if (this.RepoDB.Getwords().Where(x => x.Item1 == word_id).Count() < 1)
            {
                throw new Exception("Wrong Word ID");
            }

            return this.RepoDB.Getwords().Where(x => x.Item1 == word_id).First();
        }

        /// <summary>
        /// Random User.
        /// </summary>
        /// <returns>The id.</returns>
        public int GetUserID()
        {
            return this.RepoDB.GetUserName();
        }
    }
}
