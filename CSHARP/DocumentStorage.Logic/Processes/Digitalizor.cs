﻿// <copyright file="Digitalizor.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DocumentStorage.Logic.Processes
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using DocumentStorage.Entities;
    using DocumentStorage.Repository;
    using DocumentStorage.Repository.Interfaces;

    /// <summary>
    /// The Page processor class.
    /// </summary>
    public class Digitalizor
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Digitalizor"/> class.
        /// </summary>
        public Digitalizor()
        {
            this.Tesscop = new RepoTesseractCop();
            this.CurrentPages = new Dictionary<string, ITupleObj<Bitmap, List<ITupleObj<int, List<ITupleObj<Rectangle, string, int>>>>>>();
        }

        /// <summary>
        /// Gets or sets the pages.
        /// </summary>
        public Dictionary<string, ITupleObj<Bitmap, List<ITupleObj<int, List<ITupleObj<Rectangle, string, int>>>>>> CurrentPages { get; set; }

        private IRepoTesseractCop Tesscop { get; set; }

        /// <summary>
        /// Digitalize the images at the input directory.
        /// </summary>
        /// <param name="workfolder">The input directory.</param>
        /// <param name="filefolder">The exe directory.</param>
        /// <returns>The digitalized schema.</returns>
        public List<ITupleObj<Bitmap, List<ITupleObj<int, List<ITupleObj<Rectangle, string, int>>>>>> Digitalize(string workfolder, string filefolder)
        {
            List<Bitmap> images = new List<Bitmap>();
            string[] imageFiles = Directory.GetFiles(filefolder, "*.jpg", SearchOption.TopDirectoryOnly);
            foreach (var imageFile in imageFiles)
            {
                images.Add(new Bitmap(imageFile));
            }

            var retval = this.Tesscop.StructuredProcess(workfolder, "tsv", images);
            int i = 0;
            foreach (var item in imageFiles)
            {
                this.CurrentPages.Add(item, retval[i++]);
            }

            return retval;
        }

        /// <summary>
        /// Highlight the words on the input page images.
        /// </summary>
        /// <param name="pages">the input pages.</param>
        /// <param name="sewords">the matching words.</param>
        /// <param name="color">The color.</param>
        /// <returns>The pageset.</returns>
        public List<ITupleObj<Bitmap, List<ITupleObj<int, List<ITupleObj<Rectangle, string, int>>>>>> HighlightWords(List<ITupleObj<Bitmap, List<ITupleObj<int, List<ITupleObj<Rectangle, string, int>>>>>> pages, List<ITupleObj<Rectangle, string, int>> sewords, Color color)
        {
            return this.Tesscop.HighLightWords(pages, sewords, color);
        }
    }
}
