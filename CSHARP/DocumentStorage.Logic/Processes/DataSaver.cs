﻿// <copyright file="DataSaver.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DocumentStorage.Logic.Processes
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using DocumentStorage.Entities;
    using DocumentStorage.Repository;
    using DocumentStorage.Repository.Interfaces;

    /// <summary>
    /// The Data comm class.
    /// </summary>
    public class DataSaver
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataSaver"/> class.
        /// </summary>
        public DataSaver()
        {
            this.Database = new RepoDatabase();
        }

        private IRepoDatabase Database { get; set; }

        /// <summary>
        /// Svaes the input pageset to DB.
        /// </summary>
        /// <param name="dict">The input dictionary.</param>
        /// <param name="user">The activity user.</param>
        public void SavePageset(ref Dictionary<string, ITupleObj<Bitmap, List<ITupleObj<int, List<ITupleObj<Rectangle, string, int>>>>>> dict, string user)
        {
            var helper = dict;
            string[] toDelete = new string[dict.Count];
            int i = 0;
            foreach (var page in helper.Values)
            {
                if (page != null)
                {
                    int pageId = this.Database.Insertpage(page.Item1, helper.Where(x => x.Value == page).FirstOrDefault().Key, user, 0);
                    foreach (var line in page.Item2)
                    {
                        if (line != null)
                        {
                            int lineId = this.Database.Insertline(string.Empty, user, pageId, 0);
                            foreach (var word in line.Item2)
                            {
                                if (word != null)
                                {
                                    this.Database.Insertword(word.Item1, word.Item2, user, lineId, 0);
                                }
                            }
                        }
                    }
                }

                toDelete[i] = dict.Where(x => x.Value == page).First().Key;
            }

            for (int y = 0; y < toDelete.Length; y++)
            {
                dict.Remove(toDelete[i]);
            }
        }
    }
}
