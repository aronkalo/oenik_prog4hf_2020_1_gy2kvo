﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DocumentStorage.Program
{
    /// <summary>
    /// The executing class.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// The startup method.
        /// </summary>
        /// <param name="args">The input string parameters.</param>
        private static void Main(string[] args)
        {
            Monitor mon = new Monitor(new Logic.Processes.Searcher(new Repository.RepoDatabase()));
            mon.Welcome();
            var t = new System.Threading.Tasks.Task(() =>
            {
                try
                {
                    mon.StartUp();
                }
                catch (System.Exception ex)
                {
                    System.Console.WriteLine("|\tEx:" + ex.Message);
                    System.Console.WriteLine("|\tPress Enter To Countinue...");
                    System.Console.ReadKey();
                }
            });
            t.Start();
            bool run = true;
            while (run)
            {
                try
                {
                    run = mon.ChooseTask(mon.Menu());
                }
                catch (System.Exception ex)
                {
                    System.Console.WriteLine("|\tEx:" + ex.Message);
                    System.Console.WriteLine("|\tPress Enter To Countinue...");
                    System.Console.ReadKey();
                }
            }
        }
    }
}
