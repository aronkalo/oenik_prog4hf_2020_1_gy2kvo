﻿// <copyright file="Monitor.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DocumentStorage.Program
{
    using System;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using DocumentStorage.Entities;
    using DocumentStorage.Logic.Processes;

    /// <summary>
    /// The displayer class.
    /// </summary>
    public class Monitor
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Monitor"/> class.
        /// </summary>
        /// <param name="searcher">The obj.</param>
        public Monitor(ISearcher searcher)
        {
            this.Searcher = searcher;
        }

        private string WorkFolder { get; set; }

        private string FileFolder { get; set; }

        private ISearcher Searcher { get; set; }

        /// <summary>
        /// Starts the menu.
        /// </summary>
        /// <returns>User input id.</returns>
        public int Menu()
        {
            this.DisplayMenu();
            string userInput = Console.ReadLine();
            if (new string[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21" }.Contains(userInput))
            {
                return int.Parse(userInput);
            }
            else
            {
                Console.WriteLine("\t Incorrect choice, please Press Enter for the menu.");
                Console.ReadKey();
                return this.Menu();
            }
        }

        /// <summary>
        /// On Startup method.
        /// </summary>
        public void StartUp()
        {
            var location = Assembly.GetEntryAssembly().GetName().CodeBase;
            var splitted = location.Split(new string[] { "file:///" }, StringSplitOptions.None)[1];
            string folder = splitted.Split(new string[] { "bin" }, StringSplitOptions.None)[0];
            this.FileFolder = folder + "/TESSERACT/IMAGES";
            this.WorkFolder = folder + "/TESSERACT/";
            this.Searcher.AddUser("SYSTEM", true);
            this.Searcher.AddUser("JAVAADMIN", true);
            this.Searcher.AddUser("DBADMIN", true);
            this.Searcher.AddUser("OCRADMIN", true);
            this.Searcher.AddUser("Kalo Aron Zoltan", false);
            this.Searcher.AddUser("Sipos Cintia Rita", false);
        }

        /// <summary>
        /// The Main menu method.
        /// </summary>
        /// <param name="taskId">the user typed id.</param>
        /// <returns>exit.</returns>
        public bool ChooseTask(int taskId)
        {
            string lineStarter = "|\t";

            string name = string.Empty;

            string usethis = string.Empty;
            int useint = 0;

            switch (taskId)
            {
                case 1:
                    var users = this.Searcher.GetUsers();
                    foreach (var user in users)
                    {
                        Console.WriteLine(lineStarter + "ID: " + user.Item1 + "\n" + lineStarter + "NAME: " + user.Item2 + "\t" + lineStarter + "ISADMIN: " + user.Item3 + "\t" + lineStarter + "JOINDATE: " + user.Item4.ToShortDateString() + "\t");
                    }

                    break;
                case 2:
                    Console.WriteLine(lineStarter + ">Type in the username");
                    name = Console.ReadLine();
                    int usercount = this.Searcher.GetUsers().Where(z => z.Item2.ToLower() == name.ToLower()).Count();
                    if (usercount < 1)
                    {
                        Console.WriteLine(lineStarter + "Ex: wrong username.");
                    }
                    else
                    {
                        int user_id = this.Searcher.GetUsers().Where(z => z.Item2.ToLower() == name.ToLower()).First().Item1;
                        var pagesofuser = this.Searcher.GetPagesByUser(user_id);
                        Console.WriteLine(lineStarter + "Pages of user: " + name);
                        foreach (var userpage in pagesofuser)
                        {
                            Console.WriteLine(lineStarter + "ID: " + userpage.Item1 + "\t" + lineStarter + "FileName: " + userpage.Item3 + "\t" + lineStarter + "CREATE_DATE: " + userpage.Item4.ToShortTimeString() + "\t" + lineStarter + "DOC_TYPE: " + userpage.Item6 + "\t" + lineStarter + "LineCount: " + userpage.Item7.Count + "\t");
                        }
                    }

                    break;
                case 3:
                    Console.WriteLine(lineStarter + ">Type in the location of the ImageFolder:");
                    string imageFolder = Console.ReadLine();
                    if (Directory.Exists(imageFolder))
                    {
                        if (imageFolder.Substring(imageFolder.Length - 1, 1) != @"\")
                        {
                            imageFolder += @"\";
                        }

                        this.FileFolder = imageFolder;
                    }
                    else
                    {
                        Console.WriteLine(lineStarter + "Ex: Wrong location specified");
                    }

                    break;
                case 4:
                    Console.WriteLine(lineStarter + ">Type in the location of the tesseract.exe:");
                    string workFolder = Console.ReadLine();
                    if (workFolder.Substring(workFolder.Length - 1, 1) != @"\")
                    {
                        workFolder += @"\";
                    }

                    if (Directory.Exists(workFolder))
                    {
                        if (Directory.GetFiles(workFolder).Contains(workFolder + "tesseract.exe"))
                        {
                            this.WorkFolder = workFolder;
                        }
                        else
                        {
                            Console.WriteLine(lineStarter + "Ex: The directory not contains the exe file");
                        }
                    }
                    else
                    {
                        Console.WriteLine(lineStarter + "Ex: Wrong location specified");
                    }

                    break;
                case 5:
                    this.ProcessPages(lineStarter);
                    break;
                case 6:
                    Console.WriteLine(lineStarter + ">Type in the username");
                    name = Console.ReadLine();
                    int u = this.Searcher.GetUsers().Where(z => z.Item2.ToLower() == name.ToLower()).Count();
                    if (u < 1)
                    {
                        throw new Exception("> Ex: wrong username.");
                    }

                    Console.WriteLine(lineStarter + ">Type in the wrong word");
                    string wrongChar = Console.ReadLine();

                    Console.WriteLine(lineStarter + ">Type in the right word");
                    string rightChar = Console.ReadLine();

                    Console.WriteLine(lineStarter + ">Type in the typeof(1 or 2)");
                    string typestring = Console.ReadLine();
                    if (new string[] { "1", "2" }.Contains(typestring))
                    {
                       int type1 = int.Parse(typestring);
                       Console.WriteLine(lineStarter + this.Searcher.AddRule(wrongChar, rightChar, name, type1));
                    }
                    else
                    {
                        Console.WriteLine(lineStarter + "Ex: invalid type value");
                    }

                    break;
                case 7:
                    Console.WriteLine(lineStarter + "Add a new user");
                    Console.WriteLine(lineStarter + "Type in The username:");
                    name = Console.ReadLine();
                    Console.WriteLine(lineStarter + this.Searcher.AddUser(name, false));
                    break;
                case 8:
                    Console.WriteLine(lineStarter + "Update a user");
                    Console.WriteLine(lineStarter + "Type in the user's ID");
                    string idstring = Console.ReadLine();
                    int id = 0;
                    int.TryParse(idstring, out id);
                    if (id == 0)
                    {
                        throw new Exception("Wrong user id specified.");
                    }

                    Console.WriteLine(lineStarter + "Want to give admin privilige? If yes then press 1 and Enter.");
                    string boolstring = Console.ReadLine();
                    bool admin = false;
                    int boolint = 0;
                    int.TryParse(idstring, out boolint);
                    if (boolint == 1)
                    {
                        admin = true;
                    }

                    Console.WriteLine(lineStarter + "Type in the user's name, if dont want to update, just press Enter.");
                    name = Console.ReadLine();

                    Console.WriteLine(lineStarter + this.Searcher.UpdateUser(id, name, admin));
                    break;
                case 9:
                    Console.WriteLine(lineStarter + "Update a rule");
                    Console.WriteLine(lineStarter + "Type in the rule's ID");
                    string idstring1 = Console.ReadLine();
                    int id1 = 0;
                    int.TryParse(idstring1, out id1);
                    if (id1 == 0)
                    {
                        throw new Exception("Wrong rule id specified.");
                    }

                    Console.WriteLine(lineStarter + "Type in the wrong text, if dont want to update just press Enter.");
                    string rec_value = Console.ReadLine();
                    Console.WriteLine(lineStarter + "Type in the correct text, if dont want to update just press Enter.");
                    string cor_value = Console.ReadLine();
                    Console.WriteLine(lineStarter + "Type in the typeof(1 or 2).");
                    string typestr = Console.ReadLine();
                    if (!new string[] { "1", "2" }.Contains(typestr))
                    {
                        throw new Exception("Wrong rule type specified.");
                    }

                    int type = int.Parse(typestr);
                    Console.WriteLine(lineStarter + this.Searcher.UpdateRule(id1, rec_value, cor_value, type));
                    break;
                case 10:
                    Console.WriteLine(lineStarter + "Update a word");
                    Console.WriteLine(lineStarter + "Type in the word's ID");
                    string idstring2 = Console.ReadLine();
                    int id2 = 0;
                    int.TryParse(idstring2, out id2);
                    if (id2 == 0)
                    {
                        throw new Exception("Wrong word id specified.");
                    }

                    Console.WriteLine(lineStarter + "Type in the x value");
                    idstring2 = Console.ReadLine();
                    int x = 0;
                    int.TryParse(idstring2, out x);
                    if (x == 0)
                    {
                        throw new Exception("Wrong rule x pos specified.");
                    }

                    Console.WriteLine(lineStarter + "Type in the y value");
                    idstring2 = Console.ReadLine();
                    int y = 0;
                    int.TryParse(idstring2, out y);
                    if (y == 0)
                    {
                        throw new Exception("Wrong rule x pos specified.");
                    }

                    Console.WriteLine(lineStarter + "Type in the width value");
                    idstring2 = Console.ReadLine();
                    int width = 0;
                    int.TryParse(idstring2, out width);
                    if (width == 0)
                    {
                        throw new Exception("Wrong rule width specified.");
                    }

                    Console.WriteLine(lineStarter + "Type in the heigth value");
                    idstring2 = Console.ReadLine();
                    int heigth = 0;
                    int.TryParse(idstring2, out heigth);
                    if (heigth == 0)
                    {
                        throw new Exception("Wrong rule height pos specified.");
                    }

                    Console.WriteLine(lineStarter + "Type in the text value");
                    idstring2 = Console.ReadLine();
                    Console.WriteLine(lineStarter + this.Searcher.UpdateWord(id2, x, y, width, heigth, idstring2));

                    break;
                case 11:
                    Console.WriteLine(lineStarter + "Delete a user.");
                    Console.WriteLine(lineStarter + "Type in the word's ID");
                    string idstring3 = Console.ReadLine();
                    int id3 = 0;
                    int.TryParse(idstring3, out id3);
                    if (id3 == 0)
                    {
                        throw new Exception("Wrong rule id specified.");
                    }

                    Console.WriteLine(lineStarter + this.Searcher.DeleteUser(id3));
                    break;
                case 12:
                    Console.WriteLine(lineStarter + "Delete a page.");
                    Console.WriteLine(lineStarter + "Type in the page's ID");
                    string idstring4 = Console.ReadLine();
                    int id4 = 0;
                    int.TryParse(idstring4, out id4);
                    if (id4 == 0)
                    {
                        throw new Exception("Wrong rule id specified.");
                    }

                    Console.WriteLine(lineStarter + this.Searcher.DeletePage(id4));
                    break;
                case 13:
                    Console.WriteLine(lineStarter + "Delete a rule.");
                    Console.WriteLine(lineStarter + "Type in the word's ID");
                    usethis = Console.ReadLine();
                    useint = 0;
                    int.TryParse(usethis, out useint);
                    if (useint == 0)
                    {
                        throw new Exception("Wrong rule id specified.");
                    }

                    Console.WriteLine(lineStarter + this.Searcher.DeleteRule(useint));
                    break;
                case 14:
                    Console.WriteLine(lineStarter + "The User Name:" + this.Searcher.GetUserNameByID(this.Searcher.GetUserID()));
                    break;
                case 15:
                    Console.WriteLine(lineStarter + "Type in the page's id");
                    string idstring6 = Console.ReadLine();
                    int id6 = 0;
                    int.TryParse(idstring6, out id6);
                    if (id6 == 0)
                    {
                        throw new Exception("Wrong page id specified.");
                    }

                    Console.WriteLine(lineStarter + "Type in the searched word.");
                    string sword = Console.ReadLine();
                    Console.WriteLine(this.Searcher.SearchForWord(id6, sword));
                    break;
                case 16:
                    var rulescounts = this.Searcher.GetRuleCountForUsers();
                    foreach (var user in rulescounts)
                    {
                        Console.WriteLine(lineStarter + "User: " + user.Item1 + " |\t Rules: " + user.Item2 + " |");
                    }

                    break;
                case 17:
                    Console.WriteLine(lineStarter + "Type in the user's ID");
                    usethis = Console.ReadLine();
                    useint = 0;
                    int.TryParse(usethis, out useint);
                    if (useint == 0)
                    {
                        throw new Exception("Wrong user id specified.");
                    }

                    Console.WriteLine(lineStarter + "The Searched User's Name: " + this.Searcher.GetUserNameByID(useint));

                    break;
                case 18:
                    Console.WriteLine(lineStarter + "Type in the page's ID");
                    usethis = Console.ReadLine();
                    useint = 0;
                    int.TryParse(usethis, out useint);
                    if (useint == 0)
                    {
                        throw new Exception("Wrong page id specified.");
                    }

                    var page = this.Searcher.GetPageByID(useint);
                    Console.WriteLine(lineStarter + "ID: " + page.Item1 + " Filename: " + page.Item3 + " Create Date: " + page.Item4 + " Document tpye: " + page.Item6);

                    break;
                case 19:
                    Console.WriteLine(lineStarter + "Type in the line's ID");
                    usethis = Console.ReadLine();
                    useint = 0;
                    int.TryParse(usethis, out useint);
                    if (useint == 0)
                    {
                        throw new Exception("Wrong line id specified.");
                    }

                    var line = this.Searcher.GetLineByID(useint);
                    Console.WriteLine(lineStarter + "ID: " + line.Item1 + " Create Date: " + line.Item3 + " Create user's Id: " + line.Item4);

                    break;
                case 20:
                    Console.WriteLine(lineStarter + "Type in the word's ID");
                    usethis = Console.ReadLine();
                    useint = 0;
                    int.TryParse(usethis, out useint);
                    if (useint == 0)
                    {
                        throw new Exception("Wrong word id specified.");
                    }

                    var word = this.Searcher.GetWordByID(useint);
                    Console.WriteLine(lineStarter + "ID: " + word.Item1 + " Value: " + word.Item10 + " Create Date: " + word.Item3);
                    break;
                case 21:

                    break;
            }

            GC.Collect();
            if (taskId == 21)
            {
                return false;
            }
            else
            {
                this.ReMenu(lineStarter);
                return true;
            }
        }

        /// <summary>
        /// The welcomer method.
        /// </summary>
        public void Welcome()
        {
            Console.WriteLine("Welcome to the DocumentStorage System:");
            Console.WriteLine(" Date:" + DateTime.Now.ToString("yyyy.MM.dd").ToString());
            Console.WriteLine("Press Enter for menu.");
        }

        private void DisplayMenu()
        {
            Console.Clear();
            string lineStarter = "|\t";
            string separator = "------------------------------------------------------------------------------\n------------------------------------------------------------------------------";
            Console.WriteLine(separator);
            Console.WriteLine(lineStarter + "\t\t\t |#########|");
            Console.WriteLine(lineStarter + "\t\t\t |Main Menu|");
            Console.WriteLine(lineStarter + "\t\t\t |#########|\n|");
            Console.WriteLine(lineStarter + " 1.\t List Users");
            Console.WriteLine(lineStarter + " 2.\t List the pages of a specified user");
            Console.WriteLine(lineStarter + " 3.\t Set the ImageFolder");
            Console.WriteLine(lineStarter + " 4.\t Set the tesseract.exe folder");
            Console.WriteLine(lineStarter + " 5.\t Process Files from ImageFolder");
            Console.WriteLine(lineStarter + " 6.\t Add new rule");
            Console.WriteLine(lineStarter + " 7.\t Add new user");
            Console.WriteLine(lineStarter + " 8.\t update user");
            Console.WriteLine(lineStarter + " 9.\t update rule");
            Console.WriteLine(lineStarter + " 10.\t update word");
            Console.WriteLine(lineStarter + " 11.\t delete user");
            Console.WriteLine(lineStarter + " 12.\t delete page");
            Console.WriteLine(lineStarter + " 13.\t delete rule");
            Console.WriteLine(lineStarter + " 14.\t Get data from JAVA endpoint");
            Console.WriteLine(lineStarter + " 15.\t Search for word in specified page.");
            Console.WriteLine(lineStarter + " 16.\t List rulecount for users.");
            Console.WriteLine(lineStarter + " 17.\t Get a user by ID.");
            Console.WriteLine(lineStarter + " 18.\t Get a page by iD.");
            Console.WriteLine(lineStarter + " 19.\t Get a line by ID.");
            Console.WriteLine(lineStarter + " 20.\t Get a word by ID.");
            Console.WriteLine(lineStarter + " 21.\t Exit from the application");
            Console.WriteLine(separator);
            Console.WriteLine(lineStarter + "\t Type in the Number of the task:");
            Console.WriteLine(separator);
        }

        private void ReMenu(string lineStarter)
        {
            Console.WriteLine(lineStarter + "Press Enter To Continue.");
            Console.ReadKey();
        }

        private void ProcessPages(string lineStarter)
        {
            if (this.WorkFolder == string.Empty)
            {
                throw new Exception("Ex: The WorkFolder not specified.");
            }
            else if (this.FileFolder == string.Empty)
            {
                throw new Exception("Ex: The FileFolder not specified.");
            }
            else
            {
                Console.WriteLine(lineStarter + "Specify the activity user's name");
                string user_name = Console.ReadLine();
                int usercount = this.Searcher.GetUsers().Where(x => x.Item2.ToLower() == user_name.ToLower()).Count();
                if (usercount > 0)
                {
                    Console.WriteLine(lineStarter + "The image processing is running on backgorund.");
                    var t = new System.Threading.Tasks.Task(() =>
                    {
                        try
                        {
                            this.Searcher.ProcessImageAndSave(this.WorkFolder, this.FileFolder, user_name, Color.Blue);
                        }
                        catch (System.Exception ex)
                        {
                        System.Console.WriteLine("|\tEx:" + ex.Message);
                        System.Console.WriteLine("|\tPress Enter To Countinue...");
                        System.Console.ReadKey();
                        }

                    });
                    t.Start();
                }
                else
                {
                    throw new Exception("Wrong Username, not in users.");
                }
            }
        }
    }
}
