var class_document_storage_1_1_word_1_1_syntax_corrector =
[
    [ "SyntaxCorrector", "class_document_storage_1_1_word_1_1_syntax_corrector.html#a0e09cb822d0bde6b65eb4b282751b203", null ],
    [ "AddRule", "class_document_storage_1_1_word_1_1_syntax_corrector.html#a195da86d1bf707b3fe2c4c4331e5c750", null ],
    [ "CorrectWordSet", "class_document_storage_1_1_word_1_1_syntax_corrector.html#a36c540edc6ebb27aa509a1df6c8bc3e7", null ],
    [ "ModifyRuleSet", "class_document_storage_1_1_word_1_1_syntax_corrector.html#afae98a9f0713b20ee7517c01c0b1d4e6", null ],
    [ "RuleSet", "class_document_storage_1_1_word_1_1_syntax_corrector.html#a776497a851b6175c3d29b6306080fc8b", null ]
];