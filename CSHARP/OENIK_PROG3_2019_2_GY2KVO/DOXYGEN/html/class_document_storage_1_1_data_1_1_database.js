var class_document_storage_1_1_data_1_1_database =
[
    [ "Database", "class_document_storage_1_1_data_1_1_database.html#ae64855926ac7bf68db66e880047ca82f", null ],
    [ "DeletePage", "class_document_storage_1_1_data_1_1_database.html#a7aa5c9387234e093ccf9261fa329459b", null ],
    [ "DeleteRule", "class_document_storage_1_1_data_1_1_database.html#a4f24bb6fa33e20e87a506cb08f6a1a4a", null ],
    [ "DeleteUser", "class_document_storage_1_1_data_1_1_database.html#aac414b3d232212c2c6ec7a8797d415e0", null ],
    [ "Getlines", "class_document_storage_1_1_data_1_1_database.html#ac1e36367c29b0619316824f23c2ef868", null ],
    [ "Getpages", "class_document_storage_1_1_data_1_1_database.html#a0731673abb4aa2ed46d73160f29c7ca8", null ],
    [ "GetPagesByUser", "class_document_storage_1_1_data_1_1_database.html#a9bda5fe9b0a0e05be8366f771f4065b9", null ],
    [ "GetRuleCountForUsers", "class_document_storage_1_1_data_1_1_database.html#aaee891e14d8453afcc6702d3fd8f0133", null ],
    [ "Getrules", "class_document_storage_1_1_data_1_1_database.html#a3e99ab3c0ea247370a2cc7fb13191fe2", null ],
    [ "Getusers", "class_document_storage_1_1_data_1_1_database.html#a306e6f096159c7eb1f88fa263c2399a1", null ],
    [ "Getwords", "class_document_storage_1_1_data_1_1_database.html#aca7b02a45f11441f0571dd00731e9997", null ],
    [ "Insertline", "class_document_storage_1_1_data_1_1_database.html#a51a741092cab61885d967cfcc2198f2c", null ],
    [ "Insertpage", "class_document_storage_1_1_data_1_1_database.html#a09feb60c34240de771d032bb591cbb53", null ],
    [ "Insertrule", "class_document_storage_1_1_data_1_1_database.html#ac03cc065f87201a6116cc8d74a53667c", null ],
    [ "Insertuser", "class_document_storage_1_1_data_1_1_database.html#a806f7259a9909902b996b3db83a65101", null ],
    [ "Insertword", "class_document_storage_1_1_data_1_1_database.html#a9a13d119cb3f925f01b6b40371aa2153", null ],
    [ "Startup", "class_document_storage_1_1_data_1_1_database.html#a666c72df2f8ed0c8aee5a9e139a9fcc5", null ],
    [ "UpdateRule", "class_document_storage_1_1_data_1_1_database.html#a32106e6e75384ec229ed77139abeea89", null ],
    [ "UpdateUser", "class_document_storage_1_1_data_1_1_database.html#a241b0779a5104cffffe8a8e6d85072ac", null ],
    [ "UpdateWord", "class_document_storage_1_1_data_1_1_database.html#a681a571ab1a5447b4b1fb15132f664d7", null ],
    [ "DbEntities", "class_document_storage_1_1_data_1_1_database.html#aca8b2a03735034ddf15669d6e26219f3", null ]
];