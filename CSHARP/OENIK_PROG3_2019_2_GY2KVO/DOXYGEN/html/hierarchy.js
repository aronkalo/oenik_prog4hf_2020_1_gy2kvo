var hierarchy =
[
    [ "DocumentStorage.Logic.Processes.DataSaver", "class_document_storage_1_1_logic_1_1_processes_1_1_data_saver.html", null ],
    [ "DbContext", null, [
      [ "DocumentStorage.Data::LocalDBEntities", "class_document_storage_1_1_data_1_1_local_d_b_entities.html", null ]
    ] ],
    [ "DocumentStorage.Logic.Processes.Digitalizor", "class_document_storage_1_1_logic_1_1_processes_1_1_digitalizor.html", null ],
    [ "DocumentStorage.JavaWeb.IClient", "interface_document_storage_1_1_java_web_1_1_i_client.html", [
      [ "DocumentStorage.JavaWeb.Client", "class_document_storage_1_1_java_web_1_1_client.html", null ]
    ] ],
    [ "DocumentStorage.Data.IDatabase", "interface_document_storage_1_1_data_1_1_i_database.html", [
      [ "DocumentStorage.Data.Database", "class_document_storage_1_1_data_1_1_database.html", null ]
    ] ],
    [ "DocumentStorage.OCR.PreProcessing.ImageProcessor", "class_document_storage_1_1_o_c_r_1_1_pre_processing_1_1_image_processor.html", null ],
    [ "DocumentStorage.Repository.Interfaces.IRepoDatabase", "interface_document_storage_1_1_repository_1_1_interfaces_1_1_i_repo_database.html", [
      [ "DocumentStorage.Repository.RepoDatabase", "class_document_storage_1_1_repository_1_1_repo_database.html", null ]
    ] ],
    [ "DocumentStorage.Repository.Interfaces.IRepoTesseractCop", "interface_document_storage_1_1_repository_1_1_interfaces_1_1_i_repo_tesseract_cop.html", [
      [ "DocumentStorage.Repository.RepoTesseractCop", "class_document_storage_1_1_repository_1_1_repo_tesseract_cop.html", null ]
    ] ],
    [ "DocumentStorage.Repository.Interfaces.IRepoWordCorrector", "interface_document_storage_1_1_repository_1_1_interfaces_1_1_i_repo_word_corrector.html", null ],
    [ "DocumentStorage.Logic.Processes.ISearcher", "interface_document_storage_1_1_logic_1_1_processes_1_1_i_searcher.html", [
      [ "DocumentStorage.Logic.Processes.Searcher", "class_document_storage_1_1_logic_1_1_processes_1_1_searcher.html", null ]
    ] ],
    [ "DocumentStorage.Word.Interfaces.ISpellChecker", "interface_document_storage_1_1_word_1_1_interfaces_1_1_i_spell_checker.html", [
      [ "DocumentStorage.Word.SpellChecker", "class_document_storage_1_1_word_1_1_spell_checker.html", null ]
    ] ],
    [ "DocumentStorage.Word.Interfaces.ISyntaxCorrector", "interface_document_storage_1_1_word_1_1_interfaces_1_1_i_syntax_corrector.html", [
      [ "DocumentStorage.Word.SyntaxCorrector", "class_document_storage_1_1_word_1_1_syntax_corrector.html", null ]
    ] ],
    [ "DocumentStorage.OCR.Interfaces.ITesseractCop", "interface_document_storage_1_1_o_c_r_1_1_interfaces_1_1_i_tesseract_cop.html", [
      [ "DocumentStorage.OCR.Tesseract.TesseractCop", "class_document_storage_1_1_o_c_r_1_1_tesseract_1_1_tesseract_cop.html", null ]
    ] ],
    [ "DocumentStorage.Word.Interfaces.ITranslator", "interface_document_storage_1_1_word_1_1_interfaces_1_1_i_translator.html", [
      [ "DocumentStorage.Word.Translator", "class_document_storage_1_1_word_1_1_translator.html", null ]
    ] ],
    [ "DocumentStorage.Entities.ITupleObj< TQ, TW, TE, TR >", "interface_document_storage_1_1_entities_1_1_i_tuple_obj.html", null ],
    [ "DocumentStorage.Entities.ITupleObj< TQ, TW, TE, TR, T, TZ, TU, TI, TO, TP, TA, TS >", "interface_document_storage_1_1_entities_1_1_i_tuple_obj.html", [
      [ "DocumentStorage.Entities.TupleObj< T >", "class_document_storage_1_1_entities_1_1_tuple_obj.html", null ]
    ] ],
    [ "DocumentStorage.Entities.ITupleObj< TQ, TW, TE, TR, T >", "interface_document_storage_1_1_entities_1_1_i_tuple_obj.html", null ],
    [ "DocumentStorage.Entities.ITupleObj< T, TW, TE >", "interface_document_storage_1_1_entities_1_1_i_tuple_obj.html", null ],
    [ "DocumentStorage.Entities.ITupleObj< T, TW >", "interface_document_storage_1_1_entities_1_1_i_tuple_obj.html", null ],
    [ "DocumentStorage.Entities.ITupleObj< TQ, TW, TE, TR, T, TZ, TU, TI, TO, TP, TA >", "interface_document_storage_1_1_entities_1_1_i_tuple_obj.html", null ],
    [ "DocumentStorage.Entities.ITupleObj< T >", "interface_document_storage_1_1_entities_1_1_i_tuple_obj.html", null ],
    [ "DocumentStorage.Entities.ITupleObj< T, TW, TE, TR, TQ, TZ, TU, TI, TO, TP >", "interface_document_storage_1_1_entities_1_1_i_tuple_obj.html", null ],
    [ "DocumentStorage.Entities.ITupleObj< TQ, TW, TE, TR, T, TZ, TU, TI, TO >", "interface_document_storage_1_1_entities_1_1_i_tuple_obj.html", null ],
    [ "DocumentStorage.Entities.ITupleObj< TQ, TW, TE, TR, T, TZ, TU, TI >", "interface_document_storage_1_1_entities_1_1_i_tuple_obj.html", null ],
    [ "DocumentStorage.Entities.ITupleObj< TQ, TW, TE, TR, T, TZ, TU >", "interface_document_storage_1_1_entities_1_1_i_tuple_obj.html", null ],
    [ "DocumentStorage.Entities.ITupleObj< TQ, TW, TE, TR, T, TZ >", "interface_document_storage_1_1_entities_1_1_i_tuple_obj.html", null ],
    [ "DocumentStorage.Entities.ITupleObj< T >", "interface_document_storage_1_1_entities_1_1_i_tuple_obj.html", [
      [ "DocumentStorage.Entities.TupleObj< T >", "class_document_storage_1_1_entities_1_1_tuple_obj.html", null ]
    ] ],
    [ "DocumentStorage.Entities.ITupleObj< T, TW >", "interface_document_storage_1_1_entities_1_1_i_tuple_obj.html", [
      [ "DocumentStorage.Entities.TupleObj< T >", "class_document_storage_1_1_entities_1_1_tuple_obj.html", null ]
    ] ],
    [ "DocumentStorage.Entities.ITupleObj< T, TW, TE >", "interface_document_storage_1_1_entities_1_1_i_tuple_obj.html", [
      [ "DocumentStorage.Entities.TupleObj< T >", "class_document_storage_1_1_entities_1_1_tuple_obj.html", null ]
    ] ],
    [ "DocumentStorage.Entities.ITupleObj< T, TW, TE, TR, TQ, TZ, TU, TI, TO, TP >", "interface_document_storage_1_1_entities_1_1_i_tuple_obj.html", [
      [ "DocumentStorage.Entities.TupleObj< T >", "class_document_storage_1_1_entities_1_1_tuple_obj.html", null ]
    ] ],
    [ "DocumentStorage.Entities.ITupleObj< TQ, TW, TE, TR >", "interface_document_storage_1_1_entities_1_1_i_tuple_obj.html", [
      [ "DocumentStorage.Entities.TupleObj< T >", "class_document_storage_1_1_entities_1_1_tuple_obj.html", null ]
    ] ],
    [ "DocumentStorage.Entities.ITupleObj< TQ, TW, TE, TR, T >", "interface_document_storage_1_1_entities_1_1_i_tuple_obj.html", [
      [ "DocumentStorage.Entities.TupleObj< T >", "class_document_storage_1_1_entities_1_1_tuple_obj.html", null ]
    ] ],
    [ "DocumentStorage.Entities.ITupleObj< TQ, TW, TE, TR, T, TZ >", "interface_document_storage_1_1_entities_1_1_i_tuple_obj.html", [
      [ "DocumentStorage.Entities.TupleObj< T >", "class_document_storage_1_1_entities_1_1_tuple_obj.html", null ]
    ] ],
    [ "DocumentStorage.Entities.ITupleObj< TQ, TW, TE, TR, T, TZ, TU >", "interface_document_storage_1_1_entities_1_1_i_tuple_obj.html", [
      [ "DocumentStorage.Entities.TupleObj< T >", "class_document_storage_1_1_entities_1_1_tuple_obj.html", null ]
    ] ],
    [ "DocumentStorage.Entities.ITupleObj< TQ, TW, TE, TR, T, TZ, TU, TI >", "interface_document_storage_1_1_entities_1_1_i_tuple_obj.html", [
      [ "DocumentStorage.Entities.TupleObj< T >", "class_document_storage_1_1_entities_1_1_tuple_obj.html", null ]
    ] ],
    [ "DocumentStorage.Entities.ITupleObj< TQ, TW, TE, TR, T, TZ, TU, TI, TO >", "interface_document_storage_1_1_entities_1_1_i_tuple_obj.html", [
      [ "DocumentStorage.Entities.TupleObj< T >", "class_document_storage_1_1_entities_1_1_tuple_obj.html", null ]
    ] ],
    [ "DocumentStorage.Entities.ITupleObj< TQ, TW, TE, TR, T, TZ, TU, TI, TO, TP, TA >", "interface_document_storage_1_1_entities_1_1_i_tuple_obj.html", [
      [ "DocumentStorage.Entities.TupleObj< T >", "class_document_storage_1_1_entities_1_1_tuple_obj.html", null ]
    ] ],
    [ "DocumentStorage.OCR.Entities.Line", "class_document_storage_1_1_o_c_r_1_1_entities_1_1_line.html", null ],
    [ "DocumentStorage.Data.Line", "class_document_storage_1_1_data_1_1_line.html", null ],
    [ "DocumentStorage.Logic.Tests.Mocker", "class_document_storage_1_1_logic_1_1_tests_1_1_mocker.html", null ],
    [ "DocumentStorage.Program.Monitor", "class_document_storage_1_1_program_1_1_monitor.html", null ],
    [ "DocumentStorage.OCR.Entities.Page", "class_document_storage_1_1_o_c_r_1_1_entities_1_1_page.html", null ],
    [ "DocumentStorage.Data.Page", "class_document_storage_1_1_data_1_1_page.html", null ],
    [ "DocumentStorage.Program.Program", "class_document_storage_1_1_program_1_1_program.html", null ],
    [ "DocumentStorage.Repository.RepoWordCorrector", "class_document_storage_1_1_repository_1_1_repo_word_corrector.html", null ],
    [ "DocumentStorage.Data.Rule", "class_document_storage_1_1_data_1_1_rule.html", null ],
    [ "DocumentStorage.OCR.Tesseract.TesseractEngine", "class_document_storage_1_1_o_c_r_1_1_tesseract_1_1_tesseract_engine.html", null ],
    [ "DocumentStorage.Entities.TupleObj< TQ, TW, TE, TR, T >", "class_document_storage_1_1_entities_1_1_tuple_obj.html", null ],
    [ "DocumentStorage.Entities.TupleObj< TQ, TW, TE, TR, T, TZ >", "class_document_storage_1_1_entities_1_1_tuple_obj.html", null ],
    [ "DocumentStorage.Entities.TupleObj< T, TW >", "class_document_storage_1_1_entities_1_1_tuple_obj.html", null ],
    [ "DocumentStorage.Entities.TupleObj< T, TW, TE >", "class_document_storage_1_1_entities_1_1_tuple_obj.html", null ],
    [ "DocumentStorage.Entities.TupleObj< TQ, TW, TE, TR, T, TZ, TU >", "class_document_storage_1_1_entities_1_1_tuple_obj.html", null ],
    [ "DocumentStorage.Entities.TupleObj< TQ, TW, TE, TR >", "class_document_storage_1_1_entities_1_1_tuple_obj.html", null ],
    [ "DocumentStorage.Entities.TupleObj< TQ, TW, TE, TR, T, TZ, TU, TI, TO >", "class_document_storage_1_1_entities_1_1_tuple_obj.html", null ],
    [ "DocumentStorage.Entities.TupleObj< TQ, TW, TE, TR, T, TZ, TU, TI, TO, TP, TA >", "class_document_storage_1_1_entities_1_1_tuple_obj.html", null ],
    [ "DocumentStorage.Entities.TupleObj< TQ, TW, TE, TR, T, TZ, TU, TI >", "class_document_storage_1_1_entities_1_1_tuple_obj.html", null ],
    [ "DocumentStorage.Entities.TupleObj< T, TW, TE, TR, TQ, TZ, TU, TI, TO, TP >", "class_document_storage_1_1_entities_1_1_tuple_obj.html", null ],
    [ "DocumentStorage.Entities.TupleObj< TQ, TW, TE, TR, T, TZ, TU, TI, TO, TP, TA, TS >", "class_document_storage_1_1_entities_1_1_tuple_obj.html", null ],
    [ "DocumentStorage.Data.User", "class_document_storage_1_1_data_1_1_user.html", null ],
    [ "DocumentStorage.OCR.Entities.Word", "class_document_storage_1_1_o_c_r_1_1_entities_1_1_word.html", null ],
    [ "DocumentStorage.Data.Word", "class_document_storage_1_1_data_1_1_word.html", null ]
];