var class_document_storage_1_1_o_c_r_1_1_pre_processing_1_1_image_processor =
[
    [ "ConvertToBlackAndWhite", "class_document_storage_1_1_o_c_r_1_1_pre_processing_1_1_image_processor.html#a0afeb5f519d994a7dbfc574de3f571ff", null ],
    [ "GrayScale", "class_document_storage_1_1_o_c_r_1_1_pre_processing_1_1_image_processor.html#aefe50129d0a9f3a5d42a0d047218a282", null ],
    [ "Invert", "class_document_storage_1_1_o_c_r_1_1_pre_processing_1_1_image_processor.html#a5bba0b73428bbe70c740d2774e490531", null ],
    [ "Image", "class_document_storage_1_1_o_c_r_1_1_pre_processing_1_1_image_processor.html#a211b1fc85fc3cad95338bba00b1e0ded", null ]
];