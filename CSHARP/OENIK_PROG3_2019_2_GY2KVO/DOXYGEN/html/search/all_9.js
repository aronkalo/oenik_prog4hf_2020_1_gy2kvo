var searchData=
[
  ['page_107',['Page',['../class_document_storage_1_1_o_c_r_1_1_entities_1_1_page.html',1,'DocumentStorage.OCR.Entities.Page'],['../class_document_storage_1_1_data_1_1_page.html',1,'DocumentStorage.Data.Page']]],
  ['pages_108',['Pages',['../class_document_storage_1_1_o_c_r_1_1_tesseract_1_1_tesseract_cop.html#a800891b126961773de414f91a0f2b0db',1,'DocumentStorage::OCR::Tesseract::TesseractCop']]],
  ['position_109',['Position',['../class_document_storage_1_1_o_c_r_1_1_entities_1_1_word.html#a8ba81cafbd6314eb852491b6784ad782',1,'DocumentStorage::OCR::Entities::Word']]],
  ['processimageandsave_110',['ProcessImageAndSave',['../interface_document_storage_1_1_logic_1_1_processes_1_1_i_searcher.html#a7160b91ff7af892c1f1528648c250dbe',1,'DocumentStorage.Logic.Processes.ISearcher.ProcessImageAndSave()'],['../class_document_storage_1_1_logic_1_1_processes_1_1_searcher.html#a98800b36d754b657738326abd59f26e3',1,'DocumentStorage.Logic.Processes.Searcher.ProcessImageAndSave()']]],
  ['program_111',['Program',['../class_document_storage_1_1_program_1_1_program.html',1,'DocumentStorage::Program']]]
];
