var searchData=
[
  ['repodatabase_112',['RepoDatabase',['../class_document_storage_1_1_repository_1_1_repo_database.html',1,'DocumentStorage.Repository.RepoDatabase'],['../class_document_storage_1_1_repository_1_1_repo_database.html#a34daeb28b857b381aea4ffe3df9ec796',1,'DocumentStorage.Repository.RepoDatabase.RepoDatabase()']]],
  ['repodb_113',['RepoDB',['../class_document_storage_1_1_logic_1_1_processes_1_1_searcher.html#a07db80f4984421517f2686515947dbaa',1,'DocumentStorage::Logic::Processes::Searcher']]],
  ['repotesseractcop_114',['RepoTesseractCop',['../class_document_storage_1_1_repository_1_1_repo_tesseract_cop.html',1,'DocumentStorage.Repository.RepoTesseractCop'],['../class_document_storage_1_1_repository_1_1_repo_tesseract_cop.html#ac490f931749864ac09583f53a3045333',1,'DocumentStorage.Repository.RepoTesseractCop.RepoTesseractCop()']]],
  ['repowordcorrector_115',['RepoWordCorrector',['../class_document_storage_1_1_repository_1_1_repo_word_corrector.html',1,'DocumentStorage.Repository.RepoWordCorrector'],['../class_document_storage_1_1_repository_1_1_repo_word_corrector.html#acb93c07b8a003a92efab8089f6d36937',1,'DocumentStorage.Repository.RepoWordCorrector.RepoWordCorrector()']]],
  ['rule_116',['Rule',['../class_document_storage_1_1_data_1_1_rule.html',1,'DocumentStorage::Data']]]
];
