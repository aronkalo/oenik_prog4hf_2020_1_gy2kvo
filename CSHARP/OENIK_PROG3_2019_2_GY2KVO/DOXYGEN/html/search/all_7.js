var searchData=
[
  ['menu_101',['Menu',['../class_document_storage_1_1_program_1_1_monitor.html#a40f5cbd0f2488568d92d9191d23f92d9',1,'DocumentStorage::Program::Monitor']]],
  ['mocker_102',['Mocker',['../class_document_storage_1_1_logic_1_1_tests_1_1_mocker.html',1,'DocumentStorage::Logic::Tests']]],
  ['modifyruleset_103',['ModifyRuleSet',['../interface_document_storage_1_1_repository_1_1_interfaces_1_1_i_repo_word_corrector.html#aa583aac5c277943019a20f87c35c0197',1,'DocumentStorage.Repository.Interfaces.IRepoWordCorrector.ModifyRuleSet()'],['../class_document_storage_1_1_repository_1_1_repo_word_corrector.html#a62233897e1b8761ae2b72a2829ca3dd9',1,'DocumentStorage.Repository.RepoWordCorrector.ModifyRuleSet()'],['../interface_document_storage_1_1_word_1_1_interfaces_1_1_i_syntax_corrector.html#a18f9875a83776ad0930027af26ec163f',1,'DocumentStorage.Word.Interfaces.ISyntaxCorrector.ModifyRuleSet()'],['../class_document_storage_1_1_word_1_1_syntax_corrector.html#afae98a9f0713b20ee7517c01c0b1d4e6',1,'DocumentStorage.Word.SyntaxCorrector.ModifyRuleSet()']]],
  ['monitor_104',['Monitor',['../class_document_storage_1_1_program_1_1_monitor.html',1,'DocumentStorage.Program.Monitor'],['../class_document_storage_1_1_program_1_1_monitor.html#ae8e770a352513046c0dc77f4b4f989c3',1,'DocumentStorage.Program.Monitor.Monitor()']]]
];
