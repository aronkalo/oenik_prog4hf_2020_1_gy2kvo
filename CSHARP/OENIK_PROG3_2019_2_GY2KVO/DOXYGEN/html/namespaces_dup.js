var namespaces_dup =
[
    [ "NUnit 3.11 - October 6, 2018", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md34", [
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md33", null ],
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md35", null ]
    ] ],
    [ "NUnit 3.10.1 - March 12, 2018", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md36", null ],
    [ "NUnit 3.10 - March 12, 2018", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md37", [
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md38", null ]
    ] ],
    [ "NUnit 3.9 - November 10, 2017", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md39", [
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md40", null ]
    ] ],
    [ "NUnit 3.8.1 - August 28, 2017", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md41", [
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md42", null ]
    ] ],
    [ "NUnit 3.8 - August 27, 2017", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md43", [
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md44", null ]
    ] ],
    [ "NUnit 3.7.1 - June 6, 2017", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md45", [
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md46", null ]
    ] ],
    [ "NUnit 3.7 - May 29, 2017", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md47", [
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md48", null ]
    ] ],
    [ "NUnit 3.6.1 - February 26, 2017", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md49", [
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md50", null ]
    ] ],
    [ "NUnit 3.6 - January 9, 2017", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md51", [
      [ "Framework", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md52", null ],
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md53", null ]
    ] ],
    [ "NUnit 3.5 - October 3, 2016", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md54", [
      [ "Framework", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md55", null ],
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md56", null ]
    ] ],
    [ "NUnit 3.4.1 - June 30, 2016", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md57", [
      [ "Console Runner", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md58", null ],
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md59", null ]
    ] ],
    [ "NUnit 3.4 - June 25, 2016", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md60", [
      [ "Framework", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md61", null ],
      [ "Engine", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md62", null ],
      [ "Console Runner", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md63", null ],
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md64", null ]
    ] ],
    [ "NUnit 3.2.1 - April 19, 2016", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md65", [
      [ "Framework", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md66", null ],
      [ "Engine", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md67", null ],
      [ "Console Runner", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md68", null ],
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md69", null ]
    ] ],
    [ "NUnit 3.2 - March 5, 2016", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md70", [
      [ "Framework", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md71", null ],
      [ "Engine", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md72", null ],
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md73", null ]
    ] ],
    [ "NUnit 3.0.1 - December 1, 2015", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md74", [
      [ "Console Runner", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md75", null ],
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md76", null ]
    ] ],
    [ "NUnit 3.0.0 Final Release - November 15, 2015", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md77", [
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md78", null ]
    ] ],
    [ "NUnit 3.0.0 Release Candidate 3 - November 13, 2015", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md79", [
      [ "Engine", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md80", null ],
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md81", null ]
    ] ],
    [ "NUnit 3.0.0 Release Candidate 2 - November 8, 2015", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md82", [
      [ "Engine", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md83", null ],
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md84", null ]
    ] ],
    [ "NUnit 3.0.0 Release Candidate - November 1, 2015", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md85", [
      [ "Framework", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md86", null ],
      [ "NUnitLite", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md87", null ],
      [ "Engine", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md88", null ],
      [ "Console Runner", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md89", null ],
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md90", null ]
    ] ],
    [ "NUnit 3.0.0 Beta 5 - October 16, 2015", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md91", [
      [ "Framework", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md92", null ],
      [ "Engine", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md93", null ],
      [ "Console Runner", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md94", [
        [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md95", null ]
      ] ]
    ] ],
    [ "NUnit 3.0.0 Beta 4 - August 25, 2015", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md96", [
      [ "Framework", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md97", null ],
      [ "Engine", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md98", null ],
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md99", null ]
    ] ],
    [ "NUnit 3.0.0 Beta 3 - July 15, 2015", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md100", [
      [ "Framework", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md101", null ],
      [ "Engine", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md102", null ],
      [ "Console", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md103", null ],
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md104", null ]
    ] ],
    [ "NUnit 3.0.0 Beta 2 - May 12, 2015", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md105", [
      [ "Framework", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md106", null ],
      [ "Engine", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md107", null ],
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md108", null ]
    ] ],
    [ "NUnit 3.0.0 Beta 1 - March 25, 2015", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md109", [
      [ "General", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md110", null ],
      [ "Framework", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md111", null ],
      [ "Engine", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md112", null ],
      [ "Console", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md113", null ],
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md114", null ]
    ] ],
    [ "NUnit 3.0.0 Alpha 5 - January 30, 2015", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md115", [
      [ "General", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md116", null ],
      [ "Framework", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md117", null ],
      [ "Engine", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md118", null ],
      [ "Console", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md119", null ],
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md120", null ]
    ] ],
    [ "NUnit 3.0.0 Alpha 4 - December 30, 2014", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md121", [
      [ "Framework", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md122", null ],
      [ "Engine", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md123", null ],
      [ "Console", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md124", null ],
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md125", null ]
    ] ],
    [ "NUnit 3.0.0 Alpha 3 - November 29, 2014", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md126", [
      [ "Breaking Changes", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md127", null ],
      [ "Framework", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md128", null ],
      [ "Engine", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md129", null ],
      [ "Console", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md130", null ],
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md131", null ]
    ] ],
    [ "NUnit 3.0.0 Alpha 2 - November 2, 2014", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md132", [
      [ "Breaking Changes", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md133", null ],
      [ "General", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md134", null ],
      [ "New Features", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md135", null ],
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md136", null ]
    ] ],
    [ "NUnit 3.0.0 Alpha 1 - September 22, 2014", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md137", [
      [ "Breaking Changes", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md138", null ],
      [ "General", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md139", null ],
      [ "New Features", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md140", null ],
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md141", null ],
      [ "Console Issues Resolved (Old nunit-console project, now combined with nunit)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md142", null ]
    ] ],
    [ "NUnit 2.9.7 - August 8, 2014", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md143", [
      [ "Breaking Changes", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md144", null ],
      [ "New Features", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md145", null ],
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md146", null ]
    ] ],
    [ "NUnit 2.9.6 - October 4, 2013", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md147", [
      [ "Main Features", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md148", null ],
      [ "Bug Fixes", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md149", null ],
      [ "Bug Fixes in 2.9.6 But Not Listed Here in the Release", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md150", null ]
    ] ],
    [ "NUnit 2.9.5 - July 30, 2010", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md151", [
      [ "Bug Fixes", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md152", null ]
    ] ],
    [ "NUnit 2.9.4 - May 4, 2010", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md153", [
      [ "Bug Fixes", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md154", null ]
    ] ],
    [ "NUnit 2.9.3 - October 26, 2009", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md155", [
      [ "Main Features", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md156", null ],
      [ "Bug Fixes", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md157", null ]
    ] ],
    [ "NUnit 2.9.2 - September 19, 2009", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md158", [
      [ "Main Features", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md159", null ],
      [ "Bug Fixes", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md160", null ]
    ] ],
    [ "NUnit 2.9.1 - August 27, 2009", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md161", [
      [ "General", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md162", null ],
      [ "Bug Fixes", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_173aed6ce67929792394bb01e5474cb1.html#autotoc_md163", null ],
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md197", null ]
    ] ],
    [ "NUnit 3.11 - October 6, 2018", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md198", [
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md199", null ]
    ] ],
    [ "NUnit 3.10.1 - March 12, 2018", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md200", null ],
    [ "NUnit 3.10 - March 12, 2018", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md201", [
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md202", null ]
    ] ],
    [ "NUnit 3.9 - November 10, 2017", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md203", [
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md204", null ]
    ] ],
    [ "NUnit 3.8.1 - August 28, 2017", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md205", [
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md206", null ]
    ] ],
    [ "NUnit 3.8 - August 27, 2017", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md207", [
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md208", null ]
    ] ],
    [ "NUnit 3.7.1 - June 6, 2017", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md209", [
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md210", null ]
    ] ],
    [ "NUnit 3.7 - May 29, 2017", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md211", [
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md212", null ]
    ] ],
    [ "NUnit 3.6.1 - February 26, 2017", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md213", [
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md214", null ]
    ] ],
    [ "NUnit 3.6 - January 9, 2017", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md215", [
      [ "Framework", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md216", null ],
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md217", null ]
    ] ],
    [ "NUnit 3.5 - October 3, 2016", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md218", [
      [ "Framework", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md219", null ],
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md220", null ]
    ] ],
    [ "NUnit 3.4.1 - June 30, 2016", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md221", [
      [ "Console Runner", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md222", null ],
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md223", null ]
    ] ],
    [ "NUnit 3.4 - June 25, 2016", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md224", [
      [ "Framework", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md225", null ],
      [ "Engine", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md226", null ],
      [ "Console Runner", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md227", null ],
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md228", null ]
    ] ],
    [ "NUnit 3.2.1 - April 19, 2016", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md229", [
      [ "Framework", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md230", null ],
      [ "Engine", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md231", null ],
      [ "Console Runner", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md232", null ],
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md233", null ]
    ] ],
    [ "NUnit 3.2 - March 5, 2016", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md234", [
      [ "Framework", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md235", null ],
      [ "Engine", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md236", null ],
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md237", null ]
    ] ],
    [ "NUnit 3.0.1 - December 1, 2015", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md238", [
      [ "Console Runner", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md239", null ],
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md240", null ]
    ] ],
    [ "NUnit 3.0.0 Final Release - November 15, 2015", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md241", [
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md242", null ]
    ] ],
    [ "NUnit 3.0.0 Release Candidate 3 - November 13, 2015", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md243", [
      [ "Engine", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md244", null ],
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md245", null ]
    ] ],
    [ "NUnit 3.0.0 Release Candidate 2 - November 8, 2015", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md246", [
      [ "Engine", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md247", null ],
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md248", null ]
    ] ],
    [ "NUnit 3.0.0 Release Candidate - November 1, 2015", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md249", [
      [ "Framework", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md250", null ],
      [ "NUnitLite", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md251", null ],
      [ "Engine", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md252", null ],
      [ "Console Runner", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md253", null ],
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md254", null ]
    ] ],
    [ "NUnit 3.0.0 Beta 5 - October 16, 2015", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md255", [
      [ "Framework", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md256", null ],
      [ "Engine", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md257", null ],
      [ "Console Runner", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md258", [
        [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md259", null ]
      ] ]
    ] ],
    [ "NUnit 3.0.0 Beta 4 - August 25, 2015", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md260", [
      [ "Framework", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md261", null ],
      [ "Engine", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md262", null ],
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md263", null ]
    ] ],
    [ "NUnit 3.0.0 Beta 3 - July 15, 2015", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md264", [
      [ "Framework", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md265", null ],
      [ "Engine", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md266", null ],
      [ "Console", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md267", null ],
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md268", null ]
    ] ],
    [ "NUnit 3.0.0 Beta 2 - May 12, 2015", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md269", [
      [ "Framework", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md270", null ],
      [ "Engine", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md271", null ],
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md272", null ]
    ] ],
    [ "NUnit 3.0.0 Beta 1 - March 25, 2015", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md273", [
      [ "General", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md274", null ],
      [ "Framework", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md275", null ],
      [ "Engine", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md276", null ],
      [ "Console", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md277", null ],
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md278", null ]
    ] ],
    [ "NUnit 3.0.0 Alpha 5 - January 30, 2015", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md279", [
      [ "General", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md280", null ],
      [ "Framework", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md281", null ],
      [ "Engine", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md282", null ],
      [ "Console", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md283", null ],
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md284", null ]
    ] ],
    [ "NUnit 3.0.0 Alpha 4 - December 30, 2014", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md285", [
      [ "Framework", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md286", null ],
      [ "Engine", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md287", null ],
      [ "Console", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md288", null ],
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md289", null ]
    ] ],
    [ "NUnit 3.0.0 Alpha 3 - November 29, 2014", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md290", [
      [ "Breaking Changes", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md291", null ],
      [ "Framework", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md292", null ],
      [ "Engine", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md293", null ],
      [ "Console", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md294", null ],
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md295", null ]
    ] ],
    [ "NUnit 3.0.0 Alpha 2 - November 2, 2014", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md296", [
      [ "Breaking Changes", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md297", null ],
      [ "General", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md298", null ],
      [ "New Features", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md299", null ],
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md300", null ]
    ] ],
    [ "NUnit 3.0.0 Alpha 1 - September 22, 2014", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md301", [
      [ "Breaking Changes", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md302", null ],
      [ "General", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md303", null ],
      [ "New Features", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md304", null ],
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md305", null ],
      [ "Console Issues Resolved (Old nunit-console project, now combined with nunit)", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md306", null ]
    ] ],
    [ "NUnit 2.9.7 - August 8, 2014", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md307", [
      [ "Breaking Changes", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md308", null ],
      [ "New Features", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md309", null ],
      [ "Issues Resolved", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md310", null ]
    ] ],
    [ "NUnit 2.9.6 - October 4, 2013", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md311", [
      [ "Main Features", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md312", null ],
      [ "Bug Fixes", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md313", null ],
      [ "Bug Fixes in 2.9.6 But Not Listed Here in the Release", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md314", null ]
    ] ],
    [ "NUnit 2.9.5 - July 30, 2010", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md315", [
      [ "Bug Fixes", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md316", null ]
    ] ],
    [ "NUnit 2.9.4 - May 4, 2010", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md317", [
      [ "Bug Fixes", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md318", null ]
    ] ],
    [ "NUnit 2.9.3 - October 26, 2009", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md319", [
      [ "Main Features", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md320", null ],
      [ "Bug Fixes", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md321", null ]
    ] ],
    [ "NUnit 2.9.2 - September 19, 2009", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md322", [
      [ "Main Features", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md323", null ],
      [ "Bug Fixes", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md324", null ]
    ] ],
    [ "NUnit 2.9.1 - August 27, 2009", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md325", [
      [ "General", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md326", null ],
      [ "Bug Fixes", "md__c_1__users__kalo__aron__desktop_oenik_prog3_2019_2_gy2kvo__c_s_h_a_r_p_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html#autotoc_md327", null ]
    ] ],
    [ "DocumentStorage", "namespace_document_storage.html", "namespace_document_storage" ]
];