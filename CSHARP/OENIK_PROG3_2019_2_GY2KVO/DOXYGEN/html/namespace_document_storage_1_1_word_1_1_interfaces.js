var namespace_document_storage_1_1_word_1_1_interfaces =
[
    [ "ISpellChecker", "interface_document_storage_1_1_word_1_1_interfaces_1_1_i_spell_checker.html", "interface_document_storage_1_1_word_1_1_interfaces_1_1_i_spell_checker" ],
    [ "ISyntaxCorrector", "interface_document_storage_1_1_word_1_1_interfaces_1_1_i_syntax_corrector.html", "interface_document_storage_1_1_word_1_1_interfaces_1_1_i_syntax_corrector" ],
    [ "ITranslator", "interface_document_storage_1_1_word_1_1_interfaces_1_1_i_translator.html", "interface_document_storage_1_1_word_1_1_interfaces_1_1_i_translator" ]
];