var interface_document_storage_1_1_repository_1_1_interfaces_1_1_i_repo_database =
[
    [ "DeletePage", "interface_document_storage_1_1_repository_1_1_interfaces_1_1_i_repo_database.html#a125612b3adef117450a393c90811cac1", null ],
    [ "DeleteRule", "interface_document_storage_1_1_repository_1_1_interfaces_1_1_i_repo_database.html#a2374c739041ef11eb8122ddc3b51c026", null ],
    [ "DeleteUser", "interface_document_storage_1_1_repository_1_1_interfaces_1_1_i_repo_database.html#a7edb06dbe2944a61331f10aad9f5de07", null ],
    [ "Getlines", "interface_document_storage_1_1_repository_1_1_interfaces_1_1_i_repo_database.html#a8d6688e7abd395b4c0dbf454c333b659", null ],
    [ "Getpages", "interface_document_storage_1_1_repository_1_1_interfaces_1_1_i_repo_database.html#a9e09dc9c8ddbabf51c8542d1f0a21b6b", null ],
    [ "GetPagesByUser", "interface_document_storage_1_1_repository_1_1_interfaces_1_1_i_repo_database.html#a48447e444cd708be0528ebc312670e46", null ],
    [ "GetRuleCountForUsers", "interface_document_storage_1_1_repository_1_1_interfaces_1_1_i_repo_database.html#a73c1fdf502f2d19ee0f463870cd48651", null ],
    [ "Getrules", "interface_document_storage_1_1_repository_1_1_interfaces_1_1_i_repo_database.html#ac1781d7654148d10465dcafe3f2715fd", null ],
    [ "GetUserName", "interface_document_storage_1_1_repository_1_1_interfaces_1_1_i_repo_database.html#a7ed53aa952962a2628125942bf064bc5", null ],
    [ "Getusers", "interface_document_storage_1_1_repository_1_1_interfaces_1_1_i_repo_database.html#a8f0ba79831c6cf1f65766529c5ffd500", null ],
    [ "Getwords", "interface_document_storage_1_1_repository_1_1_interfaces_1_1_i_repo_database.html#a8dec6f2a1b11e3ccdade479a58536705", null ],
    [ "Insertline", "interface_document_storage_1_1_repository_1_1_interfaces_1_1_i_repo_database.html#a04cfdc81b82685a269f6caab29ba1068", null ],
    [ "Insertpage", "interface_document_storage_1_1_repository_1_1_interfaces_1_1_i_repo_database.html#a66e36c35479d3c0aa83a27c7814846a3", null ],
    [ "Insertrule", "interface_document_storage_1_1_repository_1_1_interfaces_1_1_i_repo_database.html#a336e42766708be21c9aeb74e36388f9e", null ],
    [ "Insertuser", "interface_document_storage_1_1_repository_1_1_interfaces_1_1_i_repo_database.html#a02bfe80d516c789f307bf57e5830038a", null ],
    [ "Insertword", "interface_document_storage_1_1_repository_1_1_interfaces_1_1_i_repo_database.html#a0db1b21ff0de8d17bc4c604c473f903d", null ],
    [ "Startup", "interface_document_storage_1_1_repository_1_1_interfaces_1_1_i_repo_database.html#ad8f06ba1fd5799e3f7ea84c613f1e9dc", null ],
    [ "UpdateRule", "interface_document_storage_1_1_repository_1_1_interfaces_1_1_i_repo_database.html#af89837e9710cb0cd789f56f38cb3bc2f", null ],
    [ "UpdateUser", "interface_document_storage_1_1_repository_1_1_interfaces_1_1_i_repo_database.html#aa9e92180da2b2237adc92af45b63770a", null ],
    [ "UpdateWord", "interface_document_storage_1_1_repository_1_1_interfaces_1_1_i_repo_database.html#a83a3b880b42907bc9cc86472366c0b96", null ]
];