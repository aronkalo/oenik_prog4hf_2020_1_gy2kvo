var class_document_storage_1_1_data_1_1_page =
[
    [ "Page", "class_document_storage_1_1_data_1_1_page.html#a3c53c22ceceeb5df32bef3b5c797eda0", null ],
    [ "BASE64_IMAGE", "class_document_storage_1_1_data_1_1_page.html#a01b5aa04ffbf839dc00bb4a50ad96978", null ],
    [ "CREATE_DATE", "class_document_storage_1_1_data_1_1_page.html#af561553d36b7ba1b88815b96c97a7337", null ],
    [ "CREATE_USER", "class_document_storage_1_1_data_1_1_page.html#a2bb5d4af4ab8d22936b8872a8f2b4274", null ],
    [ "DOCUMENT_TYPE", "class_document_storage_1_1_data_1_1_page.html#a8f84777f6ed86a2878ef2a2816f6318f", null ],
    [ "Id", "class_document_storage_1_1_data_1_1_page.html#aeafdcfa0b95bc58a05aad1d71243b2aa", null ],
    [ "Line", "class_document_storage_1_1_data_1_1_page.html#a3457bf00f3d423e2be3dc17f5ca23d17", null ],
    [ "OGIGINAL_FILENAME", "class_document_storage_1_1_data_1_1_page.html#a726c4a0dab603114c1f713b16b24c8d2", null ],
    [ "User", "class_document_storage_1_1_data_1_1_page.html#a1f90cde735f14fdcd8469419d7e68042", null ],
    [ "Word", "class_document_storage_1_1_data_1_1_page.html#a8b92f7b526c0f292743ffcfc4d5b0fd8", null ]
];