var namespace_document_storage_1_1_word =
[
    [ "Interfaces", "namespace_document_storage_1_1_word_1_1_interfaces.html", "namespace_document_storage_1_1_word_1_1_interfaces" ],
    [ "SpellChecker", "class_document_storage_1_1_word_1_1_spell_checker.html", "class_document_storage_1_1_word_1_1_spell_checker" ],
    [ "SyntaxCorrector", "class_document_storage_1_1_word_1_1_syntax_corrector.html", "class_document_storage_1_1_word_1_1_syntax_corrector" ],
    [ "Translator", "class_document_storage_1_1_word_1_1_translator.html", "class_document_storage_1_1_word_1_1_translator" ]
];