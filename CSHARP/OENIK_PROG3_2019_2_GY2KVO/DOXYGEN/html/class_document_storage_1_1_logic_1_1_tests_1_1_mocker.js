var class_document_storage_1_1_logic_1_1_tests_1_1_mocker =
[
    [ "Init", "class_document_storage_1_1_logic_1_1_tests_1_1_mocker.html#a48e6d048ad82f51623794aef85793b11", null ],
    [ "Test_Get_RuleCountForUsers_CheckCount_AndNames", "class_document_storage_1_1_logic_1_1_tests_1_1_mocker.html#a6ee41a38d57349e114ba8207a4466ae6", null ],
    [ "Test_GetLineByIDandCheckCreateUser", "class_document_storage_1_1_logic_1_1_tests_1_1_mocker.html#a43f3784104899951cd907bff3b3b75a6", null ],
    [ "Test_GetLineByIDOverIndexAndCheckPageID", "class_document_storage_1_1_logic_1_1_tests_1_1_mocker.html#af92c5a42f29dd43e9bfc02b0f6f1125c", null ],
    [ "Test_GetLineByIDOverIndexAndThrowsException", "class_document_storage_1_1_logic_1_1_tests_1_1_mocker.html#af8ce64e8f07fd57d657c9f32b6bf015f", null ],
    [ "Test_GetPageByIDandCheckImage", "class_document_storage_1_1_logic_1_1_tests_1_1_mocker.html#af3e20f0c4cee970ded73a4dfae40c471", null ],
    [ "Test_GetPageByIDandCheckLoc", "class_document_storage_1_1_logic_1_1_tests_1_1_mocker.html#a4497a7d77e6a909d07349e80f552e74a", null ],
    [ "Test_GetPageByIDOverIndexAndThrowsException", "class_document_storage_1_1_logic_1_1_tests_1_1_mocker.html#a7d473e8314a967bc6264300d044ff08e", null ],
    [ "Test_GetPagesToAUserAndCheckFilenameAndCount", "class_document_storage_1_1_logic_1_1_tests_1_1_mocker.html#ab20cae4930a3508c960f195c10c603e9", null ],
    [ "Test_GetRulesAndCheckCount", "class_document_storage_1_1_logic_1_1_tests_1_1_mocker.html#a0ad37dadde4fca0ce506cc81e75a6eb1", null ],
    [ "Test_GetUsernamesByID_OverindexandthrowsException", "class_document_storage_1_1_logic_1_1_tests_1_1_mocker.html#ac602bd335619f8a2ddb1659ed1a0907d", null ],
    [ "Test_GetUsernamesByID_validIndexReturnsName", "class_document_storage_1_1_logic_1_1_tests_1_1_mocker.html#a792d282f15a36f1a5aadb037bad5e77b", null ],
    [ "Test_GetUsers_AndCheckCount", "class_document_storage_1_1_logic_1_1_tests_1_1_mocker.html#ac70db711b157157cd1edce742ed965c9", null ],
    [ "Test_GetUsers_AndFirstItemOK", "class_document_storage_1_1_logic_1_1_tests_1_1_mocker.html#a99f32279cb8d812e0b570889512032fd", null ],
    [ "Test_GetWordByIDOverIndexAndCheckLineID", "class_document_storage_1_1_logic_1_1_tests_1_1_mocker.html#a2e76db5b9653f8431efda673f5d2c20b", null ],
    [ "Test_GetWordByIDOverIndexAndCheckValue", "class_document_storage_1_1_logic_1_1_tests_1_1_mocker.html#a0bb4b1dfaf8ae024f02c34134c532057", null ],
    [ "Test_GetWordByIDOverIndexOverIndexAndThrowsException", "class_document_storage_1_1_logic_1_1_tests_1_1_mocker.html#ade61e8bd3b5e14ccbffd6de5ffbaf15d", null ],
    [ "Test_SearchWordAndGotMatch", "class_document_storage_1_1_logic_1_1_tests_1_1_mocker.html#ae70acab69544c2a3a994cf3b02263154", null ],
    [ "Test_SearchWordAndNoMatch", "class_document_storage_1_1_logic_1_1_tests_1_1_mocker.html#a5740b2d52f3d1754ad8f4fd6e91a2ead", null ],
    [ "Test_SearchWordAndOverIndexThrowsException", "class_document_storage_1_1_logic_1_1_tests_1_1_mocker.html#a9b251f9dfcae8ef05e6a5f7fe6d07330", null ]
];