var class_document_storage_1_1_data_1_1_line =
[
    [ "Line", "class_document_storage_1_1_data_1_1_line.html#abc4adde9d4f680f018be3fa74e7c416c", null ],
    [ "BASE64_IMAGE", "class_document_storage_1_1_data_1_1_line.html#a236a0557f4cc599ba1bb52c408791d04", null ],
    [ "CREATE_DATE", "class_document_storage_1_1_data_1_1_line.html#ada15621d2227fda72b86ce5f3527ced3", null ],
    [ "CREATE_USER", "class_document_storage_1_1_data_1_1_line.html#a1c0f8afb6c5fde72a6b94687e1a137cd", null ],
    [ "HEIGHT", "class_document_storage_1_1_data_1_1_line.html#a493df2ad09d70aaff8545801b82e0a47", null ],
    [ "Id", "class_document_storage_1_1_data_1_1_line.html#aee0d7894a54cdf55ddef6bf70eae7613", null ],
    [ "Page", "class_document_storage_1_1_data_1_1_line.html#a16a99ab17713194c22e336d7901e9e12", null ],
    [ "PAGE_ID", "class_document_storage_1_1_data_1_1_line.html#a3bb745d55a9762040f150090517d4c6f", null ],
    [ "User", "class_document_storage_1_1_data_1_1_line.html#a17a54eef526019bdcd3f7ded057a2e8d", null ],
    [ "VALUE", "class_document_storage_1_1_data_1_1_line.html#a80ecdc225133f89acfafcb8a7489aa4f", null ],
    [ "WIDTH", "class_document_storage_1_1_data_1_1_line.html#a6ddaa3663da256b4ad0ea93c4ca53d76", null ],
    [ "Word", "class_document_storage_1_1_data_1_1_line.html#a1f3d0031974c931afae929c19e603c91", null ],
    [ "X", "class_document_storage_1_1_data_1_1_line.html#a19481f13e43fbfe824c160a021429cfa", null ],
    [ "Y", "class_document_storage_1_1_data_1_1_line.html#add4fe7d7a44612f6b42cbf219de89659", null ]
];