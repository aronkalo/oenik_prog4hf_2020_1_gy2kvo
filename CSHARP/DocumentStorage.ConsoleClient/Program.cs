﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DocumentStorage.ConsoleClient
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Admin { get; set; }
        public DateTime AddDate { get; set; }

        public override string ToString()
        {
            return $"ID:{Id} Admin:{Admin} Date:{AddDate.ToString()} Name:{Name}";
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("WAITING");
            Console.ReadLine();
            string localhost = "http://localhost:61395/api/UserApi/";
            using(HttpClient client = new HttpClient())
            {
                string json = client.GetStringAsync(localhost + "all").Result;
                var list = JsonConvert.DeserializeObject<List<User>>(json);
                foreach (var item in list)
                {
                    Console.WriteLine(item);
                }
                Console.ReadLine();

                Dictionary<string, string> postData;
                string response;

                postData = new Dictionary<string, string>();
                postData.Add(nameof(User.Admin), "false");
                postData.Add(nameof(User.Name), "Kolbice");

                response = client.PostAsync(localhost + "add", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                json = client.GetStringAsync(localhost + "all").Result;
                Console.WriteLine("ADD: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();

                int carId = JsonConvert.DeserializeObject<List<User>>(json).Single(x => x.Name == "Kolbice").Id;
                postData = new Dictionary<string, string>();
                postData.Add(nameof(User.Id), carId.ToString());
                postData.Add(nameof(User.Admin), "true");
                postData.Add(nameof(User.Name), "Kolbice");
                response = client.PostAsync(localhost + "mod", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                json = client.GetStringAsync(localhost + "all").Result;
                Console.WriteLine("MOD: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();

                response = client.GetStringAsync(localhost + "del/" + carId).Result;
                json = client.GetStringAsync(localhost + "all").Result;
                Console.WriteLine("DEL: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();
            }
            Console.ReadLine();
        }
    }
}
